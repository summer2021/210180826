/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-24     lizhirui     port to new version rt-smart
 */


#include <rtthread.h>
#include <rtdevice.h>
#include "interrupt.h"
#include "gic.h"

#include <lwp.h>
#include <lwp_shm.h>
#include <lwp_user_mm.h>

enum
{
    LWP_DEVICE_CMD_INIT = 0,
    LWP_DEVICE_CMD_OPEN,
    LWP_DEVICE_CMD_CLOSE,
    LWP_DEVICE_CMD_READ,
    LWP_DEVICE_CMD_WRITE,
    LWP_DEVICE_CMD_CONTROL,
    LWP_DEVICE_CMD_INITERRUPT,
    LWP_DEVICE_CMD_QUIT,
};

#define LWP_DEVICE_OP_ARGS_MAX_NR 5
struct lwp_device_cmd
{
    void *client_lwp;
    rt_sem_t reply_sem;
    rt_uint32_t cmd;
    void *ret;
    void* arg[LWP_DEVICE_OP_ARGS_MAX_NR];
};

struct lwp_device
{
    struct rt_device parent;
    struct rt_lwp *lwp;
    rt_device_t udev;
    rt_channel_t ch_server;
};

static void (*udev_isr_table[MAX_HANDLERS])(int vector, void *param);
static void *udev_isr_param[MAX_HANDLERS];
static int  udev_isr_shmid[MAX_HANDLERS];
static void udev_interrupt_handler(int vector, void *param)
{
    struct rt_channel_msg msg;
    struct lwp_device *device = (struct lwp_device *)param;
    rt_channel_t ch_server = (rt_channel_t)device->ch_server;
    struct lwp_device_cmd *cmd;

    cmd = (struct lwp_device_cmd*)lwp_shminfo(udev_isr_shmid[vector]);
    cmd->cmd = LWP_DEVICE_CMD_INITERRUPT;
    cmd->client_lwp = RT_NULL;
    cmd->reply_sem = RT_NULL;
    cmd->arg[0] = (void*)udev_isr_table[vector];
    cmd->arg[1] = (void*)vector;
    cmd->arg[2] = (void*)udev_isr_param[vector];
    cmd->arg[3] = device;
    msg.type = RT_CHANNEL_RAW;
    msg.u.d = (void*)udev_isr_shmid[vector];
    rt_raw_channel_send(ch_server, (void*)&msg);
}

static rt_err_t  lwp_device_init(rt_device_t dev)
{
    struct lwp_device *device = (struct lwp_device *)dev;
    struct rt_channel_msg msg;
    rt_sem_t sem = RT_NULL;
    rt_err_t ret = RT_EOK;
    int shmid = -1;
    struct lwp_device_cmd *cmd;

    RT_ASSERT(device->lwp != RT_NULL);
    sem = rt_sem_create("udev", 0, RT_IPC_FLAG_FIFO);
    if (!sem)
    {
        ret = RT_EIO;
        goto out;
    }
    shmid = lwp_shmget((size_t)dev, sizeof(struct lwp_device_cmd), 1);
    if (shmid < 0)
    {
        ret = RT_EIO;
        goto out;
    }
    cmd = (struct lwp_device_cmd*)lwp_shminfo(shmid);
    cmd->client_lwp = lwp_self();
    cmd->reply_sem = sem;
    cmd->ret = (void*)RT_EIO;
    cmd->cmd = LWP_DEVICE_CMD_INIT;
    cmd->arg[0] = (void*)device->udev;

    msg.type = RT_CHANNEL_RAW;
    msg.u.d = (void*)shmid;
    ret = rt_raw_channel_send(device->ch_server, (void*)&msg);
    if (ret == RT_EOK)
    {
        while (rt_sem_take(sem, RT_WAITING_FOREVER) != RT_EOK);
    }
    ret = (rt_err_t)cmd->ret;
out:
    if (shmid >=0)
    {
        lwp_shmrm(shmid);
    }
    if (sem)
    {
        rt_sem_delete(sem);
    }
    return ret;
}

static rt_err_t  lwp_device_open(rt_device_t dev, rt_uint16_t oflag)
{
    struct lwp_device *device = (struct lwp_device *)dev;
    struct rt_channel_msg msg;
    rt_sem_t sem = RT_NULL;
    rt_err_t ret = RT_EOK;
    int shmid = -1;
    struct lwp_device_cmd *cmd;

    RT_ASSERT(device->lwp != RT_NULL);
    sem = rt_sem_create("udev", 0, RT_IPC_FLAG_FIFO);
    if (!sem)
    {
        ret = RT_EIO;
        goto out;
    }
    shmid = lwp_shmget((size_t)dev, sizeof(struct lwp_device_cmd), 1);
    if (shmid < 0)
    {
        ret = RT_EIO;
        goto out;
    }
    cmd = (struct lwp_device_cmd*)lwp_shminfo(shmid);
    cmd->client_lwp = lwp_self();
    cmd->reply_sem = sem;
    cmd->ret = (void*)RT_EIO;
    cmd->cmd = LWP_DEVICE_CMD_OPEN;
    cmd->arg[0] = (void*)device->udev;
    cmd->arg[1] = (void*)(size_t)oflag;

    msg.type = RT_CHANNEL_RAW;
    msg.u.d = (void*)shmid;
    ret = rt_raw_channel_send(device->ch_server, (void*)&msg);
    if (ret == RT_EOK)
    {
        while (rt_sem_take(sem, RT_WAITING_FOREVER) != RT_EOK);
    }
    ret = (rt_err_t)cmd->ret;
out:
    if (shmid >=0)
    {
        lwp_shmrm(shmid);
    }
    if (sem)
    {
        rt_sem_delete(sem);
    }
    return ret;
}

static rt_err_t  lwp_device_close(rt_device_t dev)
{
    struct lwp_device *device = (struct lwp_device *)dev;
    struct rt_channel_msg msg;
    rt_sem_t sem = RT_NULL;
    rt_err_t ret = RT_EOK;
    int shmid = -1;
    struct lwp_device_cmd *cmd;

    RT_ASSERT(device->lwp != RT_NULL);
    sem = rt_sem_create("udev", 0, RT_IPC_FLAG_FIFO);
    if (!sem)
    {
        ret = RT_EIO;
        goto out;
    }
    shmid = lwp_shmget((size_t)dev, sizeof(struct lwp_device_cmd), 1);
    if (shmid < 0)
    {
        ret = RT_EIO;
        goto out;
    }
    cmd = (struct lwp_device_cmd*)lwp_shminfo(shmid);
    cmd->client_lwp = lwp_self();
    cmd->reply_sem = sem;
    cmd->ret = (void*)0;
    cmd->cmd = LWP_DEVICE_CMD_CLOSE;
    cmd->arg[0] = (void*)device->udev;

    msg.type = RT_CHANNEL_RAW;
    msg.u.d = (void*)shmid;
    ret = rt_raw_channel_send(device->ch_server, (void*)&msg);
    if (ret == RT_EOK)
    {
        while (rt_sem_take(sem, RT_WAITING_FOREVER) != RT_EOK);
    }
    ret = (rt_err_t)cmd->ret;
out:
    if (shmid >=0)
    {
        lwp_shmrm(shmid);
    }
    if (sem)
    {
        rt_sem_delete(sem);
    }
    return ret;
}

static rt_size_t lwp_device_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
    rt_size_t ret_len = 0;
    struct lwp_device *device = (struct lwp_device *)dev;
    struct rt_channel_msg msg;
    rt_sem_t sem = RT_NULL;
    rt_err_t ret;
    int shmid = -1;
    struct lwp_device_cmd *cmd;

    RT_ASSERT(device->lwp != RT_NULL);
    sem = rt_sem_create("udev", 0, RT_IPC_FLAG_FIFO);
    if (!sem)
    {
        goto out;
    }
    shmid = lwp_shmget((size_t)dev, sizeof(struct lwp_device_cmd), 1);
    if (shmid < 0)
    {
        goto out;
    }
    cmd = (struct lwp_device_cmd*)lwp_shminfo(shmid);
    cmd->client_lwp = lwp_self();
    cmd->reply_sem = sem;
    cmd->ret = (void*)0;
    cmd->cmd = LWP_DEVICE_CMD_READ;
    cmd->arg[0] = (void*)device->udev;
    cmd->arg[1] = (void*)pos;
    cmd->arg[2] = (void*)buffer;
    cmd->arg[3] = (void*)size;

    msg.type = RT_CHANNEL_RAW;
    msg.u.d = (void*)shmid;
    ret = rt_raw_channel_send(device->ch_server, (void*)&msg);
    if (ret == RT_EOK)
    {
        while (rt_sem_take(sem, RT_WAITING_FOREVER) != RT_EOK);
    }
    ret_len = (rt_size_t)cmd->ret;
out:
    if (shmid >=0)
    {
        lwp_shmrm(shmid);
    }
    if (sem)
    {
        rt_sem_delete(sem);
    }
    return ret_len;
}

static rt_size_t lwp_device_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    rt_size_t ret_len = 0;
    struct lwp_device *device = (struct lwp_device *)dev;
    struct rt_channel_msg msg;
    rt_sem_t sem = RT_NULL;
    rt_err_t ret;
    int shmid = -1;
    struct lwp_device_cmd *cmd;

    RT_ASSERT(device->lwp != RT_NULL);
    sem = rt_sem_create("udev", 0, RT_IPC_FLAG_FIFO);
    if (!sem)
    {
        goto out;
    }
    shmid = lwp_shmget((size_t)dev, sizeof(struct lwp_device_cmd), 1);
    if (shmid < 0)
    {
        goto out;
    }
    cmd = (struct lwp_device_cmd*)lwp_shminfo(shmid);
    cmd->client_lwp = lwp_self();
    cmd->reply_sem = sem;
    cmd->ret = (void*)0;
    cmd->cmd = LWP_DEVICE_CMD_WRITE;
    cmd->arg[0] = (void*)device->udev;
    cmd->arg[1] = (void*)pos;
    cmd->arg[2] = (void*)buffer;
    cmd->arg[3] = (void*)size;

    msg.type = RT_CHANNEL_RAW;
    msg.u.d = (void*)shmid;
    ret = rt_raw_channel_send(device->ch_server, (void*)&msg);
    if (ret == RT_EOK)
    {
        while (rt_sem_take(sem, RT_WAITING_FOREVER) != RT_EOK);
    }
    ret_len = (rt_size_t)cmd->ret;
out:
    if (shmid >=0)
    {
        lwp_shmrm(shmid);
    }
    if (sem)
    {
        rt_sem_delete(sem);
    }
    return ret_len;
}

struct lwp_device_mapinfo
{
    void *vaddr;
    void *paddr;
    size_t size;
    int cached;
};

struct lwp_device_interrupt_info
{
    int vector;
    void *handler;
    void *param;
};

struct lwp_device_data_info
{
    void *lwp;
    void *src;
    void *dst;
    size_t size;
};

struct lwp_device_cache_info
{
    int ops;
    void *addr;
    size_t size;
};

rt_isr_handler_t rt_hw_interrupt_install_user(int vector, rt_isr_handler_t handler,
        void *param, const char *name);

static rt_err_t  lwp_device_control(rt_device_t dev, int dev_cmd, void *args)
{
    struct rt_lwp *lwp = lwp_self();
    struct rt_lwp *client_lwp;
    struct lwp_device *device = (struct lwp_device *)dev;
    struct rt_channel_msg msg;
    rt_sem_t sem = RT_NULL;
    rt_err_t ret = RT_EOK;
    int shmid = -1;
    struct lwp_device_cmd *cmd;

    RT_ASSERT(device->lwp != RT_NULL);
    switch (dev_cmd)
    {
    case RT_DEVICE_CTRL_IOREMAP:
        {
            struct lwp_device_mapinfo *info = (struct lwp_device_mapinfo*)args;
            rt_kprintf("rt_device_ctrl_ioremap,info = %p,addr = %p,size = %d\n",info,info->paddr,info->size);

            /* this control option can be run in lwp process only */
            if (!lwp)
            {
                return RT_ERROR;
            }
            info->vaddr = lwp_map_user_phy(lwp, RT_NULL, info->paddr, info->size, info->cached);
            rt_kprintf("vaddr = %p\n",info->vaddr);
            if (!info->vaddr)
            {
                return RT_ERROR;
            }
            
            return RT_EOK;
        }
    case RT_DEVICE_CTRL_IOUNREMAP:
        {
            struct lwp_device_mapinfo *info = (struct lwp_device_mapinfo*)args;
            int ret;

            /* this control option can be run in lwp process only */
            if (!lwp)
            {
                return RT_ERROR;
            }
            ret = lwp_unmap_user_phy(lwp, info->vaddr);
            if (ret != 0)
            {
                return RT_ERROR;
            }
            return RT_EOK;
        }
    case RT_DEVICE_CTRL_MMAP:
        {
            struct lwp_device_mapinfo *info = (struct lwp_device_mapinfo*)args;
            void *kvaddr;

            /* this control option can be run in lwp process only */
            if (!lwp)
            {
                return RT_ERROR;
            }
            kvaddr = rt_pages_alloc(rt_page_bits(info->size));
            if (!kvaddr)
            {
                info->paddr = RT_NULL;
                info->vaddr = RT_NULL;
                return RT_ERROR;
            }
            info->paddr = kvaddr + PV_OFFSET;
            info->vaddr = lwp_map_user_phy(lwp, RT_NULL, info->paddr, info->size, info->cached);
            if (!info->vaddr)
            {
                rt_pages_free(kvaddr, rt_page_bits(info->size));
                info->paddr = RT_NULL;
                info->vaddr = RT_NULL;
                return RT_ERROR;
            }
            return RT_EOK;
        }
        break;
    case RT_DEVICE_CTRL_MUNMAP:
        {
            struct lwp_device_mapinfo *info = (struct lwp_device_mapinfo*)args;
            void *paddr;
            int ret;

            /* this control option can be run in lwp process only */
            if (!lwp) /* kernel thread */
            {
                return RT_ERROR;
            }
            if (!info->vaddr)
            {
                return RT_ERROR;
            }
            paddr = rt_hw_mmu_v2p(&lwp->mmu_info, info->vaddr);
            if (!paddr)
            {
                return RT_ERROR;
            }
            ret = lwp_unmap_user_phy(lwp, info->vaddr);
            rt_pages_free(paddr - PV_OFFSET, rt_page_bits(info->size));
            if (ret != 0)
            {
                return RT_ERROR;
            }
            return RT_EOK;
        }
    case RT_DEVICE_CTRL_DATA_GET:
        {
            struct lwp_device_data_info *info = (struct lwp_device_data_info*)args;
            rt_mmu_info *minfo;

            /* this control option can be run in udev process only */
            if (lwp != device->lwp) /* must be udev lwp */
            {
                return RT_ERROR;
            }
            client_lwp = (struct rt_lwp*)info->lwp;
            if (lwp)
            {
                minfo = &client_lwp->mmu_info;
            }
            else
            {
                minfo = &mmu_info;
            }
            info->size = lwp_data_get(minfo, info->dst, info->src, info->size);
            return RT_EOK;
        }
    case RT_DEVICE_CTRL_DATA_PUT:
        {
            struct lwp_device_data_info *info = (struct lwp_device_data_info*)args;
            rt_mmu_info *minfo;

            /* this control option can be run in udev process only */
            if (lwp != device->lwp) /* must be udev lwp */
            {
                return RT_ERROR;
            }
            client_lwp = (struct rt_lwp*)info->lwp;
            if (lwp)
            {
                minfo = &client_lwp->mmu_info;
            }
            else
            {
                minfo = &mmu_info;
            }
            info->size = lwp_data_put(minfo, info->dst, info->src, info->size);
            return RT_EOK;
        }
    case RT_DEVICE_CTRL_CACHE_MAINTAIN:
        {
            struct lwp_device_cache_info *info = (struct lwp_device_cache_info*)args;

            /* this control option can be run in udev process only */
            if (lwp != device->lwp)
            {
                return RT_ERROR;
            }
            rt_hw_cpu_dcache_ops(info->ops, info->addr, (int)info->size);
            return RT_EOK;
        }
    case RT_DEVICE_CTRL_SET_INT_HANDLER:
        {
            struct lwp_device_interrupt_info *info = (struct lwp_device_interrupt_info*)args;

            /* this control option can be run in udev process only */
            if (lwp != device->lwp)
            {
                return RT_ERROR;
            }
            if (info->handler)
            {
                int shmid = -1;

                rt_hw_interrupt_install_user(info->vector, udev_interrupt_handler, (void*)device, "udev");
                shmid = lwp_shmget((size_t)dev, sizeof(struct lwp_device_cmd), 1);
                if (shmid < 0)
                {
                    break;
                }
                udev_isr_table[info->vector] = info->handler;
                udev_isr_param[info->vector] = info->param;
                udev_isr_shmid[info->vector] = shmid;
            }
            else
            {
                rt_hw_interrupt_mask(info->vector);
                rt_hw_interrupt_install(info->vector, RT_NULL, RT_NULL, RT_NULL);
                lwp_shmrm(udev_isr_shmid[info->vector]);
                udev_isr_table[info->vector] = RT_NULL;
            }
            return RT_EOK;
        }
    case RT_DEVICE_CTRL_INT_UMASK:
        {
            struct lwp_device_interrupt_info *info = (struct lwp_device_interrupt_info*)args;

            /* this control option can be run in udev process only */
            if (lwp != device->lwp)
            {
                return RT_ERROR;
            }
            rt_hw_interrupt_umask(info->vector);
            return RT_EOK;
        }
        break;
    case RT_DEVICE_CTRL_INT_MASK:
        {
            struct lwp_device_interrupt_info *info = (struct lwp_device_interrupt_info*)args;

            /* this control option can be run in udev process only */
            if (lwp != device->lwp)
            {
                return RT_ERROR;
            }
            rt_hw_interrupt_mask(info->vector);
            return RT_EOK;
        }
        break;
    default:
        break;
    }
    sem = rt_sem_create("udev", 0, RT_IPC_FLAG_FIFO);
    if (!sem)
    {
        ret = RT_EIO;
        goto out;
    }
    shmid = lwp_shmget((size_t)dev, sizeof(struct lwp_device_cmd), 1);
    if (shmid < 0)
    {
        ret = RT_EIO;
        goto out;
    }
    cmd = (struct lwp_device_cmd*)lwp_shminfo(shmid);
    cmd->client_lwp = lwp_self();
    cmd->reply_sem = sem;
    cmd->ret = (void*)RT_EIO;
    cmd->cmd = LWP_DEVICE_CMD_CONTROL;
    cmd->arg[0] = (void*)device->udev;
    cmd->arg[1] = (void*)dev_cmd;
    cmd->arg[2] = (void*)args;

    msg.type = RT_CHANNEL_RAW;
    msg.u.d = (void*)shmid;
    ret = rt_raw_channel_send(device->ch_server, (void*)&msg);
    if (ret == RT_EOK)
    {
        while (rt_sem_take(sem, RT_WAITING_FOREVER) != RT_EOK);
    }
    ret = (rt_err_t)cmd->ret;
out:
    if (shmid >=0)
    {
        lwp_shmrm(shmid);
    }
    if (sem)
    {
        rt_sem_delete(sem);
    }
    return ret;
}

#ifdef RT_USING_DEVICE_OPS
static const struct rt_device_ops lwp_device_ops =
{
    lwp_device_init,
    lwp_device_open,
    lwp_device_close,
    lwp_device_read,
    lwp_device_write,
    lwp_device_control
};
#endif

rt_err_t lwp_device_register(rt_device_t dev,
                            const char *name,
                            rt_uint16_t flags)
{
    struct lwp_device *device = RT_NULL;
    rt_err_t ret;

    device = (struct lwp_device*)rt_malloc(sizeof *device);
    if (!device)
    {
        return RT_ENOMEM;
    }
    rt_memset(device, 0, sizeof *device);

    device->lwp = lwp_self();
    device->ch_server = rt_raw_channel_open(name, 0);
    if (!device->ch_server)
    {
        goto err;
    }

    device->udev = dev;
#ifdef RT_USING_DEVICE_OPS
    device->parent.ops = &lwp_device_ops;
#else
    device->parent.init        = lwp_device_init;
    device->parent.open        = lwp_device_open;
    device->parent.close       = lwp_device_close;
    device->parent.read        = lwp_device_read;
    device->parent.write       = lwp_device_write;
    device->parent.control     = lwp_device_control;
#endif
    ret = rt_device_register((rt_device_t)device, name, flags);
    if (ret != RT_EOK)
    {
        goto err;
    }
    return RT_EOK;
err:
    if (device->ch_server)
    {
        rt_raw_channel_close(device->ch_server);
    }
    rt_free(device);
    return RT_EIO;
}

rt_err_t lwp_device_unregister(rt_device_t dev)
{
    struct lwp_device *device = (struct lwp_device *)dev;
    struct rt_channel_msg msg;
    rt_sem_t sem = RT_NULL;
    rt_err_t ret = RT_EOK;
    int shmid = -1;
    struct lwp_device_cmd *cmd;

    RT_ASSERT(device->lwp != RT_NULL);
    sem = rt_sem_create("udev", 0, RT_IPC_FLAG_FIFO);
    if (!sem)
    {
        ret = RT_EIO;
        goto out;
    }
    shmid = lwp_shmget((size_t)dev, sizeof(struct lwp_device_cmd), 1);
    if (shmid < 0)
    {
        ret = RT_EIO;
        goto out;
    }
    cmd = (struct lwp_device_cmd*)lwp_shminfo(shmid);
    cmd->client_lwp = lwp_self();
    cmd->reply_sem = sem;
    cmd->cmd = LWP_DEVICE_CMD_QUIT;
    cmd->arg[0] = (void*)device->udev;
    cmd->arg[1] = (void*)RT_DEVICE_CTRL_QUIT;

    msg.type = RT_CHANNEL_RAW;
    msg.u.d = (void*)shmid;
    ret = rt_raw_channel_send(device->ch_server, (void*)&msg);
    if (ret == RT_EOK)
    {
        while (rt_sem_take(sem, RT_WAITING_FOREVER) != RT_EOK);
    }
    ret = (rt_err_t)cmd->ret;
    rt_device_unregister((rt_device_t)device);
out:
    if (shmid >=0)
    {
        lwp_shmrm(shmid);
    }
    if (sem)
    {
        rt_sem_delete(sem);
    }
    rt_raw_channel_close(device->ch_server);
    rt_free(device);
    return ret;
}

