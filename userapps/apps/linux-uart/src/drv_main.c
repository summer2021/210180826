#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/gpio.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <asm/io.h>
#include "MCIMX6Y2.h"
#include "fsl_uart.h"
#include "fsl_iomuxc.h"

#define UART_CNT 1
#define UART_NAME "uart3"

/*#define UART_UCR1_BASE (0x020c406c)
#define UART_UCR2_BASE (0x020c406c)
#define UART_UCR3_BASE (0x020c406c)
#define UART_USR2_BASE ()
#define UART_UFCR_BASE ()
#define UART_UBIR_BASE ()
#define UART_UBMR_BASE ()*/

#define SW_MUX_GPIO1_IO03_BASE (0x020e0068)
#define SW_PAD_GPIO1_IO03_BASE (0x020e02f4)
#define GPIO1_DR_BASE (0x0209c000)
#define GPIO1_GDIR_BASE (0x0209c004)

struct uart_dev
{
    dev_t devid;
    struct cdev cdev;
    struct class *class;
    struct device *device;
    int major;
    int minor;
};

struct uart_dev uart;

UART_Type *UART3_VIRT = NULL;
uint32_t *g_ccm_vbase;
uint32_t *g_ccm_analog_vbase;
uint32_t *g_pmu_vbase;

static void uart_putc(unsigned char c)
{
    while(((UART3_VIRT -> USR2 >> 3) & 0x01) == 0);
    UART3_VIRT -> UTXD = c & 0xff;
}

static int uart_getc()
{
    if(UART3_VIRT -> USR2 & 0x01)
    {
        return (int)UART3_VIRT -> URXD;
    }
    
    return -1;
}

static int uart_open(struct inode *inode,struct file *filp)
{
    filp->private_data = &uart;
    return 0;
}

static ssize_t uart_read(struct file *filp,char __user *buf,size_t cnt,loff_t *offt)
{
    int retvalue;
    unsigned char databuf[100];
    
    if(cnt > sizeof(databuf))
    {
        cnt = sizeof(databuf);
    }

    int i;

    for(i = 0;i < cnt;i++)
    {
        int c = uart_getc();

        if(c == -1)
        {
            cnt = i;
            break;
        }

        databuf[i] = c;
    }

    retvalue = copy_to_user(buf,databuf,cnt);

    if(retvalue < 0)
    {
        printk("kernel read failed!\n");
        return -EFAULT;
    }

    return cnt;
}

static ssize_t uart_write(struct file *filp,const char __user *buf,size_t cnt,loff_t *offt)
{
    int retvalue;
    unsigned char databuf[100];

    if(cnt > sizeof(databuf))
    {
        cnt = sizeof(databuf);
    }
    
    retvalue = copy_from_user(databuf,buf,cnt);

    if(retvalue < 0)
    {
        printk("kernel write failed!\n");
        return -EFAULT;
    }

    int i;
    
    for(i = 0;i < cnt;i++)
    {
        uart_putc(databuf[i]);
    }

    return cnt;
}

static int uart_release(struct inode *inode,struct file *filp)
{
    return 0;
}

static int uart_rt_init()
{
    printk("uart_rt_init\n");
    return 0;
}

static struct file_operations uart_ops =
{
    .owner = THIS_MODULE,
    .open = uart_open,
    .read = uart_read,
    .write = uart_write,
    .release = uart_release,
    .rt_init = uart_rt_init
};

static int __init uart_init(void)
{
    int retvalue = 0;

    if(uart.major)
    {
        uart.devid = MKDEV(uart.major,0);
        register_chrdev_region(uart.devid,UART_CNT,UART_NAME);
    }
    else
    {
        alloc_chrdev_region(&uart.devid,0,UART_CNT,UART_NAME);
        uart.major = MAJOR(uart.devid);
        uart.minor = MINOR(uart.devid);
    }

    printk("uart major=%d,minor=%d\r\n",uart.major,uart.minor);

    uart.cdev.owner = THIS_MODULE;
    cdev_init(&uart.cdev,&uart_ops);

    cdev_add(&uart.cdev,uart.devid,UART_CNT);
    
    uart.class = class_create(THIS_MODULE,UART_NAME);

    if(IS_ERR(uart.class))
    {
        printk("uart.class error\n");
        return PTR_ERR(uart.class);
    }

    uart.device = device_create(uart.class,NULL,uart.devid,NULL,UART_NAME);

    if(IS_ERR(uart.device))
    {
        printk("uart device create error %d\n",(int)uart.device);
        return PTR_ERR(uart.device);
    }

    UART3_VIRT = (UART_Type *)ioremap(UART3_BASE,sizeof(UART_Type));
    g_ccm_vbase = ioremap(0x20C4000U,sizeof(CCM_Type));
    g_ccm_analog_vbase = ioremap(0x20C8000U,sizeof(CCM_ANALOG_Type));
    g_pmu_vbase = ioremap(0x20C8110U,sizeof(PMU_Type));
    IOMUXC_SetPinMux(IOMUXC_UART3_TX_DATA_UART3_TX,0);
    IOMUXC_SetPinMux(IOMUXC_UART3_RX_DATA_UART3_RX,0);
    IOMUXC_SetPinConfig(IOMUXC_UART3_TX_DATA_UART3_TX,0x10b0);
    IOMUXC_SetPinConfig(IOMUXC_UART3_RX_DATA_UART3_RX,0x10b0);

    UART_Disable(UART3_VIRT);
    UART_SoftwareReset(UART3_VIRT);

    UART3_VIRT -> UCR1 = 0;
    UART3_VIRT -> UCR1 &= ~(1 << 14);
    UART3_VIRT -> UCR2 |= (1 << 14) | (1 << 5) | (1 << 2) | (1 << 1);
    UART3_VIRT -> UCR3 |= 1 << 2;

    UART3_VIRT -> UFCR = 5 << 7;
    UART3_VIRT -> UBIR = 71;
    UART3_VIRT -> UBMR = 3124;

    UART_Enable(UART3_VIRT);
    return 0;
}

static void __exit uart_exit(void)
{
    cdev_del(&uart.cdev);
    unregister_chrdev_region(uart.devid,UART_CNT);

    device_destroy(uart.class,uart.devid);
    class_destroy(uart.class);
}

module_init(uart_init);
module_exit(uart_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("lizhirui");