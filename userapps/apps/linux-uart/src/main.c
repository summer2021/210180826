#include <rtthread.h>
#include <stdio.h>
#include <string.h>
#include <drv_syscall.h>

static rt_thread_t thread_uart_read = RT_NULL;
static rt_device_t udev = RT_NULL;

void uart_read_thread_entry()
{
    while(1)
    {
        unsigned char buf;

        if(rt_udevice_read(udev,0,&buf,1) > 0)
        {
            printf("uart received:%c\n",buf);
        }

        rt_thread_mdelay(20);
    }
}

void drv_main()
{
    printf("uart driver is running\n");

    udev = rt_udevice_find("uart3");

    if(udev == RT_NULL)
    {
        printf("uart device find failed!\n");
        return;
    }

    printf("uart device find ok!\n");

    if(rt_udevice_open(udev,RT_DEVICE_OFLAG_RDWR | RT_DEVICE_FLAG_STREAM) != RT_EOK)
    {
        printf("uart device open failed!\n");
        return;
    }

    printf("uart device open ok!\n");

    thread_uart_read = rt_thread_create("uart_read",uart_read_thread_entry,NULL,4096,27,10);

    if(thread_uart_read == RT_NULL)
    {
        printf("uart read thread create failed\n");
        return;
    }

    if(rt_thread_startup(thread_uart_read) != RT_EOK)
    {
        printf("uart read thread startup failed\n");
        return;
    }

    printf("uart read thread create and startup ok\n");

    char *str = "This is a string from linux driver running in RT-Thread Smart in imx6ull.\n";

    while(1)
    {
        rt_udevice_write(udev,0,str,strlen(str));
        rt_thread_mdelay(1000);
    }
}