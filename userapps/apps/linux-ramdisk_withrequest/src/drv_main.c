#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/gpio.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/genhd.h>
#include <linux/blk_types.h>
#include <linux/blkdev.h>
#include <asm/io.h>

#define RAMDISK_SIZE (2 * 1024 * 1024)
#define RAMDISK_NAME "ramdisk"
#define RAMDISK_MINOR 3

struct ramdisk_dev
{
    int major;
    unsigned char *ramdiskbuf;
    spinlock_t lock;
    struct gendisk *gendisk;
    struct request_queue *queue;
};

struct ramdisk_dev ramdisk;

int ramdisk_open(struct block_device *dev,fmode_t mode)
{
    printk("ramdisk open\r\n");
    return 0;
}

void ramdisk_release(struct gendisk *disk,fmode_t mode)
{
    printk("ramdisk release\r\n");
}

int ramdisk_getgeo(struct block_device *dev,struct hd_geometry *geo)
{
    geo -> heads = 2;
    geo -> cylinders = 32;
    geo -> sectors = RAMDISK_SIZE / (2 * 32 * 512);
    return 0;
}

static struct block_device_operations ramdisk_fops = 
{
    .owner = THIS_MODULE,
    .open = ramdisk_open,
    .release = ramdisk_release,
    .getgeo = ramdisk_getgeo,
};

static void ramdisk_transfer(struct request *req)
{
    unsigned long start = blk_rq_pos(req) << 9;
    unsigned long len = blk_rq_cur_bytes(req);

    void *buffer = bio_data(req -> bio);

    if(rq_data_dir(req) == READ)
    {
        printk("read request\r\n");
        memcpy(buffer,ramdisk.ramdiskbuf + start,len);
    }
    else if(rq_data_dir(req) == WRITE)
    {
        printk("write request\r\n");
        memcpy(ramdisk.ramdiskbuf + start,buffer,len);
    }
}

void ramdisk_request_fn(struct request_queue *q)
{
    int err = 0;
    struct request *req;

    req = blk_fetch_request(q);

    while(req != NULL)
    {
        ramdisk_transfer(req);

        if(!__blk_end_request_cur(req,err))
        {
            req = blk_fetch_request(q);
        }
        else
        {
            break;
        }
    }
}

static int __init ramdisk_init()
{
    int ret = 0;

    ramdisk.ramdiskbuf = kzalloc(RAMDISK_SIZE,GFP_KERNEL);

    if(ramdisk.ramdiskbuf == NULL)
    {
        ret = -EINVAL;
        goto ram_fail;
    }

    spin_lock_init(&ramdisk.lock);

    ramdisk.major = register_blkdev(0,RAMDISK_NAME);

    if(ramdisk.major < 0)
    {
        goto register_blkdev_fail;
    }

    printk("ramdisk major = %d\r\n",ramdisk.major);

    ramdisk.gendisk = alloc_disk(RAMDISK_MINOR);

    if(!ramdisk.gendisk)
    {
        ret = -EINVAL;
        goto gendisk_alloc_fail;
    }

    ramdisk.queue = blk_init_queue(ramdisk_request_fn,&ramdisk.lock);

    if(!ramdisk.queue)
    {
        ret = -EINVAL;
        goto blk_init_fail;
    }

    ramdisk.gendisk -> major = ramdisk.major;
    ramdisk.gendisk -> first_minor = 0;
    ramdisk.gendisk -> fops = &ramdisk_fops;
    ramdisk.gendisk -> private_data = &ramdisk;
    ramdisk.gendisk -> queue = ramdisk.queue;
    sprintf(ramdisk.gendisk -> disk_name,RAMDISK_NAME);
    set_capacity(ramdisk.gendisk,RAMDISK_SIZE / 512);
    add_disk(ramdisk.gendisk);
    return 0;

blk_init_fail:
    put_disk(ramdisk.gendisk);
gendisk_alloc_fail:
    unregister_blkdev(ramdisk.major,RAMDISK_NAME);
register_blkdev_fail:
    kfree(ramdisk.ramdiskbuf);
ram_fail:
    return ret;
}

static void __exit ramdisk_exit(void)
{
    del_gendisk(ramdisk.gendisk);
    put_disk(ramdisk.gendisk);

    blk_cleanup_queue(ramdisk.queue);
    unregister_blkdev(ramdisk.major,RAMDISK_NAME);
    kfree(ramdisk.ramdiskbuf);
}

module_init(ramdisk_init);
module_exit(ramdisk_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("zuozhongkai");