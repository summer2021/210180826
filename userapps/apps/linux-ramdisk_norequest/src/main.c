#include <rtthread.h>
#include <stdio.h>
#include <string.h>
#include <drv_syscall.h>

void drv_main()
{
    char send_buf[] = {0xaa,0xcb,0xac,0xdf,0xee,0xa3,0xff,0x3e};
    char recv_buf[sizeof(send_buf)];
    printf("gpio driver is running\n");

    rt_device_t udev;

    udev = rt_udevice_find("ramdisk");

    if(udev == RT_NULL)
    {
        printf("ramdisk device find failed!\n");
        return;
    }

    printf("ramdisk device find ok!\n");

    if(rt_udevice_open(udev,RT_DEVICE_OFLAG_RDWR | RT_DEVICE_FLAG_STREAM) != RT_EOK)
    {
        printf("ramdisk device open failed!\n");
        return;
    }

    printf("ramdisk device open ok!\n");

    unsigned char buf;

    while(1)
    {
        rt_udevice_write(udev,1024,send_buf,sizeof(send_buf));
        rt_udevice_read(udev,1024,recv_buf,sizeof(recv_buf));
        
        if(memcmp(send_buf,recv_buf,sizeof(send_buf)) == 0)
        {
            printf("data is correct!\n");
        }
        else
        {
            printf("data is error!\n");
        }

        rt_thread_mdelay(1000);
    }
}