#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/gpio.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/genhd.h>
#include <linux/blk_types.h>
#include <linux/blkdev.h>
#include <asm/io.h>

#define RAMDISK_SIZE (2 * 1024 * 1024)
#define RAMDISK_NAME "ramdisk"
#define RAMDISK_MINOR 3

struct ramdisk_dev
{
    int major;
    unsigned char *ramdiskbuf;
    spinlock_t lock;
    struct gendisk *gendisk;
    struct request_queue *queue;
};

struct ramdisk_dev ramdisk;

int ramdisk_open(struct block_device *dev,fmode_t mode)
{
    printk("ramdisk open\r\n");
    return 0;
}

void ramdisk_release(struct gendisk *disk,fmode_t mode)
{
    printk("ramdisk release\r\n");
}

int ramdisk_getgeo(struct block_device *dev,struct hd_geometry *geo)
{
    geo -> heads = 2;
    geo -> cylinders = 32;
    geo -> sectors = RAMDISK_SIZE / (2 * 32 * 512);
    return 0;
}

static struct block_device_operations ramdisk_fops = 
{
    .owner = THIS_MODULE,
    .open = ramdisk_open,
    .release = ramdisk_release,
    .getgeo = ramdisk_getgeo,
};

void ramdisk_make_request_fn(struct request_queue *q,struct bio *bio)
{
    int offset;
    struct bio_vec bvec;
    struct bvec_iter iter;
    unsigned long len = 0;
    offset = (bio -> bi_iter.bi_sector) << 9;
    printk("offset = %d\r\n",offset);

    bio_for_each_segment(bvec,bio,iter)
    {
        char *ptr = page_address(bvec.bv_page) + bvec.bv_offset;
        len = bvec.bv_len;
        printk("ptr = %p,len = %d\r\n",ptr,len);

        if(bio_data_dir(bio) == READ)
        {
            memcpy(ptr,ramdisk.ramdiskbuf + offset,len);
        }
        else if(bio_data_dir(bio) == WRITE)
        {
            memcpy(ramdisk.ramdiskbuf + offset,ptr,len);
        }

        offset += len;
    }

    set_bit(BIO_UPTODATE,(unsigned long *)&bio->bi_flags);
    bio_endio(bio,0);
}

static int __init ramdisk_init()
{
    int ret = 0;

    ramdisk.ramdiskbuf = kzalloc(RAMDISK_SIZE,GFP_KERNEL);

    if(ramdisk.ramdiskbuf == NULL)
    {
        ret = -EINVAL;
        goto ram_fail;
    }

    spin_lock_init(&ramdisk.lock);

    ramdisk.major = register_blkdev(0,RAMDISK_NAME);

    if(ramdisk.major < 0)
    {
        goto register_blkdev_fail;
    }

    printk("ramdisk major = %d\r\n",ramdisk.major);

    ramdisk.gendisk = alloc_disk(RAMDISK_MINOR);

    if(!ramdisk.gendisk)
    {
        ret = -EINVAL;
        goto gendisk_alloc_fail;
    }

    ramdisk.queue = blk_alloc_queue(GFP_KERNEL);

    if(!ramdisk.queue)
    {
        ret = -EINVAL;
        goto blk_allo_fail;
    }

    blk_queue_make_request(ramdisk.queue,ramdisk_make_request_fn);

    ramdisk.gendisk -> major = ramdisk.major;
    ramdisk.gendisk -> first_minor = 0;
    ramdisk.gendisk -> fops = &ramdisk_fops;
    ramdisk.gendisk -> private_data = &ramdisk;
    ramdisk.gendisk -> queue = ramdisk.queue;
    sprintf(ramdisk.gendisk -> disk_name,RAMDISK_NAME);
    set_capacity(ramdisk.gendisk,RAMDISK_SIZE / 512);
    add_disk(ramdisk.gendisk);
    return 0;

blk_allo_fail:
    put_disk(ramdisk.gendisk);
gendisk_alloc_fail:
    unregister_blkdev(ramdisk.major,RAMDISK_NAME);
register_blkdev_fail:
    kfree(ramdisk.ramdiskbuf);
ram_fail:
    return ret;
}

static void __exit ramdisk_exit(void)
{
    del_gendisk(ramdisk.gendisk);
    put_disk(ramdisk.gendisk);

    blk_cleanup_queue(ramdisk.queue);
    unregister_blkdev(ramdisk.major,RAMDISK_NAME);
    kfree(ramdisk.ramdiskbuf);
}

module_init(ramdisk_init);
module_exit(ramdisk_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("zuozhongkai");