#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <drv_syscall.h>

/////////////////////////////
#define __REG32(x)  (*((volatile unsigned int *)(x)))
#define __REG16(x)  (*((volatile unsigned short *)(x)))
#define IRQ_PBA8_GIC_START          32

/*
 * PB-A8 on-board gic irq sources
 */ 
#define IRQ_PBA8_TIMER0_1   (IRQ_PBA8_GIC_START + 2)    /* Timer 0/1 (default timer) */
#define IRQ_PBA8_TIMER2_3   (IRQ_PBA8_GIC_START + 3)    /* Timer 2/3 */

#define TIMER01_HW_BASE_PHY                 0x10011000
#define TIMER23_HW_BASE_PHY                 0x10012000

void* timer01_hw_base;
void* timer23_hw_base;

#define TIMER01_HW_BASE timer01_hw_base
#define TIMER23_HW_BASE timer23_hw_base

#define TIMER_LOAD(hw_base)             __REG32(hw_base + 0x00)
#define TIMER_VALUE(hw_base)            __REG32(hw_base + 0x04)
#define TIMER_CTRL(hw_base)             __REG32(hw_base + 0x08)
#define TIMER_CTRL_ONESHOT              (1 << 0)
#define TIMER_CTRL_32BIT                (1 << 1)
#define TIMER_CTRL_DIV1                 (0 << 2)
#define TIMER_CTRL_DIV16                (1 << 2)
#define TIMER_CTRL_DIV256               (2 << 2)
#define TIMER_CTRL_IE                   (1 << 5)        /* Interrupt Enable (versatile only) */
#define TIMER_CTRL_PERIODIC             (1 << 6)
#define TIMER_CTRL_ENABLE               (1 << 7)

#define TIMER_INTCLR(hw_base)           __REG32(hw_base + 0x0c)
#define TIMER_RIS(hw_base)              __REG32(hw_base + 0x10)
#define TIMER_MIS(hw_base)              __REG32(hw_base + 0x14)
#define TIMER_BGLOAD(hw_base)           __REG32(hw_base + 0x18)

#define TIMER_LOAD(hw_base)             __REG32(hw_base + 0x00)
#define TIMER_VALUE(hw_base)            __REG32(hw_base + 0x04)
#define TIMER_CTRL(hw_base)             __REG32(hw_base + 0x08)
#define TIMER_CTRL_ONESHOT              (1 << 0)
#define TIMER_CTRL_32BIT                (1 << 1)
#define TIMER_CTRL_DIV1                 (0 << 2)
#define TIMER_CTRL_DIV16                (1 << 2)
#define TIMER_CTRL_DIV256               (2 << 2)
#define TIMER_CTRL_IE                   (1 << 5)        /* Interrupt Enable (versatile only) */
#define TIMER_CTRL_PERIODIC             (1 << 6)
#define TIMER_CTRL_ENABLE               (1 << 7)

#define TIMER_INTCLR(hw_base)           __REG32(hw_base + 0x0c)
#define TIMER_RIS(hw_base)              __REG32(hw_base + 0x10)
#define TIMER_MIS(hw_base)              __REG32(hw_base + 0x14)
#define TIMER_BGLOAD(hw_base)           __REG32(hw_base + 0x18)

void* sys_ctrl;
#define SYS_CTRL                        __REG32(sys_ctrl)
void* timer_hw_base;
#define TIMER_HW_BASE                   timer_hw_base

volatile uint32_t cnt;
static void rt_hw_timer_isr(int vector, void *param)
{
    /* clear interrupt */
    TIMER_INTCLR(TIMER_HW_BASE) = 0x01;
    cnt++;
}

/////////////////////////////

struct demo_dev
{
    struct rt_device parent;
    rt_device_t udev;
    void *io;
    int data;
    char buff[256];
};

static rt_err_t  demo_device_init(rt_device_t dev)
{
    printf("%s %d\n", __func__, __LINE__);
    return RT_EOK;
}

static rt_err_t  demo_device_open(rt_device_t dev, rt_uint16_t oflag)
{
    printf("%s %d flag = 0x%x\n", __func__, __LINE__, oflag);
    return RT_EOK;
}

static rt_err_t  demo_device_close(rt_device_t dev)
{
    printf("%s %d\n", __func__, __LINE__);
    return RT_EOK;
}

static rt_size_t demo_device_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
    struct demo_dev *device = (struct demo_dev *)dev;
    void* client = rt_device_client_get();
    struct rt_device_reply r;

    rt_device_reply_get(&r);
    if (size > 256)
    {
        size = 256;
    }
    printf("%s %d pos = %ld, lwp = %p, buffer = %p, size = %lu\n", __func__, __LINE__, pos, client, buffer, size);
    size = rt_data_put(device->udev, client, buffer, device->buff, size);
    rt_device_reply_ack(&r, size);
    return 0;
}

static rt_size_t demo_device_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    struct demo_dev *device = (struct demo_dev *)dev;
    void* client = rt_device_client_get();

    if (size > 256)
    {
        size = 256;
    }
    printf("%s %d pos = %ld, lwp = %p, buffer = %p, size = %lu\n", __func__, __LINE__, pos, client, buffer, size);
    size = rt_data_get(device->udev, client, device->buff, buffer, size);
    return size;
}

static rt_err_t  demo_device_control(rt_device_t dev, int cmd, void *args)
{
    struct demo_dev *device = (struct demo_dev *)dev;

    if (cmd == RT_DEVICE_CTRL_QUIT)
    {
        /* timer test finish */
        /* disable timer */
        TIMER_CTRL(TIMER_HW_BASE) &= ~TIMER_CTRL_ENABLE;

        /* uninstall interrupt */
        rt_udevice_interrupt_install(device->udev, IRQ_PBA8_TIMER0_1, NULL, NULL);

        /* unmap timer io */
        rt_iounremap(device->udev, device->io, 0x1000);
        printf("%s %d\n", __func__, __LINE__);
    }
    else
    {
        device->data = (int)(size_t)args;
        printf("%s %d, data = %d\n", __func__, __LINE__, device->data);
    }
    return RT_EOK;
}

#ifdef RT_USING_DEVICE_OPS
static const struct rt_device_ops demo_device_ops =
{
    demo_device_init,
    demo_device_open,
    demo_device_close,
    demo_device_read,
    demo_device_write,
    demo_device_control
};
#endif

struct demo_dev dev;

char buff[256];

int main(int argc, char *argv[])
{
    rt_device_t device;
    rt_err_t ret;
    struct rt_device *udev;
    int i;

    /* register */
    device = (rt_device_t)&dev.parent;
#ifdef RT_USING_DEVICE_OPS
    device->ops = &demo_device_ops;
#else
    device->init        = demo_device_init;
    device->open        = demo_device_open;
    device->close       = demo_device_close;
    device->read        = demo_device_read;
    device->write       = demo_device_write;
    device->control     = demo_device_control;
#endif

    printf("user device demo\n");
    ret = rt_udevice_register((rt_device_t)&dev, "demodev", RT_DEVICE_FLAG_RDWR);
    printf("rt_device_register ret = %d\n", (int)ret);
    if (ret != RT_EOK)
    {
        printf("Error! rt_device_register failed\n");
        return 0;
    }

    /* test ops */
    udev = rt_udevice_find("demodev");
    dev.udev = udev;

    {
        int op_ret;

        for (i = 0; i < 256; i++)
        {
            buff[i] = (char)i;
        }
        printf("udev = %p\n", udev);
        op_ret = rt_udevice_open(udev, RT_DEVICE_OFLAG_RDWR | RT_DEVICE_FLAG_STREAM);
        printf("open ret = %d\n", op_ret);
        op_ret = rt_udevice_control(udev, 0, (void*)100);
        printf("control ret = %d\n", op_ret);
        op_ret = rt_udevice_write(udev, 0, buff, 256);
        printf("write ret = %d\n", op_ret);
        op_ret = rt_udevice_read(udev, 0, buff + 128, 128);
        printf("read ret = %d\n", op_ret);
        for (i = 0; i < 256; i++)
        {
            if ((i & 0xf) == 0)
            {
                printf("0x%02x:", i);
            }
            printf(" %02x", buff[i]);
            if ((i & 0xf) == 0xf)
            {
                printf("\n");
            }
        }
        rt_udevice_close(udev);
    }
    /* test for timer0_1 */
    {
        rt_uint32_t val;

        dev.io = rt_ioremap(udev, (void*)TIMER01_HW_BASE_PHY, 0x1000, 0); /* qemu timer0_1 */

        timer_hw_base = dev.io;
        /* Setup Timer0 for generating irq */
        val = TIMER_CTRL(TIMER_HW_BASE);
        val &= ~TIMER_CTRL_ENABLE;
        val |= (TIMER_CTRL_32BIT | TIMER_CTRL_PERIODIC | TIMER_CTRL_IE);
        TIMER_CTRL(TIMER_HW_BASE) = val;

        TIMER_LOAD(TIMER_HW_BASE) = 1000000/RT_TICK_PER_SECOND;

        /* enable timer */
        TIMER_CTRL(TIMER_HW_BASE) |= TIMER_CTRL_ENABLE;

        /* install interrupt */
        rt_udevice_interrupt_install(udev, IRQ_PBA8_TIMER0_1, rt_hw_timer_isr, NULL);
        /* unmask interrupt */
        rt_udevice_interrupt_umask(udev, IRQ_PBA8_TIMER0_1);

        /* check timer count count */
        printf("check timer count (10s)...\n");
        for (i = 0; i < 10; i++)
        {
            printf("timer count value = %08x\n", cnt);
            rt_thread_mdelay(1000);
        }
        /* timer test finish */
    }
    /* unregister */
    rt_udevice_unregister(udev);
    printf("udev demo finish\n");

    return 0;
}

