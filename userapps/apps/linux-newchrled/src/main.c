#include <rtthread.h>
#include <stdio.h>
#include <drv_syscall.h>

void drv_main()
{
    printf("gpio driver is running\n");

    rt_device_t udev;

    udev = rt_udevice_find("newchrled");

    if(udev == RT_NULL)
    {
        printf("newchrled device find failed!\n");
        return;
    }

    printf("newchrled device find ok!\n");

    if(rt_udevice_open(udev,RT_DEVICE_OFLAG_RDWR | RT_DEVICE_FLAG_STREAM) != RT_EOK)
    {
        printf("newchrled device open failed!\n");
        return;
    }

    printf("newchrled device open ok!\n");

    unsigned char buf;

    while(1)
    {
        buf = 0;
        rt_udevice_write(udev,0,&buf,1);
        rt_thread_mdelay(500);
        buf = 1;
        rt_udevice_write(udev,0,&buf,1);
        rt_thread_mdelay(500);
    }
}