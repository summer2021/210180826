import os
import sys

script,inputfile,outputfile,arrayname = sys.argv

fin = open(inputfile,'rb')
fin.seek(0,0)
filedata = fin.read()
fin.close()

fout = open(outputfile,'wb')
fout.write('static const unsigned char ' + arrayname + '[] = \n')
fout.write('{\n')

for i in range(0,len(filedata)):
    if (i % 16) == 0:
        fout.write('    ')

    fout.write('0x%.2x' % ord(filedata[i]))

    if i < (len(filedata) - 1):
        fout.write(',')

    if ((i % 16) == 15) or (i == (len(filedata) - 1)):
        fout.write('\n')

fout.write('};\n')
fout.close()