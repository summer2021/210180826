# dde-usb

Device Driver Enviroment - USB stack






## 附
* kconfig参考.imx

```
menuconfig PKG_USING_LINUX_USB
    bool "linux-usb"
    default y

    config PKG_USING_LINUX_KL
        bool
        default y

    if PKG_USING_LINUX_USB
        config LINUX_USB_GADGET_ENABLE
            bool "gadget"
            default y

        config GADGET_LG_SERIAL_ENABLE
            bool "legacy serial"
            default y

        config GADGET_FSL_USB2_ENABLE
            bool "fsl usb2"
            default y

        config GADGET_USB_TEST_ENABLE
            bool "test app"
            default y
        config GADGET_USB_TEST_FSL_USB2_ENABLE
            bool "test driver"
            default y

    endif
```
