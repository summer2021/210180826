#ifndef __IMX_IOREMAP_H__
#define __IMX_IOREMAP_H__
    #include <linux/types.h>
    #include <linux/string.h>
    #include <linux/compiler.h>

    #define ioremap imx_ioremap
    static inline void *imx_ioremap(phys_addr_t offset, size_t size)
    {
        return (void *)offset;
    }

    #define iounmap imx_iounmap
    static inline void imx_iounmap(volatile void __iomem *addr)
    {
        
    }
#endif