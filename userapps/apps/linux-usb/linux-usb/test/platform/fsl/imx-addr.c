#include <rtthread.h>
#include <MCIMX6Y2.h>
#include <drv_syscall.h>
#include <stdio.h>

rt_size_t syscall(rt_size_t nr,...);

#define SYS_mmap2 53

static void *mmap2(void *addr,size_t length,int prot,int flags,int fd,off_t pgoffset)
{
    return (void *)syscall(SYS_mmap2,addr,length,prot,flags,fd,pgoffset);
}

void *imx_addr_usbphy1 = 0;
void *imx_addr_usbphy2 = 0;
void *imx_addr_usb1 = 0;
void *imx_addr_usb2 = 0;
void *imx_addr_usb_analog = 0;

uint32_t *g_ccm_vbase;
uint32_t *g_ccm_analog_vbase;
uint32_t *g_pmu_vbase;

#define ADDR_CHECK(addr) do{if((addr) == ((void *)-1)){return -1;}}while(0)

typedef struct rt_udevice
{
    struct rt_device parent;
    rt_device_t udev;
    void *io;
    int data;
    char buff[256];
}*rt_udevice_t;

static rt_err_t udevice_init(rt_device_t dev)
{
    //printf("%s %d\n", __func__, __LINE__);
    return RT_EOK;
}

static rt_err_t udevice_open(rt_device_t dev, rt_uint16_t oflag)
{
    //printf("%s %d flag = 0x%x\n", __func__, __LINE__, oflag);
    return RT_EOK;
}

static rt_err_t udevice_close(rt_device_t dev)
{
    //printf("%s %d\n", __func__, __LINE__);
    return RT_EOK;
}

static rt_size_t udevice_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
    rt_udevice_t device = (rt_udevice_t)dev;
    void* client = rt_device_client_get();
    struct rt_device_reply r;

    rt_device_reply_get(&r);
    if (size > 256)
    {
        size = 256;
    }
    //printf("%s %d pos = %ld, lwp = %p, buffer = %p, size = %lu\n", __func__, __LINE__, pos, client, buffer, size);
    size = rt_data_put(device->udev, client, buffer, device->buff, size);
    rt_device_reply_ack(&r, size);
    return 0;
}

static rt_size_t udevice_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    rt_udevice_t device = (rt_udevice_t)dev;
    void* client = rt_device_client_get();

    if (size > 256)
    {
        size = 256;
    }
    //printf("%s %d pos = %ld, lwp = %p, buffer = %p, size = %lu\n", __func__, __LINE__, pos, client, buffer, size);
    size = rt_data_get(device->udev, client, device->buff, buffer, size);
    return size;
}

static rt_err_t udevice_control(rt_device_t dev, int cmd, void *args)
{
    rt_udevice_t device = (rt_udevice_t )dev;

    if (cmd == RT_DEVICE_CTRL_QUIT)
    {
        //printf("%s %d\n", __func__, __LINE__);
    }
    else
    {
        device->data = (int)(size_t)args;
        //printf("%s %d, data = %d\n", __func__, __LINE__, device->data);
    }
    return RT_EOK;
}

#ifdef RT_USING_DEVICE_OPS
static const struct rt_device_ops demo_device_ops =
{
    udevice_init,
    udevice_open,
    udevice_close,
    udevice_read,
    udevice_write,
    udevice_control
};
#endif

static struct rt_udevice dev;

static int imx_addr_init(void)
{
    rt_device_t device;
    rt_err_t ret;
    struct rt_device *udev;

    /* register */
    device = (rt_device_t)&dev.parent;
#ifdef RT_USING_DEVICE_OPS
    device->ops = &demo_device_ops;
#else
    device->init        = demo_device_init;
    device->open        = demo_device_open;
    device->close       = demo_device_close;
    device->read        = demo_device_read;
    device->write       = demo_device_write;
    device->control     = demo_device_control;
#endif

    ret = rt_udevice_register((rt_device_t)&dev, "imxusb", RT_DEVICE_FLAG_RDWR);

    //printf("rt_device_register ret = %d\n", (int)ret);

    if (ret != RT_EOK)
    {
        printf("Error! rt_device_register failed\n");
        return 0;
    }

    udev = rt_udevice_find("imxusb");
    rt_udevice_set_current(udev);
    dev.udev = udev;

    imx_addr_usbphy1 = rt_ioremap(udev,(void *)USBPHY1_PHYBASE,sizeof(USBPHY_Type),0);
    imx_addr_usbphy2 = rt_ioremap(udev,(void *)USBPHY2_PHYBASE,sizeof(USBPHY_Type),0);
    imx_addr_usb1 = rt_ioremap(udev,(void *)USB1_PHYBASE,sizeof(USB_Type),0);
    imx_addr_usb2 = rt_ioremap(udev,(void *)USB2_PHYBASE,sizeof(USB_Type),0);
    imx_addr_usb_analog = rt_ioremap(udev,(void *)USB_ANALOG_PHYBASE,sizeof(USB_ANALOG_Type),0);
    g_ccm_vbase = rt_ioremap(udev,(void *)CCM_PHYBASE,sizeof(CCM_Type),0);
    g_ccm_analog_vbase = rt_ioremap(udev,(void *)CCM_ANALOG_PHYBASE,sizeof(CCM_ANALOG_Type),0);
    g_pmu_vbase = rt_ioremap(udev,(void *)PMU_PHYBASE,sizeof(PMU_Type),0);

    ADDR_CHECK(imx_addr_usbphy1);
    ADDR_CHECK(imx_addr_usbphy2);
    ADDR_CHECK(imx_addr_usb1);
    ADDR_CHECK(imx_addr_usb2);
    ADDR_CHECK(imx_addr_usb_analog);
    ADDR_CHECK(g_ccm_vbase);
    ADDR_CHECK(g_ccm_analog_vbase);
    ADDR_CHECK(g_pmu_vbase);
    return 0;
}
INIT_PREV_EXPORT(imx_addr_init);