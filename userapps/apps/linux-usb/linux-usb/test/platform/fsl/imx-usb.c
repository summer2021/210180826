#include <rtthread.h>
#include <rtdevice.h>
#include <drv_syscall.h>

#include <linux/platform_device.h>
#include <linux/platform_data/fsl_devices.h>
#include <linux/interrupt.h>
#include <linux/completion.h>

#include <fsl_common.h>
#include <fsl_clock.h>
#include "MCIMX6Y2.h"

#include <stdio.h>

extern int fsl_udc_probe(struct platform_device *pdev);

typedef struct _usb_phy_config_struct
{
    uint8_t D_CAL;     /* Decode to trim the nominal 17.78mA current source */
    uint8_t TXCAL45DP; /* Decode to trim the nominal 45-Ohm series termination resistance to the USB_DP output pin */
    uint8_t TXCAL45DM; /* Decode to trim the nominal 45-Ohm series termination resistance to the USB_DM output pin */
} usb_phy_config_struct_t;

/* USB PHY condfiguration */
#define BOARD_USB_PHY_D_CAL (0x0CU)
#define BOARD_USB_PHY_TXCAL45DP (0x06U)
#define BOARD_USB_PHY_TXCAL45DM (0x06U)

static void *EhciPhyGetBase(uint8_t controllerId)
{
    void *usbPhyBase = NULL;
#if ((defined FSL_FEATURE_SOC_USBPHY_COUNT) && (FSL_FEATURE_SOC_USBPHY_COUNT > 0U))
    uint32_t instance;
    uint32_t newinstance = 0;
    uint32_t usbphy_base_temp[] = USBPHY_BASE_ADDRS;
    uint32_t usbphy_base[] = USBPHY_BASE_ADDRS;

    if (controllerId > 1)
    {
        return NULL;
    }

    for (instance = 0; instance < (sizeof(usbphy_base_temp) / sizeof(usbphy_base_temp[0])); instance++)
    {
        if (usbphy_base_temp[instance])
        {
            usbphy_base[newinstance++] = usbphy_base_temp[instance];
        }
    }
    if (controllerId > newinstance)
    {
        return NULL;
    }

    usbPhyBase = (void *)usbphy_base[controllerId];
#endif
    return usbPhyBase;
}

static int EhciPhyInit(uint8_t controllerId, uint32_t freq, usb_phy_config_struct_t *phyConfig)
{
#if ((defined FSL_FEATURE_SOC_USBPHY_COUNT) && (FSL_FEATURE_SOC_USBPHY_COUNT > 0U))
    USBPHY_Type *usbPhyBase;

    usbPhyBase = (USBPHY_Type *)EhciPhyGetBase(controllerId);
    if (NULL == usbPhyBase)
    {
        return -1;
    }
    
#if ((defined FSL_FEATURE_SOC_ANATOP_COUNT) && (FSL_FEATURE_SOC_ANATOP_COUNT > 0U))
    ANATOP->HW_ANADIG_REG_3P0.RW =
        (ANATOP->HW_ANADIG_REG_3P0.RW &
         (~(ANATOP_HW_ANADIG_REG_3P0_OUTPUT_TRG(0x1F) | ANATOP_HW_ANADIG_REG_3P0_ENABLE_ILIMIT_MASK))) |
        ANATOP_HW_ANADIG_REG_3P0_OUTPUT_TRG(0x17) | ANATOP_HW_ANADIG_REG_3P0_ENABLE_LINREG_MASK;
    ANATOP->HW_ANADIG_USB2_CHRG_DETECT.SET =
        ANATOP_HW_ANADIG_USB2_CHRG_DETECT_CHK_CHRG_B_MASK | ANATOP_HW_ANADIG_USB2_CHRG_DETECT_EN_B_MASK;
#endif

#if (defined USB_ANALOG)
    USB_ANALOG->INSTANCE[controllerId].CHRG_DETECT_SET = USB_ANALOG_CHRG_DETECT_CHK_CHRG_B(1) | USB_ANALOG_CHRG_DETECT_EN_B(1);
#endif
    
#if ((!(defined FSL_FEATURE_SOC_CCM_ANALOG_COUNT)) && (!(defined FSL_FEATURE_SOC_ANATOP_COUNT)))

    usbPhyBase->TRIM_OVERRIDE_EN = 0x001fU; /* override IFR value */
#endif
    usbPhyBase->CTRL |= USBPHY_CTRL_SET_ENUTMILEVEL2_MASK; /* support LS device. */
    usbPhyBase->CTRL |= USBPHY_CTRL_SET_ENUTMILEVEL3_MASK; /* support external FS Hub with LS device connected. */
    /* PWD register provides overall control of the PHY power state */
    usbPhyBase->PWD = 0U;

    /* Decode to trim the nominal 17.78mA current source for the High Speed TX drivers on USB_DP and USB_DM. */
    usbPhyBase->TX =
        ((usbPhyBase->TX & (~(USBPHY_TX_D_CAL_MASK | USBPHY_TX_TXCAL45DM_MASK | USBPHY_TX_TXCAL45DP_MASK))) |
         (USBPHY_TX_D_CAL(phyConfig->D_CAL) | USBPHY_TX_TXCAL45DP(phyConfig->TXCAL45DP) |
          USBPHY_TX_TXCAL45DM(phyConfig->TXCAL45DM)));
#endif

    return 0;
}

static int ehci0_init(struct fsl_usb2_platform_data *fpd)
{
    usb_phy_config_struct_t phyConfig = {
        BOARD_USB_PHY_D_CAL, BOARD_USB_PHY_TXCAL45DP, BOARD_USB_PHY_TXCAL45DM,
    };

    CLOCK_EnableUsbhs0PhyPllClock(kCLOCK_Usbphy480M, 480000000U);
    CLOCK_EnableUsbhs0Clock(kCLOCK_Usb480M, 480000000U);
    EhciPhyInit(0, 0, &phyConfig);

    return 0;
}

static void fpd_setup(struct fsl_usb2_platform_data *fpd, struct resource *rs, int id)
{
    fpd->operating_mode = FSL_USB2_DR_DEVICE;
    fpd->phy_mode = FSL_USB2_PHY_UTMI;

    if (id == 0)
    {
        fpd->init = (void *)ehci0_init;
        rs[0].start = USB1_BASE;
        rs[1].start = USB_OTG1_IRQn;
    }
}

static int imx_usb_init(void)
{
    static struct platform_device pdev = {0};
    static struct device_driver dd = {
        .name = "fsl-usb",
    };
    static struct resource rs[] = {
        {.start = 0, .flags = IORESOURCE_MEM,},
        {.start = 0, .flags = IORESOURCE_IRQ,},
    };
    static struct fsl_usb2_platform_data fpd = {0};

    fpd_setup(&fpd, rs, 0);

    pdev.num_resources = ARRAY_SIZE(rs);
    pdev.resource = rs;
    pdev.dev.driver = &dd;
    pdev.dev.platform_data = &fpd;

    INIT_LIST_HEAD(&pdev.dev.dma_pools);
    rt_kprintf("ready to probe\n");
    fsl_udc_probe(&pdev);

    return 0;
}
INIT_DEVICE_EXPORT(imx_usb_init);

struct imxirq
{
    int irq;
    irq_handler_t isr;
    void *param;
    struct rt_completion wait;
    struct rt_thread *thd;
};

static struct imxirq _usb0irq = {0};

static void irq_thread(void *p)
{
    struct imxirq *ir = (struct imxirq *)p;

    while (1)
    {
        rt_completion_wait(&ir->wait, -1);

        if (ir->isr)
            ir->isr(ir->irq, ir->param);

        EnableIRQ(ir->irq);
    }
}

#ifndef MCIMX6Y2_SERIES
void USB_OTG1_IRQHandler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();
    
    if (_usb0irq.isr)
    {
        _usb0irq.isr(_usb0irq.irq, _usb0irq.param);
    }
    /* leave interrupt */
    rt_interrupt_leave();
}

int irq_hw_register(unsigned int irq, irq_handler_t handler, void *id)
{
    _usb0irq.irq = irq;
    _usb0irq.param = id;
    _usb0irq.isr = handler;

    EnableIRQ(irq);

    return 0;
}
#else
static int usb0_isr(int irq, void *p)
{
    DisableIRQ(_usb0irq.irq);
    rt_completion_done(&_usb0irq.wait);
    printf("usb0_isr\n");
    return 0;
}

int irq_hw_register(unsigned int irq, irq_handler_t handler, void *id)
{
    struct imxirq *ir = &_usb0irq;

    ir->irq = irq;
    ir->param = id;
    ir->isr = handler;

    rt_completion_init(&ir->wait);
    ir->thd = rt_thread_create("usbirq", irq_thread, ir, 4096, 1, 50);
    rt_thread_startup(ir->thd);

    rt_udevice_interrupt_install(rt_udevice_get_current(),irq, (void *)usb0_isr, id);
    EnableIRQ(irq);

    return 0;
}
#endif
