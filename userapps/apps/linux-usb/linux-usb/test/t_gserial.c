#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include <rtthread.h>

static int gsread(int argc, char **argv)
{
    int fd;
    int ret;
    char str[13] = {0};
    int cnt = 10;

    fd = open("/dev/ttyGS0", O_RDWR);
    if (fd < 0)
    {
        printf("open fail\n");
        return -1;
    }

    while ((cnt --) > 0)
    {
        ret = read(fd, str, sizeof(str) - 1);
        if (ret <= 0)
        {
            printf("read fail\n");
            break;
        }

        str[ret] = 0;
        printf("RX(%d): %s\n", cnt, str);
    }

    close(fd);

    return 0;
}
MSH_CMD_EXPORT(gsread, general serial read);

static int gswrite(int argc, char **argv)
{
    int fd;
    int ret;
    int cnt = 20;
    const char *str = "0123456789\r\n";

    fd = open("/dev/ttyGS0", O_RDWR);
    if (fd < 0)
    {
        printf("open fail\n");
        return -1;
    }

    while (cnt-- > 0)
    {
        ret = write(fd, str, strlen(str));
        if (ret < 1)
        {
            printf("ret(%d)\n", ret);
            break;
        }
    }

    close(fd);

    return 0;
}
MSH_CMD_EXPORT(gswrite, general serial write);
