#include <linux/device.h>
#include <linux/slab.h>
#include <linux/export.h>
#include <linux/module.h>
#include <linux/workqueue.h>
#include <linux/kfifo.h>

#include "u_serial.h"

#include <dfs_fs.h>
#include <dfs_file.h>
#include <dfs_poll.h>

struct tty_struct {
    struct rt_ringbuffer *rxfifo;
    struct rt_wqueue rxwq;
    struct rt_wqueue txwq;
};

struct tty_port {
    struct rt_device parent;
    struct tty_struct *tty;

    struct device dev;

    struct tty_struct _tty;
};

struct gs_port {
    struct tty_port port;

    spinlock_t		port_lock;	/* guard port_* access */

    struct gserial		*port_usb;
    u8			port_num;

    struct list_head	write_pool;
    int write_started;
    int write_allocated;
    bool                    write_busy;
    struct kfifo		port_write_buf;

    struct list_head	read_queue;
    struct list_head	read_pool;
    int read_started;
    int read_allocated;
    unsigned		n_read;
    struct delayed_work	push;

    wait_queue_head_t	drain_wait;
    wait_queue_head_t	close_wait;

    bool			suspended;	/* port suspended */
    bool start_delayed;

    struct usb_cdc_line_coding port_line_coding;
};

static struct portmaster {
    struct mutex	lock;			/* protect open/close */
    struct gs_port	*port;
} ports[MAX_U_SERIAL_PORTS];

#define QUEUE_SIZE		4
#define WRITE_BUF_SIZE		128		/* TX only */

#ifdef VERBOSE_DEBUG
#ifndef pr_vdebug
#define pr_vdebug(fmt, arg...) \
    pr_debug(fmt, ##arg)
#endif /* pr_vdebug */
#else
#ifndef pr_vdebug
#define pr_vdebug(fmt, arg...) \
    ({ if (0) pr_debug(fmt, ##arg); })
#endif /* pr_vdebug */
#endif

/***************************/
static struct device* tty_port_register_device(struct tty_port *port, int index, void *priv);

static int tty_port_init(struct tty_port *port)
{
    port->tty = NULL;

    port->_tty.rxfifo = rt_ringbuffer_create(128);
    if (!port->_tty.rxfifo)
        return -1;

    rt_wqueue_init(&port->_tty.rxwq);
    rt_wqueue_init(&port->_tty.txwq);

    return 0;
}

static bool tty_throttled(struct tty_struct *tty)
{
    if (rt_ringbuffer_space_len(tty->rxfifo) == 0)
        return true;

    return false;
}

static int tty_insert_flip_string(struct tty_port *port,
        const unsigned char *chars, size_t size)
{
    return rt_ringbuffer_put(port->tty->rxfifo, chars, size);
}

static void tty_flip_buffer_push(struct tty_port *port)
{
    rt_wqueue_wakeup(&port->tty->rxwq, (void *)POLLIN);
}

static void tty_wakeup(struct tty_struct *tty)
{
    rt_wqueue_wakeup(&tty->txwq, (void *)POLLOUT);
}

/*-----------------------------------------------------------*/
static unsigned gs_start_rx(struct gs_port *port);

static int gs_alloc_requests(struct usb_ep *ep, struct list_head *head,
        void (*fn)(struct usb_ep *, struct usb_request *),
        int *allocated)
{
    int			i;
    struct usb_request	*req;
    int n = allocated ? QUEUE_SIZE - *allocated : QUEUE_SIZE;

    /* Pre-allocate up to QUEUE_SIZE transfers, but if we can't
     * do quite that many this time, don't fail ... we just won't
     * be as speedy as we might otherwise be.
     */
    for (i = 0; i < n; i++) {
        req = gs_alloc_req(ep, ep->maxpacket, GFP_ATOMIC);
        if (!req)
            return list_empty(head) ? -ENOMEM : 0;
        req->complete = fn;
        list_add_tail(&req->list, head);
        if (allocated)
            (*allocated)++;
    }
    return 0;
}

static void gs_free_requests(struct usb_ep *ep, struct list_head *head,
                             int *allocated)
{
    struct usb_request	*req;

    while (!list_empty(head)) {
        req = list_entry(head->next, struct usb_request, list);
        list_del(&req->list);
        gs_free_req(ep, req);
        if (allocated)
            (*allocated)--;
    }
}

/*
 * gs_send_packet
 *
 * If there is data to send, a packet is built in the given
 * buffer and the size is returned.  If there is no data to
 * send, 0 is returned.
 *
 * Called with port_lock held.
 */
static unsigned gs_send_packet(struct gs_port *port, char *packet, unsigned size)
{
	unsigned len;

	len = kfifo_len(&port->port_write_buf);
	if (len < size)
		size = len;
	if (size != 0)
		size = kfifo_out(&port->port_write_buf, packet, size);
	return size;
}

/*
 * RX tasklet takes data out of the RX queue and hands it up to the TTY
 * layer until it refuses to take any more data (or is throttled back).
 * Then it issues reads for any further data.
 *
 * If the RX queue becomes full enough that no usb_request is queued,
 * the OUT endpoint may begin NAKing as soon as its FIFO fills up.
 * So QUEUE_SIZE packets plus however many the FIFO holds (usually two)
 * can be buffered before the TTY layer's buffers (currently 64 KB).
 */
static void gs_rx_push(struct work_struct *work)
{
    struct delayed_work	*w = to_delayed_work(work);
    struct gs_port		*port = container_of(w, struct gs_port, push);
    struct tty_struct	*tty;
    struct list_head	*queue = &port->read_queue;
    bool			disconnect = false;
    bool			do_push = false;
    printf("gs_rx_push\n");
    /* hand any queued data to the tty */
    spin_lock_irq(&port->port_lock);
    tty = port->port.tty;
    while (!list_empty(queue)) {
        struct usb_request	*req;

        req = list_first_entry(queue, struct usb_request, list);

        /* leave data queued if tty was rx throttled */
        if (tty && tty_throttled(tty))
            break;

        switch (req->status) {
        case -ESHUTDOWN:
            disconnect = true;
            pr_vdebug("ttyGS%d: shutdown\n", port->port_num);
            break;

        default:
            /* presumably a transient fault */
            pr_warn("ttyGS%d: unexpected RX status %d\n",
                port->port_num, req->status);
            fallthrough;
        case 0:
            /* normal completion */
            break;
        }

        /* push data to (open) tty */
        if (req->actual && tty) {
            char		*packet = req->buf;
            unsigned	size = req->actual;
            unsigned	n;
            int		count;

            /* we may have pushed part of this packet already... */
            n = port->n_read;
            if (n) {
                packet += n;
                size -= n;
            }

            count = tty_insert_flip_string(&port->port, (void *)packet,
                    size);
            if (count)
                do_push = true;
            if (count != size) {
                /* stop pushing; TTY layer can't handle more */
                port->n_read += count;
                pr_vdebug("ttyGS%d: rx block %d/%d\n",
                      port->port_num, count, req->actual);
                break;
            }
            port->n_read = 0;
        }

        list_move(&req->list, &port->read_pool);
        port->read_started--;
    }

    /* Push from tty to ldisc; this is handled by a workqueue,
     * so we won't get callbacks and can hold port_lock
     */
    if (do_push)
        tty_flip_buffer_push(&port->port);


    /* We want our data queue to become empty ASAP, keeping data
     * in the tty and ldisc (not here).  If we couldn't push any
     * this time around, RX may be starved, so wait until next jiffy.
     *
     * We may leave non-empty queue only when there is a tty, and
     * either it is throttled or there is no more room in flip buffer.
     */
    if (!list_empty(queue) && !tty_throttled(tty))
        schedule_delayed_work(&port->push, 1);

    /* If we're still connected, refill the USB RX queue. */
    if (!disconnect && port->port_usb)
        gs_start_rx(port);

    spin_unlock_irq(&port->port_lock);
}

static int gs_start_tx(struct gs_port *port)
{
	struct list_head	*pool = &port->write_pool;
	struct usb_ep		*in;
	int			status = 0;
	bool			do_tty_wake = false;

	if (!port->port_usb)
		return status;

	in = port->port_usb->in;

	while (!port->write_busy && !list_empty(pool)) {
		struct usb_request	*req;
		int			len;

		if (port->write_started >= QUEUE_SIZE)
			break;

		req = list_entry(pool->next, struct usb_request, list);
		len = gs_send_packet(port, req->buf, in->maxpacket);
		if (len == 0) {
			wake_up_interruptible(&port->drain_wait);
			break;
		}
		do_tty_wake = true;

		req->length = len;
		list_del(&req->list);
		req->zero = kfifo_is_empty(&port->port_write_buf);

		pr_vdebug("ttyGS%d: tx len=%d, 0x%02x 0x%02x 0x%02x ...\n",
			  port->port_num, len, *((u8 *)req->buf),
			  *((u8 *)req->buf+1), *((u8 *)req->buf+2));

		/* Drop lock while we call out of driver; completions
		 * could be issued while we do so.  Disconnection may
		 * happen too; maybe immediately before we queue this!
		 *
		 * NOTE that we may keep sending data for a while after
		 * the TTY closed (dev->ioport->port_tty is NULL).
		 */
		port->write_busy = true;
		spin_unlock(&port->port_lock);
		status = usb_ep_queue(in, req, GFP_ATOMIC);
		spin_lock(&port->port_lock);
		port->write_busy = false;

		if (status) {
			pr_debug("%s: %s %s err %d\n",
					__func__, "queue", in->name, status);
			list_add(&req->list, pool);
			break;
		}

		port->write_started++;

		/* abort immediately after disconnect */
		if (!port->port_usb)
			break;
	}

	if (do_tty_wake && port->port.tty)
		tty_wakeup(port->port.tty);
	return status;
}

static unsigned gs_start_rx(struct gs_port *port)
{
    struct list_head	*pool = &port->read_pool;
    struct usb_ep		*out = port->port_usb->out;

    while (!list_empty(pool)) {
        struct usb_request	*req;
        int			status;
        struct tty_struct	*tty;

        /* no more rx if closed */
        tty = port->port.tty;
        if (!tty)
            break;

        if (port->read_started >= QUEUE_SIZE)
            break;

        req = list_entry(pool->next, struct usb_request, list);
        list_del(&req->list);
        req->length = out->maxpacket;

        /* drop lock while we call out; the controller driver
         * may need to call us back (e.g. for disconnect)
         */
        spin_unlock(&port->port_lock);
        status = usb_ep_queue(out, req, GFP_ATOMIC);
        spin_lock(&port->port_lock);

        if (status) {
            pr_debug("%s: %s %s err %d\n",
                    __func__, "queue", out->name, status);
            list_add(&req->list, pool);
            break;
        }
        port->read_started++;

        /* abort immediately after disconnect */
        if (!port->port_usb)
            break;
    }
    return port->read_started;
}

static void gs_read_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct gs_port	*port = ep->driver_data;

    /* Queue all received data until the tty layer is ready for it. */
    spin_lock(&port->port_lock);
    list_add_tail(&req->list, &port->read_queue);
    schedule_delayed_work(&port->push, 0);
    spin_unlock(&port->port_lock);
}

static void gs_write_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct gs_port	*port = ep->driver_data;

    spin_lock(&port->port_lock);
    list_add(&req->list, &port->write_pool);
    port->write_started--;

    switch (req->status) {
    default:
        /* presumably a transient fault */
        pr_warn("%s: unexpected %s status %d\n",
            __func__, ep->name, req->status);
        fallthrough;
    case 0:
        /* normal completion */
        gs_start_tx(port);
        break;

    case -ESHUTDOWN:
        /* disconnect */
        pr_vdebug("%s: %s shutdown\n", __func__, ep->name);
        break;
    }

    spin_unlock(&port->port_lock);
}

static int gs_start_io(struct gs_port *port)
{
    struct list_head	*head = &port->read_pool;
    struct usb_ep		*ep = port->port_usb->out;
    int			status;
    unsigned		started;

    /* Allocate RX and TX I/O buffers.  We can't easily do this much
     * earlier (with GFP_KERNEL) because the requests are coupled to
     * endpoints, as are the packet sizes we'll be using.  Different
     * configurations may use different endpoints with a given port;
     * and high speed vs full speed changes packet sizes too.
     */
    status = gs_alloc_requests(ep, head, gs_read_complete,
        &port->read_allocated);
    if (status)
        return status;

    status = gs_alloc_requests(port->port_usb->in, &port->write_pool,
            gs_write_complete, &port->write_allocated);
    if (status) {
        gs_free_requests(ep, head, &port->read_allocated);
        return status;
    }

    /* queue read requests */
    port->n_read = 0;
    started = gs_start_rx(port);

    if (started) {
        gs_start_tx(port);
        /* Unblock any pending writes into our circular buffer, in case
         * we didn't in gs_start_tx() */
        //tty_wakeup(port->port.tty);
    } else {
        gs_free_requests(ep, head, &port->read_allocated);
        gs_free_requests(port->port_usb->in, &port->write_pool,
            &port->write_allocated);
        status = -EIO;
    }

    return status;
}

static int gs_port_alloc(unsigned port_num, struct usb_cdc_line_coding *coding)
{
    struct gs_port	*port;
    int		ret = 0;

    mutex_lock(&ports[port_num].lock);
    if (ports[port_num].port) {
        ret = -EBUSY;
        goto out;
    }

    port = kzalloc(sizeof(struct gs_port), GFP_KERNEL);
    if (port == NULL) {
        ret = -ENOMEM;
        goto out;
    }

    tty_port_init(&port->port);
    spin_lock_init(&port->port_lock);
    init_waitqueue_head(&port->drain_wait);
    init_waitqueue_head(&port->close_wait);

    INIT_DELAYED_WORK(&port->push, gs_rx_push);

    INIT_LIST_HEAD(&port->read_pool);
    INIT_LIST_HEAD(&port->read_queue);
    INIT_LIST_HEAD(&port->write_pool);

    port->port_num = port_num;
    port->port_line_coding = *coding;

    ports[port_num].port = port;
out:
    mutex_unlock(&ports[port_num].lock);
    return ret;
}

static int gs_console_init(struct gs_port *port)
{
    return -ENOSYS;
}

static void gserial_free_port(struct gs_port *port)
{

}

/*-------------------------------------------------------------------------*/

/* I/O glue between TTY (upper) and USB function (lower) driver layers */

/*
 * gs_alloc_req
 *
 * Allocate a usb_request and its buffer.  Returns a pointer to the
 * usb_request or NULL if there is an error.
 */
struct usb_request *
gs_alloc_req(struct usb_ep *ep, unsigned len, gfp_t kmalloc_flags)
{
    struct usb_request *req;

    req = usb_ep_alloc_request(ep, kmalloc_flags);

    if (req != NULL) {
        req->length = len;
        req->buf = kmalloc(len, kmalloc_flags);
        if (req->buf == NULL) {
            usb_ep_free_request(ep, req);
            return NULL;
        }
    }

    return req;
}
EXPORT_SYMBOL_GPL(gs_alloc_req);

void gs_free_req(struct usb_ep *ep, struct usb_request *req)
{
}

void gserial_free_line(unsigned char port_num)
{
}

int gserial_alloc_line_no_console(unsigned char *line_num)
{
    struct usb_cdc_line_coding	coding;
    struct gs_port			*port;
    struct device			*tty_dev;
    int				ret;
    int				port_num;

    coding.dwDTERate = cpu_to_le32(9600);
    coding.bCharFormat = 8;
    coding.bParityType = USB_CDC_NO_PARITY;
    coding.bDataBits = USB_CDC_1_STOP_BITS;

    for (port_num = 0; port_num < MAX_U_SERIAL_PORTS; port_num++) {
        ret = gs_port_alloc(port_num, &coding);
        if (ret == -EBUSY)
            continue;
        if (ret)
            return ret;
        break;
    }
    if (ret)
        return ret;

    /* ... and sysfs class devices, so mdev/udev make /dev/ttyGS* */

    port = ports[port_num].port;
    tty_dev = tty_port_register_device(&port->port,  port_num, &ports[port_num]);
    if (IS_ERR(tty_dev)) {
        pr_err("%s: failed to register tty for port %d, err %ld\n",
                __func__, port_num, PTR_ERR(tty_dev));

        ret = PTR_ERR(tty_dev);
        mutex_lock(&ports[port_num].lock);
        ports[port_num].port = NULL;
        mutex_unlock(&ports[port_num].lock);
        gserial_free_port(port);
        goto err;
    }
    *line_num = port_num;
err:
    return ret;
}
EXPORT_SYMBOL_GPL(gserial_alloc_line_no_console);

int gserial_alloc_line(unsigned char *line_num)
{
    int ret = gserial_alloc_line_no_console(line_num);

    if (!ret && !*line_num)
        gs_console_init(ports[*line_num].port);

    return ret;
}

int gserial_connect(struct gserial *gser, u8 port_num)
{
    struct gs_port	*port;
    unsigned long	flags;
    int		status;

    if (port_num >= MAX_U_SERIAL_PORTS)
        return -ENXIO;

    port = ports[port_num].port;
    if (!port) {
        pr_err("serial line %d not allocated.\n", port_num);
        return -EINVAL;
    }

    if (port->port_usb) {
        pr_err("serial line %d is in use.\n", port_num);
        return -EBUSY;
    }

    /* activate the endpoints */
    status = usb_ep_enable(gser->in);
    if (status < 0)
        return status;
    gser->in->driver_data = port;

    status = usb_ep_enable(gser->out);
    if (status < 0)
        goto fail_out;
    gser->out->driver_data = port;

    gser->ioport = port;
    port->port_usb = gser;

    return status;

fail_out:
    usb_ep_disable(gser->in);
    return status;
}

void gserial_disconnect(struct gserial *gser)
{
}

void gserial_suspend(struct gserial *gser)
{

}

void gserial_resume(struct gserial *gser)
{
    
}


/*------------------------------------*/
static struct gs_port* _get_gs(struct dfs_fd *fle)
{
    struct tty_port *tp = (struct tty_port*)fle->data;
    struct portmaster *pm = (struct portmaster *)tp->parent.user_data;
    struct gs_port *gs = pm->port;

    return gs;
}

static int _raw_read(struct gs_port *port, int nb, uint8_t *buf, size_t size)
{
    int ret = 0;
    int c;
    struct tty_struct *s = port->port.tty;

    while (1)
    {
        c = rt_ringbuffer_get(s->rxfifo, buf, size);
        if (c == 0)
        {
            if (nb && (ret == 0))
            {
                ret = -EAGAIN;
                break;
            }
            if (ret)
                break;

            rt_wqueue_wait(&s->rxwq, 0, -1);
        }
        else
        {
            size -= c;
            buf += c;
            ret += c;
        }
    }

    return ret;
}

static int _raw_write(struct gs_port *port, int nb, const uint8_t *buf, size_t size)
{
    int ret = -EAGAIN;
    int c;
    const uint8_t *dbuf;
    struct tty_struct *s = port->port.tty;

    dbuf = buf;
    while (size)
    {
        while (size > 0)
        {
            c = kfifo_in(&port->port_write_buf, (void *)dbuf, size);
            if (!c)
                break;

            if (gs_start_tx(port) != 0)
                return -EIO;
            dbuf += c;
            size -= c;
        }

        if (nb)
            break;

        /* 非阻塞模式等待数据发完后才返回 */
        rt_wqueue_wait(&s->txwq, 0, -1);
    }

    return (dbuf == buf) ? ret : (dbuf - buf);
}

static int _tty_open(struct dfs_fd *fle)
{
    struct gs_port *port;
    int		status = 0;
    struct mutex *lock;

    port = _get_gs(fle);
    lock = &ports[port->port_num].lock;

    mutex_lock(lock);

    if (port->port.parent.ref_count ++ == 1)
    {
        goto out;
    }

    if (!kfifo_initialized(&port->port_write_buf)) {
		status = kfifo_alloc(&port->port_write_buf,
				     WRITE_BUF_SIZE, GFP_KERNEL);
		if (status) {
			goto out;
		}
    }

    port->port.tty = &port->port._tty;
    if (port->port_usb) {
		/* if port is suspended, wait resume to start I/0 stream */
		if (!port->suspended) {
			struct gserial	*gser = port->port_usb;

			gs_start_io(port);

			if (gser->connect)
				gser->connect(gser);
		} else {
			port->start_delayed = true;
		}
    }

out:
    mutex_unlock(lock);

    return status;
}

static int _tty_ioctl(struct dfs_fd *fd, int cmd, void *args)
{
    return 0;
}

static int _tty_close(struct dfs_fd *file)
{
    struct gs_port *port;
    struct gserial	*gser;

    port = _get_gs(file);

    spin_lock_irq(&port->port_lock);
    if (port->port.parent.ref_count > 0)
    {
        -- port->port.parent.ref_count;
        if (port->port.parent.ref_count != 0)
            goto exit;
    }

	gser = port->port_usb;
	if (gser && !port->suspended && gser->disconnect)
		gser->disconnect(gser);

    kfifo_reset(&port->port_write_buf);

    port->start_delayed = false;
    port->port.tty = NULL;

exit:
    spin_unlock_irq(&port->port_lock);

    return 0;
}

static int _tty_read(struct dfs_fd *file, void *buf, size_t size)
{
    struct gs_port *port;
    int ret;
    int nb;

    nb = file->flags & O_NONBLOCK;

    port = _get_gs(file);

    ret = _raw_read(port, nb, (uint8_t *)buf, size);

    return ret;
}

static int _tty_write(struct dfs_fd *file, const void *buf, size_t count)
{
    struct gs_port *port;
    int ret;
    int nb;

    nb = file->flags & O_NONBLOCK;

    port = _get_gs(file);

    ret = _raw_write(port, nb, buf, count);

    return ret;
}

static int _tty_poll(struct dfs_fd *fd, struct rt_pollreq *req)
{
    return 0;
}

static const struct dfs_file_ops _gsops = {
    .open = _tty_open,
    .ioctl = _tty_ioctl,
    .close = _tty_close,
    .read = _tty_read,
    .write = _tty_write,
    .poll = _tty_poll,
};

static struct device* tty_port_register_device(struct tty_port *port, int index, void *priv)
{
    struct device* dev = ERR_PTR(-1);
    char name[8];
    int ret;

    sprintf(name, "ttyGS%d", index);
    ret = rt_udevice_register(&port->parent, name, 0);
    printf("ret = %d\n",ret);
    if (ret == 0)
    {
        port->parent.fops = &_gsops;
        port->parent.user_data = priv;
        dev = &port->dev;	
    }

    return dev;
}

static int userial_init(void)
{
    unsigned i;

    for (i = 0; i < MAX_U_SERIAL_PORTS; i++)
        mutex_init(&ports[i].lock);

    return 0;
}
module_init(userial_init);
