#!/bin/sh

for file in ./root/bin/*.elf
do
    if test -f $file
    then
        arm-linux-musleabi-strip --strip-all $file
    fi
done