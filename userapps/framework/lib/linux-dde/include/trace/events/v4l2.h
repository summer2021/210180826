/* SPDX-License-Identifier: GPL-2.0 */
#undef TRACE_SYSTEM
#define TRACE_SYSTEM v4l2

#if !defined(_TRACE_V4L2_H) || defined(TRACE_HEADER_MULTI_READ)
#define _TRACE_V4L2_H

#include <linux/tracepoint.h>
#include <media/videobuf2-v4l2.h>

DEFINE_EVENT(v4l2_event_class, v4l2_dqbuf,
	TP_PROTO(int minor, struct v4l2_buffer *buf),
	TP_ARGS(minor, buf)
);

DEFINE_EVENT(v4l2_event_class, v4l2_qbuf,
	TP_PROTO(int minor, struct v4l2_buffer *buf),
	TP_ARGS(minor, buf)
);

#endif
