/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-24     lizhirui     the first version
 */

#ifndef __DRV_SYSCALL_H__
#define __DRV_SYSCALL_H__

#include <rtthread.h>

rt_size_t syscall(rt_size_t nr,...);

typedef void (*rt_isr_handler_t)(int vector, void *param);

typedef struct rt_device_reply
{
    void *client_lwp;
    void *reply;
    void *cmd;
} *rt_device_reply_t;

struct lwp_device_data_info
{
    void *lwp;
    void *src;
    void *dst;
    size_t size;
};

struct lwp_device_mapinfo
{
    void *vaddr;
    void *paddr;
    size_t size;
    int cached;
};

struct lwp_device_interrupt_info
{
    int vector;
    void *handler;
    void *param;
};

struct lwp_device_cache_info
{
    int ops;
    void *addr;
    size_t size;
};

#define RT_DEVICE_CTRL_NOTIFY_SET       0x05            /**< set notify func */
#define RT_DEVICE_CTRL_CONSOLE_OFLAG    0x06            /**< get console open flag */

#ifdef RT_USING_LWP
#define RT_DEVICE_CTRL_IOREMAP          0x13            /**< user device ioremap */
#define RT_DEVICE_CTRL_IOUNREMAP        0x14            /**< user device unioremap */
#define RT_DEVICE_CTRL_MMAP             0x15            /**< user device alloc pages and map */
#define RT_DEVICE_CTRL_MUNMAP           0x16            /**< user device unmap and free pages*/
#define RT_DEVICE_CTRL_DATA_GET         0x17            /**< user device get data from lwp */
#define RT_DEVICE_CTRL_DATA_PUT         0x18            /**< user device put data to lwp*/
#define RT_DEVICE_CTRL_CACHE_MAINTAIN   0x19            /**< user device cache maintain */
#define RT_DEVICE_CTRL_SET_INT_HANDLER  0x1a            /**< user device interrupt install */
#define RT_DEVICE_CTRL_INT_UMASK        0x1b            /**< user device umask interrupt */
#define RT_DEVICE_CTRL_INT_MASK         0x1c            /**< user device mask interrupt */
#define RT_DEVICE_CTRL_QUIT             0x1d            /**< user device quit */
#endif

void rt_udevice_interrupt_install(rt_device_t udev, int vector, rt_isr_handler_t handler, void *param);
void rt_udevice_interrupt_mask(rt_device_t udev, int vector);
void rt_udevice_interrupt_umask(rt_device_t udev, int vector);
void* rt_ioremap(rt_device_t udev, void *paddr, size_t size, int cached);
rt_err_t rt_iounremap(rt_device_t udev, void *vaddr, size_t size);
void* rt_mmap(rt_device_t udev, void **paddr, size_t size, int cached);
rt_err_t rt_munmap(rt_device_t udev, void *vaddr, size_t size);
size_t rt_data_get(rt_device_t udev, void *lwp, void *dst, const void *src, size_t size);
size_t rt_data_put(rt_device_t udev, void *lwp, void *dst, const void *src, size_t size);
void rt_dcache_ops(rt_device_t udev, int ops, void *addr, int size);
void *rt_device_client_get(void);
void rt_device_reply_get(rt_device_reply_t r);
void rt_device_reply_ack(rt_device_reply_t r, size_t ret);
rt_err_t rt_udevice_register(rt_device_t dev, const char *name, rt_uint16_t flags);
rt_err_t rt_udevice_init(rt_device_t dev);
rt_err_t rt_udevice_control(rt_device_t dev, int cmd, void *arg);
rt_device_t rt_udevice_find(const char *name);
rt_err_t rt_udevice_open(rt_device_t dev, rt_uint16_t oflag);
rt_err_t rt_udevice_close(rt_device_t dev);
rt_size_t rt_udevice_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size);
rt_size_t rt_udevice_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size);
rt_err_t rt_udevice_unregister(rt_device_t dev);
void rt_udevice_set_current(rt_device_t dev);
rt_device_t rt_udevice_get_current();
void dev_debug();

#endif