/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __ASM_GENERIC_UNALIGNED_H
#define __ASM_GENERIC_UNALIGNED_H

#include <asm/byteorder.h>

static inline void put_unaligned_le16(u16 val, void *p)
{
    u16 tmp = cpu_to_le16(val);

    memcpy(p, &tmp, 2);
}

static inline void put_unaligned_le32(u32 val, void *p)
{
    u32 tmp = cpu_to_le32(val);

    memcpy(p, &tmp, 4);
}

#endif
