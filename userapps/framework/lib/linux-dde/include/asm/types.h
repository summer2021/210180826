/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __ASM_GENERIC_TYPES_H
#define __ASM_GENERIC_TYPES_H

#include <stdint.h>
#include <stdbool.h>
#include <sys/types.h>

typedef uint8_t u8;
typedef int8_t s8;
typedef uint16_t u16;
typedef int16_t s16;
typedef unsigned int u32;
typedef int32_t s32;
typedef int64_t s64;
typedef uint64_t u64;

typedef int32_t __s32;
typedef uint32_t __u32;
typedef uint8_t __u8;
typedef int8_t __s8;
typedef uint16_t __u16;
typedef int16_t __s16;
typedef uint64_t __u64;
typedef int64_t __s64;

#endif
