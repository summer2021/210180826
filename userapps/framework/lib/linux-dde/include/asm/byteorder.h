/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _ASM_BYTEORDER_H
#define _ASM_BYTEORDER_H

#include <asm/types.h>
#include <linux/swab.h>

static __always_inline bool __cpu_is_le(void)
{
    u16 val = 0x0102;
    char *p = (char*)&val;

    return (p[0] == 0x02);
}

static __always_inline u16 __Bswap16(u16 v)
{
    u16 ret;

    ret = (v << 8) | (v >> 8);

    return ret;
}

#define __Bswap32(v) ((((v) & 0xff) << 24) | ((v) >> 24) | (((v) & 0xFF0000) >> 8) | (((v) & 0xFF00) << 8))

/*
static __always_inline u32 __Bswap32(u32 v)
{
    u32 ret;
    
    ret = (v << 24) | (v >> 24) | ((v & 0xFF0000) >> 8) | ((v & 0xFF00) << 8);

    return ret;
}*/

static __always_inline u64 __Bswap64(u64 v)
{
    u64 ret;
    
    ret = ((__Bswap32(v)) + (0ULL << 32)) | (__Bswap32(v >> 32));

    return ret;
}

static __always_inline __u16 __le16_to_cpu(__u16 le)
{
    if (__cpu_is_le())
        return le;

    return __Bswap16(le);
}

static __always_inline __u16 __be16_to_cpu(__u16 be)
{
     if (!__cpu_is_le())
        return be;

    return __Bswap16(be);   
}

static __always_inline __u32 __le32_to_cpu(__u32 le)
{
    if (__cpu_is_le())
        return le;

    return __Bswap32(le);
}

static __always_inline __le64 __cpu_to_le64p(const __u64 *p)
{
	return (__force __le64)*p;
}
static __always_inline __u64 __le64_to_cpup(const __le64 *p)
{
	return (__force __u64)*p;
}
static __always_inline __le32 __cpu_to_le32p(const __u32 *p)
{
	return (__force __le32)*p;
}
static __always_inline __u32 __le32_to_cpup(const __le32 *p)
{
	return (__force __u32)*p;
}
static __always_inline __le16 __cpu_to_le16p(const __u16 *p)
{
	return (__force __le16)*p;
}
static __always_inline __u16 __le16_to_cpup(const __le16 *p)
{
	return (__force __u16)*p;
}
static __always_inline __be64 __cpu_to_be64p(const __u64 *p)
{
	return (__force __be64)__Bswap64((__u64)(unsigned long)p);
}
static __always_inline __u64 __be64_to_cpup(const __be64 *p)
{
	return __swab64p((__u64 *)p);
}

#define __cpu_to_be32p(p) (__force __be32)__Bswap32((__u32)(unsigned long)(p)) 
#define __be32_to_cpup(p) (__swab32p((__u32 *)(p)))

/*
static __always_inline __be32 __cpu_to_be32p(const __u32 *p)
{
	return (__force __be32)__Bswap32((__u32)(unsigned long)p);
}
static __always_inline __u32 __be32_to_cpup(const __be32 *p)
{
	return __swab32p((__u32 *)p);
}*/

static __always_inline __be16 __cpu_to_be16p(const __u16 *p)
{
	return (__force __be16)__Bswap16((__u16)(unsigned long)p);
}
static __always_inline __u16 __be16_to_cpup(const __be16 *p)
{
	return __Bswap16((__u16)(unsigned long)(__u16 *)p);
}
#define __cpu_to_le64s(x) do { (void)(x); } while (0)
#define __le64_to_cpus(x) do { (void)(x); } while (0)
#define __cpu_to_le32s(x) do { (void)(x); } while (0)
#define __le32_to_cpus(x) do { (void)(x); } while (0)
#define __cpu_to_le16s(x) do { (void)(x); } while (0)
#define __le16_to_cpus(x) do { (void)(x); } while (0)
#define __cpu_to_be64s(x) __Bswap64((x))
#define __be64_to_cpus(x) __Bswap64((x))
#define __cpu_to_be32s(x) __Bswap32((x))
#define __be32_to_cpus(x) __Bswap32((x))
#define __cpu_to_be16s(x) __Bswap16((x))
#define __be16_to_cpus(x) __Bswap16((x))

/*#define le16_to_cpu __le16_to_cpu
#define cpu_to_le16(x) (x) //TODO
#define __cpu_to_le16 __le16_to_cpu
#define __cpu_to_be16 __be16_to_cpu
#define cpu_to_le32 __le32_to_cpu
#define le32_to_cpu __le32_to_cpu*/

#define __cpu_to_le16
#define __cpu_to_le32
#define __cpu_to_le64
#define __le16_to_cpu
#define __le32_to_cpu
#define __le64_to_cpu
#define __cpu_to_be16 __Bswap16
#define __cpu_to_be32 __Bswap32
#define __cpu_to_be64 __Bswap64
#define __be16_to_cpu __Bswap16
#define __be32_to_cpu __Bswap32
#define __be64_to_cpu __Bswap64

#define cpu_to_le64 __cpu_to_le64
#define le64_to_cpu __le64_to_cpu
#define cpu_to_le32 __cpu_to_le32
#define le32_to_cpu __le32_to_cpu
#define cpu_to_le16 __cpu_to_le16
#define le16_to_cpu __le16_to_cpu
#define cpu_to_be64 __cpu_to_be64
#define be64_to_cpu __be64_to_cpu
#define cpu_to_be32 __cpu_to_be32
#define be32_to_cpu __be32_to_cpu
#define cpu_to_be16 __cpu_to_be16
#define be16_to_cpu __be16_to_cpu
#define cpu_to_le64p __cpu_to_le64p
#define le64_to_cpup __le64_to_cpup
#define cpu_to_le32p __cpu_to_le32p
#define le32_to_cpup __le32_to_cpup
#define cpu_to_le16p __cpu_to_le16p
#define le16_to_cpup __le16_to_cpup
#define cpu_to_be64p __cpu_to_be64p
#define be64_to_cpup __be64_to_cpup
#define cpu_to_be32p __cpu_to_be32p
#define be32_to_cpup __be32_to_cpup
#define cpu_to_be16p __cpu_to_be16p
#define be16_to_cpup __be16_to_cpup
#define cpu_to_le64s __cpu_to_le64s
#define le64_to_cpus __le64_to_cpus
#define cpu_to_le32s __cpu_to_le32s
#define le32_to_cpus __le32_to_cpus
#define cpu_to_le16s __cpu_to_le16s
#define le16_to_cpus __le16_to_cpus
#define cpu_to_be64s __cpu_to_be64s
#define be64_to_cpus __be64_to_cpus
#define cpu_to_be32s __cpu_to_be32s
#define be32_to_cpus __be32_to_cpus
#define cpu_to_be16s __cpu_to_be16s
#define be16_to_cpus __be16_to_cpus

static inline void le16_add_cpu(__le16 *var, u16 val)
{
	*var = cpu_to_le16(le16_to_cpu(*var) + val);
}

#endif
