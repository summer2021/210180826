/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __ASM_GENERIC_PAGE_H
#define __ASM_GENERIC_PAGE_H
/*
 * Generic page.h implementation, for NOMMU architectures.
 * This provides the dummy definitions for the memory management.
 */

#define PAGE_SHIFT	12
#define PAGE_SIZE	(1 << PAGE_SHIFT)
#define PAGE_MASK	(~((1 << PAGE_SHIFT) - 1))

#define	__pfn_to_phys(pfn) pfn //TODO

static inline struct page * virt_to_page(const void *addr)
{
    printk("TODO: %s\n", __func__);
    return 0;//TODO
}

static inline dma_addr_t page_to_phys(struct page *p)
{
    printk("TODO: %s\n", __func__);
    return 0;
}

static inline unsigned long page_to_pfn(const struct page *p)
{
    printk("TODO: %s\n", __func__);
    return 0;
}

static inline struct page * pfn_to_page(unsigned long p)
{
    printk("TODO: %s\n", __func__);
    return 0;
}
#endif
