/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright(c) 2004 - 2006 Intel Corporation. All rights reserved.
 */
#ifndef LINUX_DMAENGINE_H
#define LINUX_DMAENGINE_H

#include <linux/device.h>
#include <linux/scatterlist.h>
#include <linux/bitmap.h>
#include <linux/types.h>

#include <rtdevice.h>

struct dma_device;
struct dma_slave_config;
typedef s32 dma_cookie_t;

/**
 * enum dma_status - DMA transaction status
 * @DMA_COMPLETE: transaction completed
 * @DMA_IN_PROGRESS: transaction not yet processed
 * @DMA_PAUSED: transaction is paused
 * @DMA_ERROR: transaction failed
 */
enum dma_status {
	DMA_COMPLETE,
	DMA_IN_PROGRESS,
	DMA_PAUSED,
	DMA_ERROR,
	DMA_OUT_OF_ORDER,
};

/**
 * enum dma_transfer_direction - dma transfer mode and direction indicator
 * @DMA_MEM_TO_MEM: Async/Memcpy mode
 * @DMA_MEM_TO_DEV: Slave mode & From Memory to Device
 * @DMA_DEV_TO_MEM: Slave mode & From Device to Memory
 * @DMA_DEV_TO_DEV: Slave mode & From Device to Device
 */
enum dma_transfer_direction {
	DMA_MEM_TO_MEM,
	DMA_MEM_TO_DEV,
	DMA_DEV_TO_MEM,
	DMA_DEV_TO_DEV,
	DMA_TRANS_NONE,
};

struct dma_chan {
	const char *name;

    struct dma_device *device;
	struct list_head device_node;
	int client_count;
	void *priv;
};

/**
 * struct dma_tx_state - filled in to report the status of
 * a transfer.
 * @last: last completed DMA cookie
 * @used: last issued DMA cookie (i.e. the one in progress)
 * @residue: the remaining number of bytes left to transmit
 *	on the selected transfer for states DMA_IN_PROGRESS and
 *	DMA_PAUSED if this is implemented in the driver, else 0
 * @in_flight_bytes: amount of data in bytes cached by the DMA.
 */
struct dma_tx_state {
	dma_cookie_t last;
	dma_cookie_t used;
	u32 residue;
	u32 in_flight_bytes;
};

/**
 * enum dma_slave_buswidth - defines bus width of the DMA slave
 * device, source or target buses
 */
enum dma_slave_buswidth {
	DMA_SLAVE_BUSWIDTH_UNDEFINED = 0,
	DMA_SLAVE_BUSWIDTH_1_BYTE = 1,
	DMA_SLAVE_BUSWIDTH_2_BYTES = 2,
	DMA_SLAVE_BUSWIDTH_3_BYTES = 3,
	DMA_SLAVE_BUSWIDTH_4_BYTES = 4,
	DMA_SLAVE_BUSWIDTH_8_BYTES = 8,
	DMA_SLAVE_BUSWIDTH_16_BYTES = 16,
	DMA_SLAVE_BUSWIDTH_32_BYTES = 32,
	DMA_SLAVE_BUSWIDTH_64_BYTES = 64,
};

enum dmaengine_tx_result {
	DMA_TRANS_NOERROR = 0,		/* SUCCESS */
	DMA_TRANS_READ_FAILED,		/* Source DMA read failed */
	DMA_TRANS_WRITE_FAILED,		/* Destination DMA write failed */
	DMA_TRANS_ABORTED,		/* Op never submitted / aborted */
};

struct dmaengine_result {
	enum dmaengine_tx_result result;
	u32 residue;
};

/**
 * enum dma_ctrl_flags - DMA flags to augment operation preparation,
 *  control completion, and communicate status.
 * @DMA_PREP_INTERRUPT - trigger an interrupt (callback) upon completion of
 *  this transaction
 * @DMA_CTRL_ACK - if clear, the descriptor cannot be reused until the client
 *  acknowledges receipt, i.e. has a chance to establish any dependency
 *  chains
 * @DMA_PREP_PQ_DISABLE_P - prevent generation of P while generating Q
 * @DMA_PREP_PQ_DISABLE_Q - prevent generation of Q while generating P
 * @DMA_PREP_CONTINUE - indicate to a driver that it is reusing buffers as
 *  sources that were the result of a previous operation, in the case of a PQ
 *  operation it continues the calculation with new sources
 * @DMA_PREP_FENCE - tell the driver that subsequent operations depend
 *  on the result of this operation
 * @DMA_CTRL_REUSE: client can reuse the descriptor and submit again till
 *  cleared or freed
 * @DMA_PREP_CMD: tell the driver that the data passed to DMA API is command
 *  data and the descriptor should be in different format from normal
 *  data descriptors.
 * @DMA_PREP_REPEAT: tell the driver that the transaction shall be automatically
 *  repeated when it ends until a transaction is issued on the same channel
 *  with the DMA_PREP_LOAD_EOT flag set. This flag is only applicable to
 *  interleaved transactions and is ignored for all other transaction types.
 * @DMA_PREP_LOAD_EOT: tell the driver that the transaction shall replace any
 *  active repeated (as indicated by DMA_PREP_REPEAT) transaction when the
 *  repeated transaction ends. Not setting this flag when the previously queued
 *  transaction is marked with DMA_PREP_REPEAT will cause the new transaction
 *  to never be processed and stay in the issued queue forever. The flag is
 *  ignored if the previous transaction is not a repeated transaction.
 */
enum dma_ctrl_flags {
	DMA_PREP_INTERRUPT = (1 << 0),
	DMA_CTRL_ACK = (1 << 1),
	DMA_PREP_PQ_DISABLE_P = (1 << 2),
	DMA_PREP_PQ_DISABLE_Q = (1 << 3),
	DMA_PREP_CONTINUE = (1 << 4),
	DMA_PREP_FENCE = (1 << 5),
	DMA_CTRL_REUSE = (1 << 6),
	DMA_PREP_CMD = (1 << 7),
	DMA_PREP_REPEAT = (1 << 8),
	DMA_PREP_LOAD_EOT = (1 << 9),
};

struct dma_device {
    struct rt_device rtd;

	struct list_head channels;

	int (*device_config)(struct dma_chan *chan,
			     struct dma_slave_config *config);
	struct dma_async_tx_descriptor *(*device_prep_slave_sg)(
		struct dma_chan *chan, struct scatterlist *sgl,
		unsigned int sg_len, enum dma_transfer_direction direction,
		unsigned long flags, void *context);
	void (*device_issue_pending)(struct dma_chan *chan);
	int (*device_terminate_all)(struct dma_chan *chan);
	void (*device_synchronize)(struct dma_chan *chan);
	enum dma_status (*device_tx_status)(struct dma_chan *chan,
					    dma_cookie_t cookie,
					    struct dma_tx_state *txstate);
};

/**
 * enum dma_desc_metadata_mode - per descriptor metadata mode types supported
 * @DESC_METADATA_CLIENT - the metadata buffer is allocated/provided by the
 *  client driver and it is attached (via the dmaengine_desc_attach_metadata()
 *  helper) to the descriptor.
 *
 * Client drivers interested to use this mode can follow:
 * - DMA_MEM_TO_DEV / DEV_MEM_TO_MEM:
 *   1. prepare the descriptor (dmaengine_prep_*)
 *	construct the metadata in the client's buffer
 *   2. use dmaengine_desc_attach_metadata() to attach the buffer to the
 *	descriptor
 *   3. submit the transfer
 * - DMA_DEV_TO_MEM:
 *   1. prepare the descriptor (dmaengine_prep_*)
 *   2. use dmaengine_desc_attach_metadata() to attach the buffer to the
 *	descriptor
 *   3. submit the transfer
 *   4. when the transfer is completed, the metadata should be available in the
 *	attached buffer
 *
 * @DESC_METADATA_ENGINE - the metadata buffer is allocated/managed by the DMA
 *  driver. The client driver can ask for the pointer, maximum size and the
 *  currently used size of the metadata and can directly update or read it.
 *  dmaengine_desc_get_metadata_ptr() and dmaengine_desc_set_metadata_len() is
 *  provided as helper functions.
 *
 *  Note: the metadata area for the descriptor is no longer valid after the
 *  transfer has been completed (valid up to the point when the completion
 *  callback returns if used).
 *
 * Client drivers interested to use this mode can follow:
 * - DMA_MEM_TO_DEV / DEV_MEM_TO_MEM:
 *   1. prepare the descriptor (dmaengine_prep_*)
 *   2. use dmaengine_desc_get_metadata_ptr() to get the pointer to the engine's
 *	metadata area
 *   3. update the metadata at the pointer
 *   4. use dmaengine_desc_set_metadata_len()  to tell the DMA engine the amount
 *	of data the client has placed into the metadata buffer
 *   5. submit the transfer
 * - DMA_DEV_TO_MEM:
 *   1. prepare the descriptor (dmaengine_prep_*)
 *   2. submit the transfer
 *   3. on transfer completion, use dmaengine_desc_get_metadata_ptr() to get the
 *	pointer to the engine's metadata area
 *   4. Read out the metadata from the pointer
 *
 * Note: the two mode is not compatible and clients must use one mode for a
 * descriptor.
 */
enum dma_desc_metadata_mode {
	DESC_METADATA_NONE = 0,
	DESC_METADATA_CLIENT = BIT(0),
	DESC_METADATA_ENGINE = BIT(1),
};

typedef void (*dma_async_tx_callback)(void *dma_async_param);
typedef void (*dma_async_tx_callback_result)(void *dma_async_param,
				const struct dmaengine_result *result);

/**
 * struct dma_async_tx_descriptor - async transaction descriptor
 * ---dma generic offload fields---
 * @cookie: tracking cookie for this transaction, set to -EBUSY if
 *	this tx is sitting on a dependency list
 * @flags: flags to augment operation preparation, control completion, and
 *	communicate status
 * @phys: physical address of the descriptor
 * @chan: target channel for this operation
 * @tx_submit: accept the descriptor, assign ordered cookie and mark the
 * descriptor pending. To be pushed on .issue_pending() call
 * @callback: routine to call after this operation is complete
 * @callback_param: general parameter to pass to the callback routine
 * @desc_metadata_mode: core managed metadata mode to protect mixed use of
 *	DESC_METADATA_CLIENT or DESC_METADATA_ENGINE. Otherwise
 *	DESC_METADATA_NONE
 * @metadata_ops: DMA driver provided metadata mode ops, need to be set by the
 *	DMA driver if metadata mode is supported with the descriptor
 * ---async_tx api specific fields---
 * @next: at completion submit this descriptor
 * @parent: pointer to the next level up in the dependency chain
 * @lock: protect the parent and next pointers
 */
struct dma_async_tx_descriptor {
	dma_cookie_t cookie;
	enum dma_ctrl_flags flags; /* not a 'long' to pack with cookie */
	dma_addr_t phys;
	struct dma_chan *chan;
	dma_cookie_t (*tx_submit)(struct dma_async_tx_descriptor *tx);
	int (*desc_free)(struct dma_async_tx_descriptor *tx);
	dma_async_tx_callback callback;
	dma_async_tx_callback_result callback_result;
	void *callback_param;
	struct dmaengine_unmap_data *unmap;
	enum dma_desc_metadata_mode desc_metadata_mode;
	struct dma_descriptor_metadata_ops *metadata_ops;
#ifdef CONFIG_ASYNC_TX_ENABLE_CHANNEL_SWITCH
	struct dma_async_tx_descriptor *next;
	struct dma_async_tx_descriptor *parent;
	spinlock_t lock;
#endif
};

/**
 * struct dma_slave_config - dma slave channel runtime config
 * @direction: whether the data shall go in or out on this slave
 * channel, right now. DMA_MEM_TO_DEV and DMA_DEV_TO_MEM are
 * legal values. DEPRECATED, drivers should use the direction argument
 * to the device_prep_slave_sg and device_prep_dma_cyclic functions or
 * the dir field in the dma_interleaved_template structure.
 * @src_addr: this is the physical address where DMA slave data
 * should be read (RX), if the source is memory this argument is
 * ignored.
 * @dst_addr: this is the physical address where DMA slave data
 * should be written (TX), if the source is memory this argument
 * is ignored.
 * @src_addr_width: this is the width in bytes of the source (RX)
 * register where DMA data shall be read. If the source
 * is memory this may be ignored depending on architecture.
 * Legal values: 1, 2, 3, 4, 8, 16, 32, 64.
 * @dst_addr_width: same as src_addr_width but for destination
 * target (TX) mutatis mutandis.
 * @src_maxburst: the maximum number of words (note: words, as in
 * units of the src_addr_width member, not bytes) that can be sent
 * in one burst to the device. Typically something like half the
 * FIFO depth on I/O peripherals so you don't overflow it. This
 * may or may not be applicable on memory sources.
 * @dst_maxburst: same as src_maxburst but for destination target
 * mutatis mutandis.
 * @src_port_window_size: The length of the register area in words the data need
 * to be accessed on the device side. It is only used for devices which is using
 * an area instead of a single register to receive the data. Typically the DMA
 * loops in this area in order to transfer the data.
 * @dst_port_window_size: same as src_port_window_size but for the destination
 * port.
 * @device_fc: Flow Controller Settings. Only valid for slave channels. Fill
 * with 'true' if peripheral should be flow controller. Direction will be
 * selected at Runtime.
 * @slave_id: Slave requester id. Only valid for slave channels. The dma
 * slave peripheral will have unique id as dma requester which need to be
 * pass as slave config.
 *
 * This struct is passed in as configuration data to a DMA engine
 * in order to set up a certain channel for DMA transport at runtime.
 * The DMA device/engine has to provide support for an additional
 * callback in the dma_device structure, device_config and this struct
 * will then be passed in as an argument to the function.
 *
 * The rationale for adding configuration information to this struct is as
 * follows: if it is likely that more than one DMA slave controllers in
 * the world will support the configuration option, then make it generic.
 * If not: if it is fixed so that it be sent in static from the platform
 * data, then prefer to do that.
 */
struct dma_slave_config {
	enum dma_transfer_direction direction;
	phys_addr_t src_addr;
	phys_addr_t dst_addr;
	enum dma_slave_buswidth src_addr_width;
	enum dma_slave_buswidth dst_addr_width;
	u32 src_maxburst;
	u32 dst_maxburst;
	u32 src_port_window_size;
	u32 dst_port_window_size;
	bool device_fc;
	unsigned int slave_id;
};

static inline int dma_submit_error(dma_cookie_t cookie)
{
	return cookie < 0 ? cookie : 0;
}

int dmaenginem_async_device_register(struct dma_device *device);
void dma_release_channel(struct dma_chan *chan);
enum dma_status dmaengine_tx_status(struct dma_chan *chan,
	dma_cookie_t cookie, struct dma_tx_state *state);
struct dma_async_tx_descriptor *dmaengine_prep_slave_single(
	struct dma_chan *chan, dma_addr_t buf, size_t len,
	enum dma_transfer_direction dir, unsigned long flags);
void dma_async_issue_pending(struct dma_chan *chan);
void dmaengine_synchronize(struct dma_chan *chan);
int dmaengine_terminate_all(struct dma_chan *chan);
dma_cookie_t dmaengine_submit(struct dma_async_tx_descriptor *desc);
int dmaengine_slave_config(struct dma_chan *chan, struct dma_slave_config *config);
struct dma_chan *dma_request_chan(struct device *dev, const char *name);

#endif
