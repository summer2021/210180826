/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __LINUX_COMPLETION_H
#define __LINUX_COMPLETION_H

#include <rtdevice.h>

struct completion {
    struct rt_completion _c;
};

static inline void __init_completion(struct completion *x)
{
    rt_completion_init(&x->_c);
}

#define init_completion(x) __init_completion(x)

static inline void complete(struct completion *x)
{
    rt_completion_done(&x->_c);
}

#endif
