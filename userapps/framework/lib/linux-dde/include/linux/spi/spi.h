/* SPDX-License-Identifier: GPL-2.0-or-later
 *
 * Copyright (C) 2005 David Brownell
 */

#ifndef __LINUX_SPI_H
#define __LINUX_SPI_H

struct spi_controller {
    int dummy;
};

/* Compatibility layer */
#define spi_master			spi_controller

struct spi_board_info {
    int dummy;
};

struct spi_device {
    int dummy;
};

#endif
