/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __LINUX_COMPILER_TYPES_H
#define __LINUX_COMPILER_TYPES_H

#define __user
#define __must_check

# define __acquires(x)
# define __releases(x)

/* Attributes */
#include <linux/compiler_attributes.h>

#define compiletime_assert(condition, msg)

#endif
