/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_SCHED_H
#define _LINUX_SCHED_H

#include <linux/refcount.h>

struct task_struct {
    int dumy;
};

/* Convenience macros for the sake of wake_up(): */
#define TASK_NORMAL		0//TODO	(TASK_INTERRUPTIBLE | TASK_UNINTERRUPTIBLE)

/* Task command name length: */
#define TASK_COMM_LEN			16

/* Used in tsk->state: */
#define TASK_RUNNING			0x0000
#define TASK_INTERRUPTIBLE		0x0001

#define set_current_state(state_value)

void schedule(void);

#endif
