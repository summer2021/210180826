/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * phy.h -- generic phy header file
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com
 *
 * Author: Kishon Vijay Abraham I <kishon@ti.com>
 */

#ifndef __DRIVERS_PHY_H
#define __DRIVERS_PHY_H

#include <linux/of.h>
#include <linux/err.h>

struct phy {
    int dummy;
};

static inline int phy_power_on(struct phy *phy)
{
    return 0;
}

static inline int phy_power_off(struct phy *phy)
{
    return 0;
}

static inline int phy_exit(struct phy *phy)
{
    return 0;
}

static inline int phy_init(struct phy *phy)
{
    return 0;
}

static inline int phy_get_bus_width(struct phy *phy)
{
    return -ENOSYS;
}

static inline struct phy *devm_phy_get(struct device *dev, const char *string)
{
    return ERR_PTR(-ENOSYS);
}

#endif
