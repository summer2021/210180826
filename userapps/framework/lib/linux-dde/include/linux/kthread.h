/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_KTHREAD_H
#define _LINUX_KTHREAD_H
/* Simple interface for creating and stopping kernel threads without mess. */
#include <linux/err.h>
#include <linux/sched.h>

bool kthread_should_stop(void);
int kthread_stop(struct task_struct *k);
struct task_struct * kthread_run(int (*threadfn)(void *data), void *data, const char *namefmt, ...);

#endif
