/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_TIMEKEEPING_H
#define _LINUX_TIMEKEEPING_H

static inline u64 ktime_get_ns(void)
{
	return 0;//TODO ktime_to_ns(ktime_get());
}

extern ktime_t ktime_get(void);

#endif
