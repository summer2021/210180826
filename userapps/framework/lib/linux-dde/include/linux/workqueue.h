/* SPDX-License-Identifier: GPL-2.0 */
/*
 * workqueue.h --- work queue handling for Linux.
 */

#ifndef _LINUX_WORKQUEUE_H
#define _LINUX_WORKQUEUE_H

#include <linux/bitops.h>
#include <linux/atomic.h>
#include <linux/cpumask.h>
#include <linux/timer.h>

#include <ipc/workqueue.h>

typedef struct work_struct work_struct_t;

typedef void (*work_func_t)(work_struct_t *work);

struct workqueue_struct
{
    struct rt_workqueue _wq;
};

struct work_struct
{
    struct rt_work _wk;
    work_func_t func;

    void *priv;
};

struct delayed_work
{
    struct work_struct work;

    struct workqueue_struct *wq;
};

#define INIT_WORK(_work, _func) //TODO

extern bool flush_work(struct work_struct *work);
static inline bool schedule_work(struct work_struct *work)
{
    //TODO
    return true;
}

bool queue_work(struct workqueue_struct *wq,
			      struct work_struct *work);

int _delayed_work_init(struct delayed_work *dw, work_func_t f, int op);

#define INIT_DELAYED_WORK(w, f) _delayed_work_init(w, f, 1)

static inline struct delayed_work *to_delayed_work(struct work_struct *work)
{
	return container_of(work, struct delayed_work, work);
}

bool schedule_delayed_work(struct delayed_work *dwork,
					 unsigned long delay);

#endif
