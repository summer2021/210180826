/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_BUG_H
#define _LINUX_BUG_H

#include <linux/kernel.h>
#include <linux/build_bug.h>

#define __WARN() printk("WARN: %s:%d\n", __FILE__, __LINE__)

#define BUG()	do {								\
	printk("BUG: failure at %s:%d/%s()!\n", __FILE__, __LINE__, __func__); \
	barrier_before_unreachable();						\
	__builtin_trap();							\
} while (0)

#define WARN_ON(condition) ({						\
	int __ret_warn_on = !!(condition);				\
	if (unlikely(__ret_warn_on))					\
		__WARN();						\
	unlikely(__ret_warn_on);					\
})

#define WARN(condition, format...) ({					\
	int __ret_warn_on = !!(condition);				\
	no_printk(format);						\
	unlikely(__ret_warn_on);					\
})

#define BUG_ON(condition) do { if (unlikely(condition)) BUG(); } while (0)
#define WARN_ONCE(condition, format...) WARN(condition, format)
#define WARN_ON_ONCE(condition) WARN_ON(condition)

#endif
