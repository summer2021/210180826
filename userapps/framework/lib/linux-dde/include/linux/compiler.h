/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __LINUX_COMPILER_H
#define __LINUX_COMPILER_H

#include <linux/compiler_types.h>

# ifndef likely
#  define likely(x)	(x)
# endif
# ifndef unlikely
#  define unlikely(x)	(x)
# endif

#define WRITE_ONCE(x, val) (x = val)
#define READ_ONCE(x) (x)

# define __force

#define __must_be_array(a)  (a)

#define __iomem

#define barrier_before_unreachable()

#endif /* __LINUX_COMPILER_H */
