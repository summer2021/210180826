/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __LINUX_UACCESS_H__
#define __LINUX_UACCESS_H__

#include <linux/sched.h>
#include <linux/minmax.h>

unsigned long __must_check
copy_from_user(void *to, const void __user *from, unsigned long n);
unsigned long __must_check
copy_to_user(void __user *to, const void *from, unsigned long n);

#endif
