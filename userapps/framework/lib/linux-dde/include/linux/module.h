/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Dynamic loading of modules into the kernel.
 *
 * Rewritten by Richard Henderson <rth@tamu.edu> Dec 1996
 * Rewritten again by Rusty Russell, 2002
 */

#ifndef _LINUX_MODULE_H
#define _LINUX_MODULE_H

#include <linux/stringify.h>
#include <linux/export.h>
#include <linux/compiler.h>
#include <linux/kobject.h>
#include <linux/moduleparam.h>
#include <linux/kmod.h>

#ifndef KBUILD_MODNAME
#define KBUILD_MODNAME ""
#endif

#define MODULE_DESCRIPTION(...)
#define MODULE_AUTHOR(...)
#define MODULE_LICENSE(...)
#define MODULE_SUPPORTED_DEVICE(...)
#define MODULE_VERSION(...)

#define MODULE_DEVICE_TABLE(type, name)

struct module {
    int dumy;
};

static inline void module_put(struct module *module)
{
}

static inline bool try_module_get(struct module *module)
{
	return true;
}

static inline int request_module(const char *name, ...) { return -ENOSYS; }

#define module_init(x) INIT_COMPONENT_EXPORT(x)

#define MODULE_ALIAS(_alias)

#endif
