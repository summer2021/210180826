/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * A generic kernel FIFO implementation
 *
 * Copyright (C) 2013 Stefani Seibold <stefani@seibold.net>
 */

#ifndef _LINUX_KFIFO_H
#define _LINUX_KFIFO_H

#include <ipc/ringbuffer.h>
#include <ipc/waitqueue.h>
#include <linux/mm.h>

struct kfifo
{
    struct rt_ringbuffer _rb;
    int mask;
};

/**
 * kfifo_initialized - Check if the fifo is initialized
 * @fifo: address of the fifo to check
 *
 * Return %true if fifo is initialized, otherwise %false.
 * Assumes the fifo was 0 before.
 */
#define kfifo_initialized(fifo) ((fifo)->mask)

static inline int kfifo_alloc(struct kfifo *f, int size, int gfp)
{
    void *p;

    p = kvmalloc(size, 0);
    if (!p)
        return -1;
    rt_ringbuffer_init(&f->_rb, p, size);
    f->mask = 1;

    return 0;
}

static inline int kfifo_len(struct kfifo *f)
{
    return rt_ringbuffer_get_size(&f->_rb);
}

static inline int kfifo_out(struct kfifo *f, char *buf, int len)
{
    return rt_ringbuffer_get(&f->_rb, (void *)buf, len);
}

static inline int kfifo_is_empty(struct kfifo *f)
{
    return rt_ringbuffer_space_len(&f->_rb) != 0;
}

static inline int kfifo_in(struct kfifo *f, const char *buf, int len)
{
    return rt_ringbuffer_put(&f->_rb, (void *)buf, len);
}

static void kfifo_reset(struct kfifo *f)
{
    rt_ringbuffer_reset(&f->_rb);
}

#endif
