// SPDX-License-Identifier: GPL-2.0
/*
 * device.h - generic, centralized driver model
 *
 * Copyright (c) 2001-2003 Patrick Mochel <mochel@osdl.org>
 * Copyright (c) 2004-2009 Greg Kroah-Hartman <gregkh@suse.de>
 * Copyright (c) 2008-2009 Novell Inc.
 *
 * See Documentation/driver-api/driver-model/ for more information.
 */

#ifndef _DEVICE_H_
#define _DEVICE_H_

#include <linux/kconfig.h>
#include <linux/dev_printk.h>
#include <linux/kobject.h>
#include <linux/gfp.h>
#include <linux/lockdep.h>
#include <linux/overflow.h>
#include <linux/device/class.h>
#include <linux/ioport.h>
#include <linux/device/bus.h>
#include <linux/mutex.h>
#include <linux/pm.h>

struct device_node;

/* interface for exporting device attributes */
struct device_attribute {
    int dummy;
	struct attribute attr;
};

struct device_driver {
	const char		*name;
    struct module		*owner;
	struct bus_type		*bus;

	const struct of_device_id	*of_match_table;
};

struct device_dma_parameters {
	/*
	 * a low level driver may set these to teach IOMMU code about
	 * sg limitations.
	 */
	unsigned int max_segment_size;
	unsigned long segment_boundary_mask;
};

struct device
{
	struct kobject kobj;
	struct device		*parent;
    void		*driver_data;	/* Driver data, set and get with
					   dev_set_drvdata/dev_get_drvdata */
	struct device_driver *driver;	/* which driver has allocated this
					   device */
	void		*platform_data;
	dev_t			devt;	/* dev_t, creates the sysfs "dev" */
    struct class		*class;
	const struct attribute_group **groups;
	struct device_node	*of_node;
	struct fwnode_handle	*fwnode; /* firmware device node */
    u64		*dma_mask;
	u64		coherent_dma_mask;
	struct list_head	dma_pools;
	struct device_dma_parameters *dma_parms;
    struct bus_type	*bus;
	void (*release)(struct device *cd);
	bool			offline_disabled:1;
	bool			offline:1;
	bool			of_node_reused:1;
	bool			state_synced:1;
};

#define DEVICE_ATTR(_name, _mode, _show, _store) \
	struct device_attribute dev_attr_##_name = __ATTR(_name, _mode, _show, _store)
#define DEVICE_ATTR_RW(_name) \
	struct device_attribute dev_attr_##_name
#define DEVICE_ATTR_RO(_name) \
	struct device_attribute dev_attr_##_name
#define DEVICE_ATTR_WO(_name) \
	struct device_attribute dev_attr_##_name = __ATTR_WO(_name)

/*
 * get_device - atomically increment the reference count for the device.
 *
 */
struct device *get_device(struct device *dev);
void put_device(struct device *dev);

static inline void *dev_get_platdata(const struct device *dev)
{
	return dev->platform_data;
}

static inline void *dev_get_drvdata(const struct device *dev)
{
	return dev->driver_data;
}

static inline void dev_set_drvdata(struct device *dev, void *data)
{
	dev->driver_data = data;
}

#define MODULE_ALIAS_CHARDEV_MAJOR(major)

void *devm_kcalloc(struct device *dev,
				 size_t n, size_t size, gfp_t flags);
void devm_kfree(struct device *dev, const void *p);
const char *dev_name(const struct device *dev);
void device_initialize(struct device *dev);
int device_create_file(struct device *device,
		       const struct device_attribute *entry);
void device_remove_file(struct device *dev,
			const struct device_attribute *attr);
int device_register(struct device *dev);
void device_unregister(struct device *dev);
int dev_set_name(struct device *dev, const char *name, ...);

void *devm_kzalloc(struct device *dev, size_t size, gfp_t gfp);

void __iomem *devm_ioremap_resource(struct device *dev,
				    const struct resource *res);

int __must_check device_add(struct device *dev);
void device_del(struct device *dev);

static inline int devm_add_action_or_reset(struct device *dev,
					   void (*action)(void *), void *data)
{
	return 0;
}

struct device *device_create(struct class *class, struct device *parent,
                             dev_t devt, void *drvdata, const char *fmt, ...);
void device_destroy(struct class *class, dev_t devt);

#endif
