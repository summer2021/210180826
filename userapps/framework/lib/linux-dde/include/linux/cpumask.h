/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __LINUX_CPUMASK_H
#define __LINUX_CPUMASK_H

#include <linux/bitmap.h>

#define cpu_online(cpu)		((cpu) == 0)


#endif
