/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_TYPES_H
#define _LINUX_TYPES_H


#include <uapi/linux/types.h>

#define	NUMA_NO_NODE	(-1)

#define BITS_PER_LONG 32

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define DECLARE_BITMAP(name,bits) \
	unsigned long name[BITS_TO_LONGS(bits)]

typedef unsigned __poll_t;
typedef long loff_t;
typedef unsigned int gfp_t;
//typedef long off_t;
typedef unsigned int size_t;
//typedef unsigned long dev_t;

typedef struct {
	int counter;
} atomic_t;
#if 0
struct timespec
{
    int tv_sec;
	long tv_nsec;
};
#endif
typedef int ssize_t;

struct list_head {
	struct list_head *next, *prev;
};

struct hlist_head {
	struct hlist_node *first;
};

struct hlist_node {
	struct hlist_node *next, **pprev;
};

typedef u32 dma_addr_t;
typedef u32 phys_addr_t;

typedef u64 sector_t;

typedef phys_addr_t resource_size_t;

typedef unsigned short		umode_t;
typedef unsigned int __bitwise fmode_t;

#endif
