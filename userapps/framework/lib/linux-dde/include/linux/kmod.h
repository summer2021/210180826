/* SPDX-License-Identifier: GPL-2.0-or-later */
#ifndef __LINUX_KMOD_H__
#define __LINUX_KMOD_H__

#include <linux/compiler.h>
#include <linux/stddef.h>

#define try_then_request_module(x, mod...) (x)

#endif
