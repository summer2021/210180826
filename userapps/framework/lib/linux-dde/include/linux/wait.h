/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_WAIT_H
#define _LINUX_WAIT_H

#include <linux/stddef.h>
#include <linux/sched.h>
#include <linux/spinlock.h>

//#include <ipc/waitqueue.h>

typedef struct wait_queue_head
{
    //rt_wqueue_t head;
	int head;
}wait_queue_head_t;

/**
 * __wake_up - wake up threads blocked on a waitqueue.
 * @wq_head: the waitqueue
 * @mode: which threads
 * @nr_exclusive: how many wake-one or wake-many threads to wake up
 * @key: is directly passed to the wakeup function
 *
 * If this function wakes up a task, it executes a full memory barrier before
 * accessing the task state.
 */
void __wake_up(struct wait_queue_head *wq_head, unsigned int mode,
			int nr_exclusive, void *key);

#define wake_up(x)			__wake_up(x, TASK_NORMAL, 1, NULL)
#define wake_up_all(x)			__wake_up(x, TASK_NORMAL, 0, NULL)
#define wake_up_interruptible_all(x)	__wake_up(x, TASK_INTERRUPTIBLE, 0, NULL)
#define wake_up_interruptible(x) __wake_up(x, TASK_INTERRUPTIBLE, 1, NULL)

#define wait_event(wq_head, condition) rt_wqueue_wait(&(wq_head.head), condition, -1)

#define init_waitqueue_head(wq_head)						\
	do {									\
		static struct lock_class_key __key;				\
										\
		__init_waitqueue_head((wq_head), #wq_head, &__key);		\
	} while (0)

#define wait_event_interruptible(wq_head, condition) rt_wqueue_wait(&(wq_head.head), condition, -1)
void __init_waitqueue_head(struct wait_queue_head *wq_head, const char *name, struct lock_class_key *key);

#endif
