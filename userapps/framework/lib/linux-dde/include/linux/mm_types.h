/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_MM_TYPES_H
#define _LINUX_MM_TYPES_H

struct vm_area_struct {
	/* The first cache line has the info for VMA tree walking. */

	unsigned long vm_start;
	unsigned long vm_end;
	unsigned long vm_pgoff;
	unsigned long vm_flags;

	/* Function pointers to deal with this struct. */
	const struct vm_operations_struct *vm_ops;

	void * vm_private_data;		/* was vm_pte (shared mem) */
};

struct page {
	unsigned long flags;
};

#endif
