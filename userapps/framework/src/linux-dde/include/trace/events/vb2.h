/* SPDX-License-Identifier: GPL-2.0 */
#undef TRACE_SYSTEM
#define TRACE_SYSTEM vb2

#if !defined(_TRACE_VB2_H) || defined(TRACE_HEADER_MULTI_READ)
#define _TRACE_VB2_H

#include <linux/tracepoint.h>
#include <media/videobuf2-core.h>


DEFINE_EVENT(vb2_event_class, vb2_buf_done,
	TP_PROTO(struct vb2_queue *q, struct vb2_buffer *vb),
	TP_ARGS(q, vb)
);

DEFINE_EVENT(vb2_event_class, vb2_buf_queue,
	TP_PROTO(struct vb2_queue *q, struct vb2_buffer *vb),
	TP_ARGS(q, vb)
);

DEFINE_EVENT(vb2_event_class, vb2_dqbuf,
	TP_PROTO(struct vb2_queue *q, struct vb2_buffer *vb),
	TP_ARGS(q, vb)
);

DEFINE_EVENT(vb2_event_class, vb2_qbuf,
	TP_PROTO(struct vb2_queue *q, struct vb2_buffer *vb),
	TP_ARGS(q, vb)
);

#endif
