/* SPDX-License-Identifier: GPL-2.0-or-later */
/* Generic I/O port emulation.
 *
 * Copyright (C) 2007 Red Hat, Inc. All Rights Reserved.
 * Written by David Howells (dhowells@redhat.com)
 */
#ifndef __ASM_GENERIC_IO_H
#define __ASM_GENERIC_IO_H

#include <linux/types.h>
#include <linux/string.h>
#include <drv_syscall.h>

#ifndef __raw_readl
#define __raw_readl __raw_readl
static inline u32 __raw_readl(const volatile void __iomem *addr)
{
    return *(const volatile u32 __force *)addr;
}
#endif

#ifndef __raw_writel
#define __raw_writel __raw_writel
static inline void __raw_writel(u32 value, volatile void __iomem *addr)
{
    *(volatile u32 __force *)addr = value;
}
#endif

static inline u32 readl_relaxed(const volatile void __iomem *addr)
{
    return *((u32*)addr);
}

#ifndef writel_relaxed
#define writel_relaxed writel_relaxed
static inline void writel_relaxed(u32 value, volatile void __iomem *addr)
{
    *((u32*)addr) = value;
}
#endif

#ifndef readl
#define readl __raw_readl
#endif

#ifndef writel
#define writel __raw_writel
#endif

#ifndef ioread32
#define ioread32 readl
#endif

#ifndef ioremap
#define ioremap ioremap
static inline void *ioremap(phys_addr_t offset, size_t size)
{
    return rt_ioremap(rt_udevice_get_current(),(void *)(rt_size_t)offset,size,0);
}
#endif

#ifndef iounmap
#define iounmap iounmap
static inline void iounmap(volatile void __iomem *addr)
{
    rt_iounremap(rt_udevice_get_current(),(void *)(rt_size_t)addr,0);
}
#endif

#endif
