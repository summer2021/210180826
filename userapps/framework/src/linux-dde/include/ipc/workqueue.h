/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-26     lizhirui     the first version
 */
#ifndef WORKQUEUE_H__
#define WORKQUEUE_H__

#include <rtthread.h>
#include <ipc/ipc_interface.h>

#define rt_workqueue rt_user_ipc
#define rt_work rt_user_ipc

struct rt_workqueue *rt_workqueue_create(const char *name,rt_uint16_t stack_size,rt_uint8_t priority);
rt_err_t rt_workqueue_submit_work(struct rt_workqueue *queue,struct rt_work *work,rt_tick_t time);
rt_err_t rt_work_init(struct rt_work *work,void (*work_func)(struct rt_work *work,void *work_data),void *work_data);

#endif
