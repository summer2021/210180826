/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-25     lizhirui     the first version
 */

#ifndef __IPC_INTERFACE_H__
#define __IPC_INTERFACE_H__

#include <rtthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <drv_syscall.h>

#define RT_USER_IPC_CREATE(ipc_type, ...)\
    do {\
        struct rt_user_ipc *ipc; \
\
        ipc = (struct rt_user_ipc*)malloc(sizeof *ipc);\
        if (!ipc)\
        {\
            return NULL;\
        }\
        ipc->data = (void*)syscall(__VA_ARGS__);\
        if (ipc->data)\
        {\
            ipc->type = ipc_type;\
        }\
        else\
        {\
            free(ipc);\
            ipc = NULL;\
        }\
        return ipc;\
    } while (0)

#define RT_USER_IPC_INIT(ipc, ipc_type, ...)\
    do {\
        if (!ipc)\
        {\
            return RT_EIO;\
        }\
        ipc->data = (void*)syscall(__VA_ARGS__);\
        if (ipc->data)\
        {\
            ipc->type = ipc_type;\
        }\
        else\
        {\
            return RT_ERROR;\
        }\
        return RT_EOK;\
    } while (0)

#define RT_USER_IPC_DELETE(ipc, ipc_type, sc_no)\
    do {\
        rt_err_t ret; \
        if (!ipc || !ipc->data)\
        {\
            return RT_EIO;\
        }\
        if (ipc->type != ipc_type)\
        {\
            return RT_EIO;\
        }\
        ret = (rt_err_t)syscall(sc_no, ipc->data);\
        if (ret != RT_EOK) \
        {\
            return ret;\
        }\
        free(ipc); \
        return RT_EOK;\
    } while (0)

#define RT_USER_IPC_DETACH(ipc, ipc_type, sc_no)\
    do {\
        rt_err_t ret; \
        if (!ipc || !ipc->data)\
        {\
            return RT_EIO;\
        }\
        if (ipc->type != ipc_type)\
        {\
            return RT_EIO;\
        }\
        ret = (rt_err_t)syscall(sc_no, ipc->data);\
        if (ret != RT_EOK) \
        {\
            return ret;\
        }\
        ipc->type = 0; \
        ipc->data = RT_NULL;\
        return RT_EOK;\
    } while (0)

#define RT_USER_IPC_CHECK(ipc, ipc_type)\
    do {\
        if (!ipc || !ipc->data)\
        {\
            return RT_EIO;\
        }\
        if (ipc->type != ipc_type)\
        {\
            return RT_EIO;\
        }\
    } while (0)

#endif