/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_FS_H
#define _LINUX_FS_H

#include <linux/wait.h>
#include <linux/mutex.h>
#include <linux/err.h>
#include <linux/lockdep.h>
#include <linux/stddef.h>
#include <linux/kdev_t.h>
#include <linux/mm_types.h>
#include <linux/export.h>
#include <linux/io.h>
#include <linux/build_bug.h>
#include <linux/fcntl.h>
#include <linux/poll.h>

struct inode {
    void			*i_private; /* fs or device private pointer */
    union {
		struct cdev		*i_cdev;
	};
	dev_t			i_rdev;
};

struct file
{
    unsigned int 		f_flags;
	const struct file_operations	*f_op;
	struct inode		*f_inode;	/* cached value */
    void *private_data;
};

struct linux_dev
{
    struct rt_device parent;
    rt_device_t udev;
	void *private_data;
    void *io;
    int data;
    char buff[256];
};

struct file_operations {
	struct module *owner;
    ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
	ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
	int (*open) (struct inode *, struct file *);
	unsigned long (*get_unmapped_area)(struct file *, unsigned long, unsigned long, unsigned long, unsigned long);
	int (*mmap) (struct file *, struct vm_area_struct *);
	long (*unlocked_ioctl) (struct file *, unsigned int, unsigned long);
	int (*release) (struct inode *, struct file *);
	__poll_t (*poll) (struct file *, struct poll_table_struct *);
	loff_t (*llseek) (struct file *, loff_t, int);

#ifdef RT_USING_DEVICE_OPS
	struct rt_device_ops rt_device_ops;
#endif
	struct linux_dev rt_device;
	rt_bool_t rt_device_inited;
	int (*rt_init)();
};

static inline unsigned iminor(const struct inode *inode)
{
	return MINOR(inode->i_rdev);
}

static inline struct inode *file_inode(const struct file *f)
{
	return f->f_inode;
}

extern loff_t no_llseek(struct file *file, loff_t offset, int whence);
extern int alloc_chrdev_region(dev_t *, unsigned, unsigned, const char *);

extern int register_chrdev_region(dev_t, unsigned, const char *);
extern void unregister_chrdev_region(dev_t, unsigned);

extern int register_chrdev(unsigned int major, const  char *name, struct file_operations *fops);
extern int unregister_chrdev(unsigned int major, const char *name);

#endif
