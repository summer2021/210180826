/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_INIT_H
#define _LINUX_INIT_H

#include <linux/compiler.h>

#define __init
#define __exit

#define subsys_initcall(...)
#define module_exit(...)

#endif
