/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Written by Mark Hemment, 1996 (markhe@nextd.demon.co.uk).
 *
 * (C) SGI 2006, Christoph Lameter
 * 	Cleaned up and restructured to ease the addition of alternative
 * 	implementations of SLAB allocators.
 * (C) Linux Foundation 2008-2013
 *      Unified interface for all slab allocators
 */

#ifndef _LINUX_SLAB_H
#define	_LINUX_SLAB_H

#include <linux/types.h>
#include <linux/overflow.h>
#include <linux/workqueue.h>

#define GFP_KERNEL 0
#define GFP_ATOMIC 0

void kfree(const void *);
void *kzalloc(size_t size, gfp_t flags);
void *kmalloc(size_t size, gfp_t flags);
void *kcalloc(size_t n, size_t size, gfp_t flags);
void *kmalloc_node(size_t size, gfp_t flags, int node);

#endif
