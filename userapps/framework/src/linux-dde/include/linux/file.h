/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Wrapper functions for accessing the file_struct fd array.
 */

#ifndef __LINUX_FILE_H
#define __LINUX_FILE_H

#include <linux/compiler.h>
#include <linux/errno.h>

struct file;

struct fd {
	struct file *file;
	unsigned int flags;
};

struct fd fdget(unsigned int fd);
void fdput(struct fd fd);

extern void fd_install(unsigned int fd, struct file *file);
extern int get_unused_fd_flags(unsigned flags);
extern void put_unused_fd(unsigned int fd);

#endif
