/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_DELAY_H
#define _LINUX_DELAY_H

void msleep(unsigned int msecs);
void usleep_range(unsigned long min, unsigned long max);
void mdelay(unsigned long x);
void ndelay(unsigned long x);
void udelay(unsigned long x);

#endif
