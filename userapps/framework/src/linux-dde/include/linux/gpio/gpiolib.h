/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __LINUX_GPIO_GPIOLIB_H
#define __LINUX_GPIO_GPIOLIB_H

struct gpio_desc {
    unsigned long		flags;
	/* Connection label */
	const char		*label;
	/* Name of the GPIO */
	const char		*name;    
};

struct gpio_array {
	struct gpio_desc	*desc;
	unsigned int		size;
};

#endif
