// SPDX-License-Identifier: GPL-2.0
/*
 * dev_printk.h - printk messages helpers for devices
 *
 * Copyright (c) 2001-2003 Patrick Mochel <mochel@osdl.org>
 * Copyright (c) 2004-2009 Greg Kroah-Hartman <gregkh@suse.de>
 * Copyright (c) 2008-2009 Novell Inc.
 *
 */

#ifndef _DEVICE_PRINTK_H_
#define _DEVICE_PRINTK_H_

#include <linux/compiler.h>
#include <linux/types.h>
#include <linux/printk.h>
#include <linux/bug.h>

#define dev_err(dev, fmt, ...) printk(fmt, ##__VA_ARGS__)
#define dev_info(dev, fmt, ...) printk(fmt, ##__VA_ARGS__)
#define dev_dbg(dev, fmt, ...) printk(fmt, ##__VA_ARGS__)
#define dev_vdbg(dev, fmt, ...) printk(fmt, ##__VA_ARGS__)
#define dev_notice(dev, fmt, ...) printk(fmt, ##__VA_ARGS__)
#define dev_warn(dev, fmt, ...) printk(fmt, ##__VA_ARGS__)

#define dev_WARN_ONCE(dev, condition, format, arg...) \
	WARN_ONCE(condition, format, ## arg)

#endif
