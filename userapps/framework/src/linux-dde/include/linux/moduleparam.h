/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_MODULE_PARAMS_H
#define _LINUX_MODULE_PARAMS_H
/* (C) Copyright 2001, 2002 Rusty Russell IBM Corporation */

#define module_param(name, type, perm)
#define MODULE_PARM_DESC(_parm, desc)

#define module_param_named(name, value, type, perm)

#endif /* _LINUX_MODULE_PARAMS_H */
