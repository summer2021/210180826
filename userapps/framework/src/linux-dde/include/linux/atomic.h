/* SPDX-License-Identifier: GPL-2.0 */
/* Atomic operations usable in machine independent code */
#ifndef _LINUX_ATOMIC_H
#define _LINUX_ATOMIC_H
#include <linux/types.h>
#include <linux/compiler.h>
//TODO

static inline int atomic_fetch_sub(int i, atomic_t *v)
{
	//unsigned long flags;
	unsigned long orig;

	/*
	 * spin lock/unlock provides the needed smp_mb() before/after
	 */
	//atomic_ops_lock(flags);
	orig = v->counter;
	v->counter -= i;
	//atomic_ops_unlock(flags);

	return orig;
}

static inline int atomic_fetch_add(int i, atomic_t *v)
{
	//unsigned long flags;
	unsigned long orig;

	/*
	 * spin lock/unlock provides the needed smp_mb() before/after
	 */
	//atomic_ops_lock(flags);
	orig = v->counter;
	v->counter += i;
	//atomic_ops_unlock(flags);

	return orig;
}

static inline void atomic_set(atomic_t *v, int i)
{
	v->counter = i;
}

static inline unsigned int atomic_read(const atomic_t *v)
{
	unsigned int ret;

	ret = v->counter;

	return ret;
}

static inline bool atomic_try_cmpxchg_relaxed(atomic_t *v, int *old, int new)
{
	int r, o = *old;
	//TODO r = atomic_cmpxchg_relaxed(v, o, new);
	if (unlikely(r != o))
		*old = r;
	return likely(r == o);
}

static inline int atomic_fetch_add_relaxed(int v, int *p)
{
	return 0; //TODO
}

static inline int atomic_inc_return(atomic_t *v)
{
	return 0; //TODO atomic_add_return(1, v);
}

static inline void atomic_dec(atomic_t *v)
{
	//TODO
	v->counter--;
}

static inline void atomic_inc(atomic_t *v)
{
	v->counter++;
}

#define atomic_fetch_sub_release atomic_fetch_sub
#define atomic_fetch_add_relaxed atomic_fetch_add

#endif
