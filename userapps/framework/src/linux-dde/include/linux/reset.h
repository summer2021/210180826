/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_RESET_H_
#define _LINUX_RESET_H_

struct reset_control {
    const char *name;
};

static inline int reset_control_assert(struct reset_control *rstc)
{
	return 0;
}

static inline int reset_control_deassert(struct reset_control *rstc)
{
	return 0;
}

struct reset_control *devm_reset_control_get_optional(
				struct device *dev, const char *id);


#endif
