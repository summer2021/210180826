/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_BITOPS_H
#define _LINUX_BITOPS_H

#include <linux/types.h>
#include <linux/const.h>
#include <linux/bits.h>
#include <asm/__ffs.h>
#include <asm/__fls.h>
#include <asm/fls.h>
#include <asm/fls64.h>

#define BITS_PER_BYTE		8

#define DIV_ROUND_UP(x, y) ((x + y - 1)/y)

#define BITS_PER_TYPE(type)	(sizeof(type) * BITS_PER_BYTE)
#define BITS_TO_LONGS(nr)	DIV_ROUND_UP(nr, BITS_PER_TYPE(long))
#define BITS_TO_U64(nr)		DIV_ROUND_UP(nr, BITS_PER_TYPE(u64))
#define BITS_TO_U32(nr)		DIV_ROUND_UP(nr, BITS_PER_TYPE(u32))
#define BITS_TO_BYTES(nr)	DIV_ROUND_UP(nr, BITS_PER_TYPE(char))

static inline void set_bit(unsigned long nr, unsigned long *p)
{
    p += nr >> 5;
    (*p) |= (1 << (nr & 0x1f)); 
}

static inline int test_and_set_bit(unsigned long nr, unsigned long *p)
{
    unsigned long old, mask;

    p += nr >> 5;
    old = *p;
    mask = 1UL << (nr & 0x1f);
    *p = old | mask;

    return ((old & mask) != 0);
}

static inline int test_and_clear_bit(unsigned long nr, unsigned long *p)
{
    unsigned long old, mask;

    p += nr >> 5;
    old = *p;
    mask = 1UL << (nr & 0x1f);
    *p = old & ~mask;

    return ((old & mask) != 0);
}

/*
 * This routine doesn't need to be atomic.
 */
static inline int test_bit(unsigned int nr, const volatile unsigned long *addr)
{
	unsigned long mask;

	addr += nr >> 5;

	mask = 1UL << (nr & 0x1f);

	return ((mask & *addr) != 0);
}

static inline void clear_bit(unsigned int nr, volatile unsigned long *addr)
{
    addr += nr >> 5;
    *addr &= ~(1UL << (nr & 0x1f));
}

static inline void __clear_bit(int nr, volatile unsigned long *addr)
{
	unsigned long mask = BIT_MASK(nr);
	unsigned long *p = ((unsigned long *)addr) + BIT_WORD(nr);

	*p &= ~mask;
}

/*
 * Find the next set bit in a memory region.
 */
extern unsigned long find_next_bit(const unsigned long *addr, unsigned long size,
			    unsigned long offset);

/**
 * find_next_zero_bit - find the next cleared bit in a memory region
 * @addr: The address to base the search on
 * @offset: The bitnumber to start searching at
 * @size: The bitmap size in bits
 *
 * Returns the bit number of the next zero bit
 * If no bits are zero, returns @size.
 */
extern unsigned long find_next_zero_bit(const unsigned long *addr, unsigned
		long size, unsigned long offset);

static inline unsigned fls_long(unsigned long l)
{
	if (sizeof(l) == 4)
		return fls(l);
	return fls64(l);
}

static inline int get_count_order(unsigned int count)
{
	if (count == 0)
		return -1;

	return fls(--count);
}

#define __set_bit set_bit
#define __test_and_set_bit test_and_set_bit

#define find_first_zero_bit(addr, size) find_next_zero_bit((addr), (size), 0)
#ifndef find_first_bit
#define find_first_bit(addr, size) find_next_bit((addr), (size), 0)
#endif

#endif
