/* SPDX-License-Identifier: GPL-2.0-only */
#ifndef __LINUX_REGMAP_H
#define __LINUX_REGMAP_H

struct i2c_client;

struct regmap_config {
	int reg_bits;
	int val_bits;
	unsigned int max_register;
};

struct regmap {
    struct regmap_config config;
	int (*read)(struct regmap *map, unsigned int reg, unsigned int *val);
	int (*write)(struct regmap *map, unsigned int reg, unsigned int val);
    void *priv;
};

struct regmap *devm_regmap_init_sccb(struct i2c_client *i2c, const struct regmap_config *config);
int regmap_write(struct regmap *map, unsigned int reg,
			       unsigned int val);
int regmap_read(struct regmap *map, unsigned int reg,
			      unsigned int *val);
int regmap_update_bits(struct regmap *map, unsigned int reg,
				     unsigned int mask, unsigned int val);

#endif
