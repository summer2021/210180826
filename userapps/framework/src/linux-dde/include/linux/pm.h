/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 *  pm.h - Power management interface
 *
 *  Copyright (C) 2000 Andrew Henroid
 */

#ifndef _LINUX_PM_H
#define _LINUX_PM_H

#include <linux/list.h>
#include <linux/workqueue.h>
#include <linux/spinlock.h>
#include <linux/wait.h>
#include <linux/timer.h>
#include <linux/hrtimer.h>
#include <linux/completion.h>


typedef struct pm_message {
	int event;
} pm_message_t;

#define device_set_wakeup_capable(...)

#endif
