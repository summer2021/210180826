/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * i2c.h - definitions for the Linux i2c bus interface
 * Copyright (C) 1995-2000 Simon G. Vogl
 * Copyright (C) 2013-2019 Wolfram Sang <wsa@kernel.org>
 *
 * With some changes from Kyösti Mälkki <kmalkki@cc.hut.fi> and
 * Frodo Looijaard <frodol@dds.nl>
 */
#ifndef _LINUX_I2C_H
#define _LINUX_I2C_H

#include <linux/types.h>
#include <linux/device.h>
#include <linux/of.h>
#include <linux/regmap.h>

#define I2C_NAME_SIZE	20

struct rt_i2c_msg;

#define i2c_msg rt_i2c_msg
#define I2C_M_RD RT_I2C_RD

struct i2c_adapter {
	struct rt_i2c_bus_device *bus;
};

struct i2c_client
{
    struct device dev;
	struct regmap rm;

	struct i2c_adapter *adapter;
	struct rt_i2c_bus_device *bus;
	uint16_t addr;
	uint16_t flags;

	struct i2c_adapter _adapter;
};

/**
 * struct i2c_board_info - template for device creation
 * @type: chip type, to initialize i2c_client.name
 * @flags: to initialize i2c_client.flags
 * @addr: stored in i2c_client.addr
 * @dev_name: Overrides the default <busnr>-<addr> dev_name if set
 * @platform_data: stored in i2c_client.dev.platform_data
 * @of_node: pointer to OpenFirmware device node
 * @fwnode: device node supplied by the platform firmware
 * @properties: additional device properties for the device
 * @resources: resources associated with the device
 * @num_resources: number of resources in the @resources array
 * @irq: stored in i2c_client.irq
 *
 * I2C doesn't actually support hardware probing, although controllers and
 * devices may be able to use I2C_SMBUS_QUICK to tell whether or not there's
 * a device at a given address.  Drivers commonly need more information than
 * that, such as chip type, configuration, associated IRQ, and so on.
 *
 * i2c_board_info is used to build tables of information listing I2C devices
 * that are present.  This information is used to grow the driver model tree.
 * For mainboards this is done statically using i2c_register_board_info();
 * bus numbers identify adapters that aren't yet available.  For add-on boards,
 * i2c_new_client_device() does this dynamically with the adapter already known.
 */
struct i2c_board_info {
	char		type[I2C_NAME_SIZE];
	unsigned short	flags;
	unsigned short	addr;
	const char	*dev_name;
	void		*platform_data;
	struct device_node *of_node;
	struct fwnode_handle *fwnode;
	const struct property_entry *properties;
	const struct resource *resources;
	unsigned int	num_resources;
	int		irq;
};

void i2c_unregister_device(struct i2c_client *client);

s32 i2c_smbus_read_byte_data(const struct i2c_client *client, u8 command);
s32 i2c_smbus_write_byte_data(const struct i2c_client *client,
			      u8 command, u8 value);

/* Internal numbers to terminate lists */
#define I2C_CLIENT_END		0xfffeU

static inline void *i2c_get_clientdata(const struct i2c_client *client)
{
	return dev_get_drvdata(&client->dev);
}

static inline void i2c_set_clientdata(struct i2c_client *client, void *data)
{
	dev_set_drvdata(&client->dev, data);
}

int i2c_sccb_read(const struct i2c_client *c, unsigned reg, int *v, int reglen);
int i2c_sccb_write(const struct i2c_client *c, unsigned reg, int *v, int reglen);

int i2c_client_init(struct i2c_client *c, const char *name, uint16_t slv_addr);

int i2c_transfer(struct i2c_adapter *adap, struct i2c_msg *msgs, int num);

#endif
