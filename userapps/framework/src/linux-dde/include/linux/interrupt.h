/* SPDX-License-Identifier: GPL-2.0 */
/* interrupt.h */
#ifndef _LINUX_INTERRUPT_H
#define _LINUX_INTERRUPT_H

#include <linux/irqreturn.h>
#include <linux/hardirq.h>
#include <linux/hrtimer.h>

struct device;
typedef irqreturn_t (*irq_handler_t)(int, void *);

#define IRQF_SHARED		0x00000080

struct tasklet_struct
{
    struct tasklet_struct *next;
};

int devm_request_irq(struct device *dev, unsigned int irq, irq_handler_t handler,
         unsigned long irqflags, const char *devname, void *dev_id);

extern const void *free_irq(unsigned int, void *);

int request_irq(unsigned int irq, irq_handler_t handler, unsigned long flags,
	    const char *name, void *dev);

#endif
