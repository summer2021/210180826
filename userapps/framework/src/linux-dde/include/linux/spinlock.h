/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __LINUX_SPINLOCK_H
#define __LINUX_SPINLOCK_H

#include <linux/kernel.h>
#include <linux/lockdep.h>
#include <linux/spinlock_types.h>
#include <linux/stringify.h>

void spin_lock_init(spinlock_t *lock);
void _spin_lock_irqsave(spinlock_t *lock, unsigned long *flags);
void spin_unlock_irqrestore(spinlock_t *lock, unsigned long flags);
void assert_spin_locked(spinlock_t *lock);
void spin_lock_irq(spinlock_t *lock);
void spin_unlock_irq(spinlock_t *lock);
void spin_lock(spinlock_t *lock);
void spin_unlock(spinlock_t *lock);

#define spin_lock_irqsave(l, f) _spin_lock_irqsave(l, &(f))

#endif
