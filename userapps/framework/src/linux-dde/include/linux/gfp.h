/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __LINUX_GFP_H
#define __LINUX_GFP_H

#include <linux/stddef.h>

#define __GFP_NOWARN  0x2000u
#define __GFP_ZERO    0x100u
#define GFP_KERNEL    0
#define GFP_NOWAIT	  0
#define __GFP_DMA     0x01
#define GFP_ATOMIC    0
#define GFP_DMA       0

#endif
