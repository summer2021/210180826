/* SPDX-License-Identifier: GPL-2.0-or-later */
#ifndef _LINUX_MEMBLOCK_H
#define _LINUX_MEMBLOCK_H

void *memblock_alloc(phys_addr_t size,phys_addr_t align);

#endif /* _LINUX_MEMBLOCK_H */
