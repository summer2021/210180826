/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_ERRNO_H
#define _LINUX_ERRNO_H

#include <libc/libc_errno.h>

#ifndef ENOIOCTLCMD
#define ENOIOCTLCMD	515
#endif
#ifndef ERESTARTSYS
#define ERESTARTSYS	512
#endif

#ifndef ENXIO
#define ENXIO 1006
#endif
#ifndef EPROBE_DEFER
#define EPROBE_DEFER	517
#endif

#ifndef EBADR
#define EBADR 51
#endif

#ifndef ENOTSUPP
#define ENOTSUPP	524
#endif

#ifndef ESHUTDOWN
#define ESHUTDOWN 108
#endif
#ifndef EISNAM
#define	EISNAM		120
#endif

#endif
