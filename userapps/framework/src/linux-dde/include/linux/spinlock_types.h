#ifndef __LINUX_SPINLOCK_TYPES_H
#define __LINUX_SPINLOCK_TYPES_H

typedef struct
{
    int iflags;
}spinlock_t;

#define DEFINE_SPINLOCK(x)	spinlock_t x

#endif
