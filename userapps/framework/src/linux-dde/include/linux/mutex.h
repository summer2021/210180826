/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Mutexes: blocking mutual exclusion locks
 *
 * started by Ingo Molnar:
 *
 *  Copyright (C) 2004, 2005, 2006 Red Hat, Inc., Ingo Molnar <mingo@redhat.com>
 *
 * This file contains the main data structure and API definitions.
 */
#ifndef __LINUX_MUTEX_H
#define __LINUX_MUTEX_H

#include <rtdef.h>
#include <linux/lockdep.h>

struct mutex
{
    struct rt_mutex m;
};

#define mutex_init(mutex)					  \
do {									      \
	__mutex_init((mutex), #mutex);    \
} while (0)

void __mutex_init(struct mutex *lock, const char *name);
void mutex_lock(struct mutex *lock);
void mutex_unlock(struct mutex *lock);
void mutex_destroy(struct mutex *lock);
int mutex_lock_interruptible(struct mutex *lock);

#define DEFINE_MUTEX(mutexname) \
	struct mutex mutexname; \
static int _##mutexname##_init(void) \
{ \
    __mutex_init(&mutexname, #mutexname); \
    return 0; \
} \
INIT_BOARD_EXPORT(_##mutexname##_init)

#endif
