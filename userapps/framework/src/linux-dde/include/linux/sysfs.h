/* SPDX-License-Identifier: GPL-2.0 */
/*
 * sysfs.h - definitions for the device driver filesystem
 *
 * Copyright (c) 2001,2002 Patrick Mochel
 * Copyright (c) 2004 Silicon Graphics, Inc.
 * Copyright (c) 2007 SUSE Linux Products GmbH
 * Copyright (c) 2007 Tejun Heo <teheo@suse.de>
 *
 * Please see Documentation/filesystems/sysfs.rst for more information.
 */

#ifndef _SYSFS_H_
#define _SYSFS_H_

//#include <linux/kernfs.h>
#include <linux/compiler.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/lockdep.h>
//#include <linux/kobject_ns.h>
//#include <linux/stat.h>
#include <linux/kobject.h>
#include <linux/atomic.h>

struct attribute {
    const char		*name;
};

struct attribute_group {
	const char		*name;
    struct attribute	**attrs;
};

#define __ATTR(_name, _mode, _show, _store) {				\
	.attr = {.name = __stringify(_name),				\
		 },		\
}

#define __ATTR_WO(_name) {						\
	.attr	= { .name = __stringify(_name)},		\
}

#define __ATTRIBUTE_GROUPS(_name)				\
static const struct attribute_group *_name##_groups[] = {	\
	&_name##_group,						\
	NULL,							\
}

#define ATTRIBUTE_GROUPS(_name)					\
static const struct attribute_group _name##_group = {		\
	.attrs = _name##_attrs,					\
};                           \
__ATTRIBUTE_GROUPS(_name)


static inline void sysfs_notify(struct kobject *kobj, const char *dir,
				const char *attr)
{
}

#endif
