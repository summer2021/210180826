/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_CDEV_H
#define _LINUX_CDEV_H

#include <linux/kobject.h>
#include <linux/device.h>

#include <rtdevice.h>

struct cdev
{
    struct rt_device rtd;
    dev_t id;

    struct kobject kobj;
    struct module *owner;
    const struct file_operations *ops;
};

struct cdev *cdev_alloc(void);
int cdev_device_add(struct cdev *cdev, struct device *dev);
void cdev_del(struct cdev *);
int cdev_add(struct cdev *, dev_t, unsigned);

#endif
