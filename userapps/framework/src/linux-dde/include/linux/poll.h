/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_POLL_H
#define _LINUX_POLL_H

#include <linux/ktime.h>
#include <linux/wait.h>
#include <uapi/linux/eventpoll.h>
#include <linux/uaccess.h>

//#include <ipc/poll.h>
#include <linux/string.h>

struct file;

#define DEFAULT_POLLMASK 0

typedef struct poll_table_struct poll_table;
typedef void (*poll_queue_proc)(int *, poll_table *);

/*
 * Do not touch the structure directly, use the access functions
 * poll_does_not_wait() and poll_requested_events() instead.
 */
typedef struct poll_table_struct {
	poll_queue_proc _qproc;
	short _key;
} poll_table;

/*
 * Return the set of events that the application wants to poll for.
 * This is useful for drivers that need to know whether a DMA transfer has
 * to be started implicitly on poll(). You typically only want to do that
 * if the application is actually polling for POLLIN and/or POLLOUT.
 */
static inline __poll_t poll_requested_events(const poll_table *p)
{
	return p ? p->_key : ~(__poll_t)0;
}

void poll_wait(struct file * filp, wait_queue_head_t * wait_address, poll_table *p);

#endif
