/* SPDX-License-Identifier: GPL-2.0 */
/* Freezer declarations */

#ifndef FREEZER_H_INCLUDED
#define FREEZER_H_INCLUDED

#include <linux/sched.h>

static inline void set_freezable(void) {}
static inline bool try_to_freeze(void) { return false; }

#endif
