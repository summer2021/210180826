/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * pm_runtime.h - Device run-time power management helper functions.
 *
 * Copyright (C) 2009 Rafael J. Wysocki <rjw@sisk.pl>
 */

#ifndef _LINUX_PM_RUNTIME_H
#define _LINUX_PM_RUNTIME_H

#include <linux/pm.h>

#define pm_runtime_put(...)
static inline int pm_runtime_get_sync(struct device *dev)
{
	return 0;
}
static inline void pm_runtime_enable(struct device *dev) {}

#define pm_runtime_put_sync_suspend(...)

#endif
