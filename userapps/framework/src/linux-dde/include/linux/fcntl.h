/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_FCNTL_H
#define _LINUX_FCNTL_H

#include <fcntl.h>

#ifndef O_CLOEXEC
#define O_CLOEXEC 02000000
#endif

#endif
