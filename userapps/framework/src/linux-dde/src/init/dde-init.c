#include <rtthread.h>
#include <stdio.h>
#include <drv_syscall.h>

/*
 * Components Initialization will initialize some driver and components as following
 * order:
 * rti_start         --> 0
 * BOARD_EXPORT      --> 1
 * rti_board_end     --> 1.end
 *
 * DEVICE_EXPORT     --> 2
 * COMPONENT_EXPORT  --> 3
 * FS_EXPORT         --> 4
 * ENV_EXPORT        --> 5
 * APP_EXPORT        --> 6
 *
 * rti_end           --> 6.end
 *
 * These automatically initialization, the driver or component initial function must
 * be defined with:
 * INIT_BOARD_EXPORT(fn);
 * INIT_DEVICE_EXPORT(fn);
 * ...
 * INIT_APP_EXPORT(fn);
 * etc.
 */
static int rti_start(void)
{
    return 0;
}
INIT_EXPORT(rti_start, "0");

static int rti_board_start(void)
{
    return 0;
}
INIT_EXPORT(rti_board_start, "0.end");

static int rti_board_end(void)
{
    return 0;
}
INIT_EXPORT(rti_board_end, "1.end");

static int rti_end(void)
{
    return 0;
}
INIT_EXPORT(rti_end, "6.end");

/**
 * RT-Thread Components Initialization for board
 */
void rt_components_board_init(void)
{
    int result;
    const struct rt_init_desc *desc;

    rt_kprintf("do board components initialization.\n");

    for (desc = &__rt_init_desc_rti_board_start; desc < &__rt_init_desc_rti_board_end; desc ++)
    {
        rt_kprintf("initialize %s\n", desc->fn_name);
        result = desc->fn();
        rt_kprintf(":%d done\n", result);
    }

    rt_kprintf("do board components initialization finished.\n");
}

/**
 * RT-Thread Components Initialization
 */
void rt_components_init(void)
{
    int result;
    const struct rt_init_desc *desc;

    rt_kprintf("do components initialization.\n");
    
    for (desc = &__rt_init_desc_rti_board_end; desc < &__rt_init_desc_rti_end; desc ++)
    {
        rt_kprintf("initialize %s\n", desc->fn_name);
        result = desc->fn();
        rt_kprintf(":%d done\n", result);
    }

    rt_kprintf("do components initialization finished.\n");
}

RT_WEAK void drv_main()
{

}

int main()
{
    rt_components_board_init();
    rt_components_init();
    drv_main();

    while(1)
    {
        rt_thread_mdelay(2000);
    }
    
    return 0;
}