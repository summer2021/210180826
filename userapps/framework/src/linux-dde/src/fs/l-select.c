#include <linux/file.h>
#include <linux/poll.h>
#include <linux/fs.h>

void poll_wait(struct file * filp, wait_queue_head_t * wait_address, poll_table *p)
{
    if (p && p->_qproc && wait_address)
        p->_qproc(&wait_address->head, p);
}
