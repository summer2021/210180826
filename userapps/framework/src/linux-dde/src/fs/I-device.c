#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/poll.h>

struct device *device_create(struct class *class, struct device *parent,
                             dev_t devt, void *drvdata, const char *fmt, ...)
{
    //因为目前不需要，暂时不实现
    return (struct device *)-MAX_ERRNO - 1;
}

void device_destroy(struct class *class, dev_t devt)
{
    //因为目前不需要，暂时不实现
}