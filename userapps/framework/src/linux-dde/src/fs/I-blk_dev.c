#include <linux/genhd.h>
#include <linux/blk_types.h>
#include <linux/blkdev.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/poll.h>

#include <rtthread.h>

#define DEVID_LIST_NUM 20
static unsigned int devid_list[DEVID_LIST_NUM];

#define MAJOR_LIST_NUM 256
static const char *major_list[MAJOR_LIST_NUM];

static const char *find_device_name_by_id(dev_t devid)
{
    return major_list[MAJOR(devid)];
}

static unsigned int get_new_major()
{
    unsigned int i;

    for(i = 1;i < MAJOR_LIST_NUM;i++)
    {
        if(major_list[i] == NULL)
        {
            return i;
        }
    }

    return 0;
}

static rt_err_t linux_device_init(rt_device_t dev)
{
    return RT_EOK;
}

static rt_err_t linux_device_open(rt_device_t dev, rt_uint16_t oflag)
{
    struct gendisk *disk = (struct gendisk *)dev->user_data;
    rt_err_t err = RT_EOK;

    if(disk->fops->open != NULL)
    {
        err = disk->fops->open(disk->part0, 0);
    }

    return err;
}

static rt_err_t linux_device_close(rt_device_t dev)
{
    struct gendisk *disk = (struct gendisk *)dev->user_data;

    if(disk->fops->release != NULL)
    {
        disk->fops->release(disk, 0);
    }

    return RT_EOK;
}

static rt_size_t linux_device_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
    struct linux_dev *device = (struct linux_dev *)dev;
    void* client = rt_device_client_get();
    struct rt_device_reply r;
    struct bio t_bio;
    struct bio_vec vec[2] = {0};

    rt_device_reply_get(&r);

    if (size > 256)
    {
        size = 256;
    }

    struct gendisk *disk = (struct gendisk *)dev->user_data;
    disk->queue->req.__sector = pos >> 9;
    disk->queue->req.bio = &t_bio;
    t_bio.bi_iter.bi_size = size;
    t_bio.bi_opf = REQ_OP_READ;
    t_bio.bi_iter.bi_sector = pos >> 9;
    t_bio.bi_iter.bi_bvec_done = 0;
    t_bio.bi_iter.bi_idx = 0;
    t_bio.bi_io_vec = vec;
    vec[0].bv_page = (void *)device->buff;
    vec[0].bv_offset = 0;
    vec[0].bv_len = size;
    disk->queue->req.cmd_flags = READ;
    
    if(disk->queue->mfn != NULL)
    {
        disk->queue->mfn(disk->queue,&t_bio);
    }
    else
    {
        disk->queue->rfn(disk->queue);
    }

    size = rt_data_put(device->udev, client, buffer, device->buff, size);
    rt_device_reply_ack(&r, size);
    return 0;
}

static rt_size_t linux_device_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    struct linux_dev *device = (struct linux_dev *)dev;
    void* client = rt_device_client_get();
    struct bio t_bio;
    struct bio_vec vec[2] = {0};

    if (size > 256)
    {
        size = 256;
    }
    
    size = rt_data_get(device->udev, client, device->buff, buffer, size);
    struct gendisk *disk = (struct gendisk *)dev->user_data;
    disk->queue->req.__sector = pos >> 9;
    disk->queue->req.bio = &t_bio;
    t_bio.bi_iter.bi_size = size;
    t_bio.bi_opf = REQ_OP_WRITE;
    t_bio.bi_iter.bi_sector = pos >> 9;
    t_bio.bi_iter.bi_bvec_done = 0;
    t_bio.bi_iter.bi_idx = 0;
    t_bio.bi_io_vec = vec;
    vec[0].bv_page = (void *)device->buff;
    vec[0].bv_offset = 0;
    vec[0].bv_len = size;
    disk->queue->req.cmd_flags = WRITE;

    if(disk->queue->mfn != NULL)
    {
        disk->queue->mfn(disk->queue,&t_bio);
    }
    else
    {
        disk->queue->rfn(disk->queue);
    }

    return size;
}

static rt_err_t linux_device_control(rt_device_t dev, int cmd, void *args)
{
    struct linux_dev *device = (struct linux_dev *)dev;
    struct gendisk *disk = (struct gendisk *)dev->user_data;

    if (cmd == RT_DEVICE_CTRL_QUIT)
    {
        disk->fops->release(disk, 0);
    }
    else
    {
        device->data = (int)(size_t)args;

        if(disk->fops->ioctl != NULL)
        {
            disk->fops->ioctl(disk->part0,0,cmd, (unsigned long)args);
        }
    }
    return RT_EOK;
}

int register_blkdev(unsigned int major,const char *name)
{
    if(major == 0)
    {
        major = get_new_major();

        if(major > 0)
        {
            major_list[major] = name;
        }

        return major;
    }

    if(major >= MAJOR_LIST_NUM)
    {
        return -1;
    }

    if(major_list[major] != NULL)
    {
        return -1;
    }

    major_list[major] = name;
    return major;
}

void unregister_blkdev(unsigned int major,const char *name)
{
    if(major < MAJOR_LIST_NUM)
    {
        major_list[major] = NULL;
    }

    rt_device_t device = rt_udevice_find(name);

    if(device != RT_NULL)
    {
        rt_udevice_unregister(device);
    }
}

struct gendisk *alloc_disk(int minors)
{
    struct gendisk *ret = kzalloc(sizeof(struct gendisk),GFP_KERNEL);

    if(ret != NULL)
    {
        ret->first_minor = 0;
        ret->minors = minors;
    }

    return ret;
}

void del_gendisk(struct gendisk *gp)
{
    free(gp);
}

void add_disk(struct gendisk *disk)
{
    rt_device_t device,udev;
    rt_err_t ret;
    device = &disk->rt_device.parent;
    device->user_data = (void *)disk;
#ifdef RT_USING_DEVICE_OPS
    device->ops = &disk->rt_device_ops;
    disk->rt_device_ops.init = linux_device_init;
    disk->rt_device_ops.open = linux_device_open;
    disk->rt_device_ops.close = linux_device_close;
    disk->rt_device_ops.read = linux_device_read;
    disk->rt_device_ops.write = linux_device_write;
    disk->rt_device_ops.control = linux_device_control;
#else
    device->init = linux_device_init;
    device->open = linux_device_open;
    device->close = linux_device_close;
    device->read = linux_device_read;
    device->write = linux_device_write;
    device->control = linux_device_control;
#endif

    const char *name = major_list[disk->major];
    ret = rt_udevice_register(device, name, RT_DEVICE_FLAG_RDWR);

    if(!ret)
    {
        udev = rt_udevice_find(name);
        disk->rt_device.udev = udev;
        rt_udevice_set_current(udev);
    }
    else
    {
        printk("rt_udevice_register failed!ret = %d\r\n",ret);
    }
}

void set_capacity(struct gendisk *disk,sector_t size)
{

}

struct kobject *get_disk(struct gendisk *disk)
{
    return NULL;
}

void put_disk(struct gendisk *disk)
{

}

struct request_queue *blk_alloc_queue(gfp_t gfp_mask)
{
    return kzalloc(sizeof(struct request_queue),GFP_KERNEL);
}

struct request_queue *blk_init_queue(request_fn_proc *rfn,spinlock_t *lock)
{
    struct request_queue *ret = blk_alloc_queue(GFP_KERNEL);

    if(ret != NULL)
    {
        ret->rfn = rfn;
    }

    return ret;
}

void blk_cleanup_queue(struct request_queue *q)
{
    free(q);
}

void blk_queue_make_request(struct request_queue *q,mask_request_fn *mfn)
{
    q->mfn = mfn;
}

struct request *blk_peek_request(struct request_queue *q)
{
    return &q->req;
}

void blk_start_request(struct request *req)
{
    
}

struct request *blk_fetch_request(struct request_queue *q)
{
    struct request *rq;

    rq = blk_peek_request(q);

    if(rq)
    {
        blk_start_request(rq);
    }

    return rq;
}

bool __blk_end_request_cur(struct request *rq,int error)
{
    return true;
}

void bio_endio(struct bio *bio,...)
{

}