#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/poll.h>

#include <rtthread.h>

#include <dfs_fs.h>
#include <dfs_file.h>
#include <dfs_poll.h>

struct cdevfile
{
    struct file f;
    struct inode in;
    loff_t pos;
};

#define DEVID_LIST_NUM 20
static const char *devid_list[DEVID_LIST_NUM];

static const char *find_device_name_by_id(dev_t devid)
{
    return devid_list[MINOR(devid)];
}

static short pollmask_to_linux(short key)
{
    short ret = 0;

    if (key & POLLIN)
        ret |= EPOLLIN;
    if (key & POLLOUT)
        ret |= EPOLLIN;
    if (key & POLLERR)
        ret |= EPOLLERR;
    if (key & POLLHUP)
        ret |= EPOLLHUP;

    return ret;
}

static int pollmask_from_linux(int key)
{
    int ret = 0;

    if (key & EPOLLIN)
        ret |= POLLIN;
    if (key & EPOLLOUT)
        ret |= POLLOUT;
    if (key & EPOLLERR)
        ret |= POLLERR;
    if (key & EPOLLHUP)
        ret |= POLLHUP;

    return ret;
}

static int _cdev_open(struct dfs_fd *fle)
{
    struct cdev *dev = (struct cdev *)fle->data;
    int ret;
    struct cdevfile *cf;

    cf = rt_calloc(1, sizeof(*cf));
    if (!cf)
        return -ENOMEM;

    cf->f.f_op = dev->ops;
    cf->in.i_cdev = dev;
    cf->in.i_rdev = dev->id;
    cf->f.f_inode = &cf->in;

    ret = dev->ops->open(&cf->in, &cf->f);
    if (ret == 0)
        fle->data = cf;
    else
        rt_free(cf);

    return ret;
}

static int _cdev_ioctl(struct dfs_fd *fd, int cmd, void *args)
{
    struct cdevfile *cf = fd->data;
    int ret;

    ret = (int)cf->f.f_op->unlocked_ioctl(&cf->f, cmd, (unsigned long)args);

    return ret;
}

static int _cdev_close(struct dfs_fd *fd)
{
    struct cdevfile *cf = fd->data;
    int ret;

    fd->data = NULL;
    ret = cf->f.f_op->release(&cf->in, &cf->f);
    //TODO free cf may not safe
    rt_free(cf);

    return ret;
}

static int _cdev_read(struct dfs_fd *fd, void *buf, size_t count)
{
    struct cdevfile *cf = fd->data;
    int ret;

    ret = cf->f.f_op->read(&cf->f, buf, count, &cf->pos);

    return ret;
}
/*
static int _cdev_poll(struct dfs_fd *fd, struct rt_pollreq *req)
{
    struct cdevfile *cf = fd->data;
    int ret;

    req->_key = pollmask_to_linux(req->_key);

    ret = cf->f.f_op->poll(&cf->f, (poll_table*)req);
    ret = pollmask_from_linux(ret);

    return ret;
}*/

static const struct dfs_file_ops _cdev_fops = {
    .open = _cdev_open,
    .ioctl = _cdev_ioctl,
    .close = _cdev_close,
    .read = _cdev_read,
    //.poll = _cdev_poll,
};

static void prepare_filp(struct file *filp, rt_device_t dev)
{
    struct linux_dev *ldev = (struct linux_dev *)dev;
    filp->private_data = ldev->private_data;
}

static void deal_filp(struct file *filp, rt_device_t dev)
{
    struct linux_dev *ldev = (struct linux_dev *)dev;
    ldev->private_data = filp->private_data;
}

static rt_err_t linux_device_init(rt_device_t dev)
{
    return RT_EOK;
}

static rt_err_t linux_device_open(rt_device_t dev, rt_uint16_t oflag)
{
    struct file_operations *fops = (struct file_operations *)dev->user_data;

    if(!fops->rt_device_inited)
    {
        return -RT_EBUSY;
    }

    struct inode inode;
    struct file filp;
    
    rt_err_t err = RT_EOK;

    if(fops->open != NULL)
    {
        prepare_filp(&filp, dev);
        err = fops->open(&inode, &filp);
        deal_filp(&filp, dev);
    }
    
    return err;
}

static rt_err_t linux_device_close(rt_device_t dev)
{
    struct file_operations *fops = (struct file_operations *)dev->user_data;
    struct inode inode;
    struct file filp;
    rt_err_t ret = RT_EOK;

    if(fops->release != NULL)
    {
        prepare_filp(&filp, dev);
        ret = fops->release(&inode, &filp);
        deal_filp(&filp, dev);
    }

    return ret;
}

static rt_size_t linux_device_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
    struct linux_dev *device = (struct linux_dev *)dev;
    void* client = rt_device_client_get();
    struct rt_device_reply r;

    rt_device_reply_get(&r);

    if (size > 256)
    {
        size = 256;
    }

    struct file_operations *fops = (struct file_operations *)dev->user_data;
    struct file filp;

    if(fops->read != NULL)
    {
        prepare_filp(&filp, dev);
        deal_filp(&filp, dev);
        size = fops->read(&filp, device->buff, size, &pos);
    }

    size = rt_data_put(device->udev, client, buffer, device->buff, size);
    rt_device_reply_ack(&r, size);
    return 0;
}

static rt_size_t linux_device_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    struct linux_dev *device = (struct linux_dev *)dev;
    struct file_operations *fops = (struct file_operations *)dev->user_data;
    void* client = rt_device_client_get();

    if (size > 256)
    {
        size = 256;
    }
    
    size = rt_data_get(device->udev, client, device->buff, buffer, size);
    struct file filp;

    if(fops->write != NULL)
    {
        prepare_filp(&filp, dev);
        size = fops->write(&filp, device->buff, size, &pos);
        deal_filp(&filp, dev);
    }
    
    return size;
}

static rt_err_t linux_device_control(rt_device_t dev, int cmd, void *args)
{
    struct linux_dev *device = (struct linux_dev *)dev;
    struct file_operations *fops = (struct file_operations *)dev->user_data;

    if (cmd == RT_DEVICE_CTRL_QUIT)
    {
        if(fops->release != NULL)
        {
            fops->release(RT_NULL, RT_NULL);
        }
    }
    else
    {
        device->data = (int)(size_t)args;
        struct file filp;
        prepare_filp(&filp, dev);
        fops->unlocked_ioctl(&filp, cmd, (unsigned long)args);
        deal_filp(&filp, dev);
    }
    return RT_EOK;
}

int cdev_device_add(struct cdev *cdev, struct device *dev)
{
    //TODO
    return 0;
}

void cdev_device_del(struct cdev *cdev, struct device *dev)
{
}

void cdev_init(struct cdev *cdev, const struct file_operations *fops)
{
    cdev->ops = fops;
}

void cdev_del(struct cdev *cdev)
{
    rt_device_t device = rt_udevice_find(find_device_name_by_id(cdev->id));

    if(device == RT_NULL)
    {
        return;
    }

    rt_udevice_unregister(device);
}

struct cdev *cdev_alloc(void)
{
    struct cdev *p = kzalloc(sizeof(struct cdev), GFP_KERNEL);

    return p;
}

int cdev_add(struct cdev *p, dev_t dev, unsigned count)
{
    rt_device_t device,udev;
    rt_err_t ret;
    p->id = dev;
    struct file_operations *fops = (struct file_operations *)p->ops;
    device = &fops->rt_device.parent;
    device->user_data = (void *)fops;
#ifdef RT_USING_DEVICE_OPS
    device->ops = &fops->rt_device_ops;
    fops->rt_device_ops.init = linux_device_init;
    fops->rt_device_ops.open = linux_device_open;
    fops->rt_device_ops.close = linux_device_close;
    fops->rt_device_ops.read = linux_device_read;
    fops->rt_device_ops.write = linux_device_write;
    fops->rt_device_ops.control = linux_device_control;
#else
    device->init = linux_device_init;
    device->open = linux_device_open;
    device->close = linux_device_close;
    device->read = linux_device_read;
    device->write = linux_device_write;
    device->control = linux_device_control;
#endif

    fops->rt_device_inited = RT_FALSE;
    const char *name = find_device_name_by_id(dev);
    ret = rt_udevice_register(device, name, RT_DEVICE_FLAG_RDWR);

    if(!ret)
    {
        udev = rt_udevice_find(name);
        fops->rt_device.udev = udev;
        rt_udevice_set_current(udev);
        ret = fops->rt_init();
        fops->rt_device_inited = RT_TRUE;
    }

    return ret;
}

int register_chrdev_region(dev_t from, unsigned count, const char *name)
{
    dev_t i;
    dev_t start_pos = MINOR(from);
    int found_cnt = 0;

    for(i = start_pos;i < DEVID_LIST_NUM;i++)
    {
        if(!devid_list[i])
        {
            found_cnt++;

            if(found_cnt == count)
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    if(found_cnt < count)
    {
        return -EFAULT;
    }

    for(i = start_pos;i < (start_pos + count);i++)
    {
        devid_list[i] = name;
    }

    return 0;
}

void unregister_chrdev_region(dev_t from, unsigned count)
{
    dev_t i;

    for(i = 0;i < count;i++)
    {
        devid_list[from + i] = NULL;
    }
}

int alloc_chrdev_region(dev_t *dev, unsigned baseminor, unsigned count,
                        const char *name)
{
    dev_t i;
    dev_t start_pos = MINOR(baseminor);
    int found_cnt = 0;

    for(i = start_pos;i < DEVID_LIST_NUM;i++)
    {
        if(!devid_list[i])
        {
            found_cnt++;

            if(found_cnt == count)
            {
                break;
            }
        }
        else
        {
            start_pos = i + 1;
            found_cnt = 0;
        }
    }

    if(found_cnt < count)
    {
        return -EFAULT;
    }

    for(i = start_pos;i < (start_pos + count);i++)
    {
        devid_list[i] = name;
    }

    *dev = MKDEV(1,start_pos);
    return 0;
}

int register_chrdev(unsigned int major, const  char *name, struct file_operations *fops)
{
    rt_device_t device,udev;
    rt_err_t ret;
    device = &fops->rt_device.parent;
    device->user_data = (void *)fops;
#ifdef RT_USING_DEVICE_OPS
    device->ops = &fops->rt_device_ops;
    fops->rt_device_ops.init = linux_device_init;
    fops->rt_device_ops.open = linux_device_open;
    fops->rt_device_ops.close = linux_device_close;
    fops->rt_device_ops.read = linux_device_read;
    fops->rt_device_ops.write = linux_device_write;
    fops->rt_device_ops.control = linux_device_control;
#else
    device->init = linux_device_init;
    device->open = linux_device_open;
    device->close = linux_device_close;
    device->read = linux_device_read;
    device->write = linux_device_write;
    device->control = linux_device_control;
#endif

    fops->rt_device_inited = RT_FALSE;
    ret = rt_udevice_register(device, name, RT_DEVICE_FLAG_RDWR);

    if(!ret)
    {
        udev = rt_udevice_find(name);
        fops->rt_device.udev = udev;
        rt_udevice_set_current(udev);
        fops->rt_init();
        fops->rt_device_inited = RT_TRUE;
    }

    return ret;
}

int unregister_chrdev(unsigned int major, const char *name)
{
    rt_device_t device = rt_udevice_find(name);

    if(device == RT_NULL)
    {
        return -EIO;
    }

    rt_udevice_unregister(device);
    return 0;
}