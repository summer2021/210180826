#include <linux/mutex.h>

#include <rtthread.h>

void __mutex_init(struct mutex *lock, const char *name)
{
    rt_mutex_init(&lock->m, name, 0);
}

void mutex_lock(struct mutex *lock)
{
    rt_mutex_take(&lock->m, RT_WAITING_FOREVER);
}

void mutex_unlock(struct mutex *lock)
{
    rt_mutex_release(&lock->m);
}

void mutex_destroy(struct mutex *lock)
{
    rt_mutex_detach(&lock->m);
}

int mutex_lock_interruptible(struct mutex *lock)
{
    return rt_mutex_take(&lock->m, RT_WAITING_FOREVER);
}
