#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/gfp.h>
#include <linux/printk.h>

#include <rtthread.h>

static void *_alloc(size_t n, size_t size, gfp_t flags)
{
    void *p;

    if (flags & __GFP_ZERO)
        p = rt_calloc(n, size);
    else
        p = rt_malloc(n * size);

    return p;
}

void kfree(const void *p)
{
    rt_free((void*)p);
}

void *kzalloc(size_t size, gfp_t flags)
{
    return _alloc(1, size, flags | __GFP_ZERO);
}

void *kmalloc(size_t size, gfp_t flags)
{
    return _alloc(1, size, flags);
}

void *kcalloc(size_t n, size_t size, gfp_t flags)
{
    return _alloc(n, size, flags | __GFP_ZERO);
}

void *kvmalloc_array(size_t n, size_t size, gfp_t flags)
{
    return _alloc(n, size, flags);
}

void kvfree(const void *addr)
{
    rt_free((void*)addr);
}

void *kvmalloc(size_t size, gfp_t flags)
{
    return _alloc(1, size, flags);
}

void *kvzalloc(size_t size, gfp_t flags)
{
    return _alloc(1, size, flags | __GFP_ZERO);
}

void *kmalloc_node(size_t size, gfp_t flags, int node)
{
    return _alloc(1, size, flags | __GFP_ZERO);
}


void frame_vector_to_pfns(struct frame_vector *vec)
{
    printk("TODO: %s\n", __func__);
}

int frame_vector_to_pages(struct frame_vector *vec)
{
    printk("TODO: %s\n", __func__);
    return 0;
}

int set_page_dirty_lock(struct page *page)
{
    printk("TODO: %s\n", __func__);
    return 0;
}

struct frame_vector *frame_vector_create(unsigned int nr_frames)
{
    printk("TODO: %s\n", __func__);
    return NULL;
}

void frame_vector_destroy(struct frame_vector *vec)
{
    printk("TODO: %s\n", __func__);
}

int get_vaddr_frames(unsigned long start, unsigned int nr_pfns,
             unsigned int gup_flags, struct frame_vector *vec)
{
    printk("TODO: %s\n", __func__);
    return 0;//
}

void put_vaddr_frames(struct frame_vector *vec)
{
    printk("TODO: %s\n", __func__);
}

#ifdef CONFIG_MMU
bool is_vmalloc_addr(const void *x)
{
    printk("TODO: %s\n", __func__);
    return false;
}
#else
bool is_vmalloc_addr(const void *x)
{
    return false;
}
#endif

void *page_address(const struct page *page)
{
    /*printk("TODO: %s\n", __func__);
    return NULL;*/
    return (void *)page;
}