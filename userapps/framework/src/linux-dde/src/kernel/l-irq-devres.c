#include <linux/interrupt.h>

RT_WEAK int irq_hw_register(unsigned int irq, irq_handler_t handler, void *id)
{
    return -1;
}

int devm_request_irq(struct device *dev, unsigned int irq, irq_handler_t handler,
		 unsigned long irqflags, const char *devname, void *dev_id)
{
    return irq_hw_register(irq, handler, dev_id);
}

void synchronize_irq(unsigned int irq)
{
    //TODO
}

const void *free_irq(unsigned int irq, void *d)
{
    return 0;
}

int request_irq(unsigned int irq, irq_handler_t handler, unsigned long flags,
	    const char *name, void *dev)
{
    return irq_hw_register(irq, handler, dev);
}
