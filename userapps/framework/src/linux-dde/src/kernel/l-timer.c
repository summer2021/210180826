#include <linux/delay.h>
#include <rtthread.h>

void msleep(unsigned int msecs)
{
    rt_thread_mdelay(msecs);
}

void usleep_range(unsigned long min, unsigned long max)
{
    rt_int32_t msecs;

    msecs = (min + max + 2000)/2000;

    rt_thread_mdelay(msecs);
}

void mdelay(unsigned long x)
{
    rt_thread_mdelay(x);
}

void ndelay(unsigned long x)
{

}

void udelay(unsigned long x)
{
    x *= 1000000;
    while (x-- > 0);
}
