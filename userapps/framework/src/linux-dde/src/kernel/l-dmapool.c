#include <linux/export.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/dmapool.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/spinlock.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/dma-mapping.h>
#include <rtthread.h>

struct dma_pool {		/* the pool */
    struct list_head page_list;
    spinlock_t lock;
    size_t size;
    struct device *dev;
    size_t allocation;
    size_t boundary;
    size_t align;
    char name[28];
    struct list_head pools;
};

struct dma_page {		/* cacheable header for 'allocation' bytes */
    struct list_head page_list;
    void *vaddr;
    dma_addr_t dma;
    unsigned int in_use;
    unsigned int offset;
};

static DEFINE_MUTEX(pools_lock);
static DEFINE_MUTEX(pools_reg_lock);

static void pool_initialise_page(struct dma_pool *pool, struct dma_page *page)
{
    unsigned int offset = 0;
    unsigned int next_boundary = pool->boundary;

    do {
        unsigned int next = offset + pool->size;
        if (unlikely((next + pool->size) >= next_boundary)) {
            next = next_boundary;
            next_boundary += pool->boundary;
        }
        *(int *)(page->vaddr + offset) = next;
        offset = next;
    } while (offset < pool->allocation);
}

static struct dma_page *pool_alloc_page(struct dma_pool *pool, gfp_t mem_flags)
{
    struct dma_page *page;

    page = kmalloc(sizeof(*page), mem_flags);
    if (!page)
        return NULL;
    page->vaddr = rt_mmap(rt_udevice_get_current(), (void **)&page->dma, pool->allocation, 0);
    printf("page->vaddr = %p,page->dma = %p,pool->allocation = %d,pool->align = %d\n",page->vaddr,(void *)page->dma,pool->allocation,pool->align);
    //page->vaddr = rt_malloc_align(pool->allocation, pool->align);
    //page->dma = (dma_addr_t)page->vaddr;
    rt_memset(page->vaddr, 0, pool->allocation);

    if (page->vaddr) {
        pool_initialise_page(pool, page);
        page->in_use = 0;
        page->offset = 0;
    } else {
        kfree(page);
        page = NULL;
    }
    return page;
}

static struct dma_page *pool_find_page(struct dma_pool *pool, dma_addr_t dma)
{
    struct dma_page *page;

    list_for_each_entry(page, &pool->page_list, page_list) {
        if (dma < page->dma)
            continue;
        if ((dma - page->dma) < pool->allocation)
            return page;
    }
    return NULL;
}

void *dma_pool_alloc(struct dma_pool *pool, gfp_t mem_flags,
                   dma_addr_t *handle)
{
    unsigned long flags;
    struct dma_page *page;
    size_t offset;
    void *retval;

    spin_lock_irqsave(&pool->lock, flags);
    list_for_each_entry(page, &pool->page_list, page_list) {
        if (page->offset < pool->allocation)
            goto ready;
    }

    /* pool_alloc_page() might sleep, so temporarily drop &pool->lock */
    spin_unlock_irqrestore(&pool->lock, flags);

    page = pool_alloc_page(pool, mem_flags & (~__GFP_ZERO));
    if (!page)
        return NULL;

    spin_lock_irqsave(&pool->lock, flags);

    list_add(&page->page_list, &pool->page_list);
 ready:
    page->in_use++;
    offset = page->offset;
    page->offset = *(int *)(page->vaddr + offset);
    retval = offset + page->vaddr;
    *handle = offset + page->dma;

    spin_unlock_irqrestore(&pool->lock, flags);

    return retval;
}

struct dma_pool *dma_pool_create(const char *name, struct device *dev,
                 size_t size, size_t align, size_t boundary)
{
    struct dma_pool *retval;
    size_t allocation;
    bool empty = false;

    if (align == 0)
        align = 1;
    else if (align & (align - 1))
        return NULL;

    if (size == 0)
        return NULL;
    else if (size < 4)
        size = 4;

    size = __ALIGN_KERNEL(size, align);
    allocation = max_t(size_t, size, PAGE_SIZE);

    if (!boundary)
        boundary = allocation;
    else if ((boundary < size) || (boundary & (boundary - 1)))
        return NULL;

    retval = kmalloc_node(sizeof(*retval), GFP_KERNEL, 0);
    if (!retval)
        return retval;

    strlcpy(retval->name, name, sizeof(retval->name));

    retval->dev = dev;

    INIT_LIST_HEAD(&retval->page_list);
    spin_lock_init(&retval->lock);
    retval->size = size;
    retval->boundary = boundary;
    retval->allocation = allocation;
    retval->align = align;

    INIT_LIST_HEAD(&retval->pools);

    /*
     * pools_lock ensures that the ->dma_pools list does not get corrupted.
     * pools_reg_lock ensures that there is not a race between
     * dma_pool_create() and dma_pool_destroy() or within dma_pool_create()
     * when the first invocation of dma_pool_create() failed on
     * device_create_file() and the second assumes that it has been done (I
     * know it is a short window).
     */
    mutex_lock(&pools_reg_lock);
    mutex_lock(&pools_lock);
    if (list_empty(&dev->dma_pools))
        empty = true;
    list_add(&retval->pools, &dev->dma_pools);
    mutex_unlock(&pools_lock);
    mutex_unlock(&pools_reg_lock);

    return retval;
}

void dma_pool_destroy(struct dma_pool *pool)
{

}

void dma_pool_free(struct dma_pool *pool, void *vaddr,
                 dma_addr_t dma)
{
    struct dma_page *page;
    unsigned long flags;
    unsigned int offset;

    spin_lock_irqsave(&pool->lock, flags);
    page = pool_find_page(pool, dma);
    if (!page) {
        spin_unlock_irqrestore(&pool->lock, flags);
        return;
    }

    offset = vaddr - page->vaddr;

    page->in_use--;
    *(int *)vaddr = page->offset;
    page->offset = offset;
    /*
     * Resist a temptation to do
     *    if (!is_page_busy(page)) pool_free_page(pool, page);
     * Better have a few empty pages hang around.
     */
    spin_unlock_irqrestore(&pool->lock, flags);
}
