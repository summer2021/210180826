#include <linux/workqueue.h>

static void work_func(struct rt_work *work, void *work_data)
{
    struct work_struct *w = (struct work_struct *) work_data;

    w->func(w);
}

bool flush_work(struct work_struct *work)
{
    //TODO
    return true;
}

bool queue_work(struct workqueue_struct *wq,
			      struct work_struct *work)
{
    return true;
}

int _delayed_work_init(struct delayed_work *dw, work_func_t f, int op)
{
    if (op == 1)
    {
        dw->wq = (void *)rt_workqueue_create("delay", 4096, 20);
    }

    if (!dw->wq)
        return -1;

    dw->work.func = f;
    rt_work_init((void *)&dw->work, work_func, &dw->work);

    return 0;
}

bool schedule_delayed_work(struct delayed_work *dw, unsigned long delay)
{
    return (rt_workqueue_submit_work((void *)dw->wq, (void *)&dw->work, delay) == 0);
}
