#include <linux/uaccess.h>
#include <string.h>

unsigned long copy_to_user(void __user *to, const void *from, unsigned long n)
{
    memcpy(to, from, n);
    return 0;
}

unsigned long copy_from_user(void *to, const void __user *from, unsigned long n)
{
    memcpy(to, from, n);
    return 0;
}
