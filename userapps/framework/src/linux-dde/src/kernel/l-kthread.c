#include <linux/kthread.h>

bool kthread_should_stop(void)
{
    return true;
}

struct task_struct * kthread_run(int (*threadfn)(void *data), void *data, const char *namefmt, ...)
{
    return NULL;
}

int kthread_stop(struct task_struct *k)
{
    return 0;
}
