#include <linux/spinlock.h>

#include <rthw.h>
//TODO
void spin_lock_init(spinlock_t *lock)
{

}

void spin_lock(spinlock_t *lock)
{

}

void spin_unlock(spinlock_t *lock)
{

}

rt_base_t rt_hw_interrupt_disable()
{
    return 0;
}

void rt_hw_interrupt_enable(rt_base_t level)
{
    
}

void _spin_lock_irqsave(spinlock_t *lock, unsigned long *flags)
{
    *flags = rt_hw_interrupt_disable();
}

void spin_unlock_irqrestore(spinlock_t *lock, unsigned long flags)
{
    rt_hw_interrupt_enable(flags);
}

void assert_spin_locked(spinlock_t *lock)
{

}

void spin_lock_irq(spinlock_t *lock)
{
    rt_base_t i = rt_hw_interrupt_disable();
    //TODO
    lock->iflags = i;
}

void spin_unlock_irq(spinlock_t *lock)
{
    rt_hw_interrupt_enable(lock->iflags);
}
