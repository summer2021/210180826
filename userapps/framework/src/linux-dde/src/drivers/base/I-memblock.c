#include <linux/types.h>
#include <rtthread.h>

void *memblock_alloc(phys_addr_t size,phys_addr_t align)
{
    return rt_malloc_align(size,align);
}