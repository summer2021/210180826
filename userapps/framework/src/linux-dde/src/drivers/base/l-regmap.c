#include <linux/regmap.h>

int regmap_write(struct regmap *map, unsigned int reg,
				 unsigned int val)
{
	return map->write(map, reg, (unsigned int)&val);
}

int regmap_read(struct regmap *map, unsigned int reg,
				unsigned int *val)
{
	return map->read(map, reg, val);
}

int regmap_update_bits(struct regmap *map, unsigned int reg,
					   unsigned int mask, unsigned int val)
{
	int ret;
	unsigned int tmp, orig;

	ret = regmap_read(map, reg, &orig);
	if (ret != 0)
		return ret;

	tmp = orig & ~mask;
	tmp |= (val & mask);

	ret = regmap_write(map, reg, tmp);

	return ret;
}
