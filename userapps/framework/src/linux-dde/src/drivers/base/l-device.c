#include <linux/mm.h>

#include <linux/device.h>
#include <rtthread.h>

void *devm_kcalloc(struct device *dev,
				 size_t n, size_t size, gfp_t flags)
{
    void *p;

    p = kvmalloc_array(n, size, flags);

    return p;
}

const char *dev_name(const struct device *dev)
{
    return ""; //TODO
}

void *devm_kzalloc(struct device *dev, size_t size, gfp_t gfp)
{
    return kvzalloc(size, gfp);
}

void devm_kfree(struct device *dev, const void *p)
{
    kvfree(p);
}

void __iomem *devm_ioremap_resource(struct device *dev,
				    const struct resource *res)
{
    return (void*)res->start;
}

int device_add(struct device *dev)
{
    return 0;
}

void device_del(struct device *dev)
{

}
