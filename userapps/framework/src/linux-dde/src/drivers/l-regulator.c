#include <linux/of.h>
#include <linux/regulator/consumer.h>
#include <stdio.h>
#include <string.h>

struct regulator *devm_regulator_get_optional(struct device *dev, const char *id)
{
    struct device_node *dn;
    char name[12];
    int i;
    struct regulator *ret = NULL;

    for (i = 0; i < 8; i ++)
    {
        struct regulator *r;

        sprintf(name, "supply-%d", i);
        dn = device_node_find(dev, name);
        if (dn)
        {
            r = (struct regulator *)dn->value;
            if (strcmp(r->supply_name, id) == 0)
            {
                ret = r;
                break;
            }
        }
    }

    return ret;
}
