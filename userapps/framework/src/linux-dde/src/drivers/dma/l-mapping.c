#include <linux/dma-mapping.h>
#include <linux/export.h>
#include <linux/gfp.h>
#include <rtthread.h>
#include <drv_syscall.h>

void *dma_alloc_attrs(struct device *dev, size_t size, dma_addr_t *dma_handle,
		gfp_t flag, unsigned long attrs)
{
	void *p;

	//p = rt_malloc_align(size, 2048);
	p = rt_mmap(rt_udevice_get_current(), (void **)dma_handle, size, 0);
	rt_memset(p, 0, size);
    //*dma_handle = (dma_addr_t)p;

    return p;
}

void dma_free_attrs(struct device *dev, size_t size, void *cpu_addr,
		dma_addr_t dma_handle, unsigned long attrs)
{
	//kvfree(cpu_addr);
	rt_munmap(rt_udevice_get_current(), cpu_addr, size);
}

int dma_get_sgtable_attrs(struct device *dev, struct sg_table *sgt,
		void *cpu_addr, dma_addr_t dma_addr, size_t size,
		unsigned long attrs)
{
	rt_kprintf("TODO: %s", __func__);
    return 0;
}

dma_addr_t dma_map_resource(struct device *dev, phys_addr_t phys_addr,
		size_t size, enum dma_data_direction dir, unsigned long attrs)
{
	rt_kprintf("TODO: %s", __func__);
    return 0;
}

int dma_map_sg_attrs(struct device *dev, struct scatterlist *sg, int nents,
		enum dma_data_direction dir, unsigned long attrs)
{
	rt_kprintf("TODO: %s", __func__);
    return 0;
}

int dma_mmap_attrs(struct device *dev, struct vm_area_struct *vma,
		void *cpu_addr, dma_addr_t dma_addr, size_t size,
		unsigned long attrs)
{
	rt_kprintf("TODO: %s", __func__);
    return 0;
}

void dma_sync_sg_for_cpu(struct device *dev, struct scatterlist *sg,
		    int nelems, enum dma_data_direction dir)
{
    rt_kprintf("TODO: %s", __func__);
}

void dma_sync_sg_for_device(struct device *dev, struct scatterlist *sg,
		       int nelems, enum dma_data_direction dir)
{
    rt_kprintf("TODO: %s", __func__);
}

void dma_unmap_resource(struct device *dev, dma_addr_t addr, size_t size,
		enum dma_data_direction dir, unsigned long attrs)
{
    rt_kprintf("TODO: %s", __func__);

}

void dma_unmap_sg_attrs(struct device *dev, struct scatterlist *sg,
				      int nents, enum dma_data_direction dir,
				      unsigned long attrs)
{
    rt_kprintf("TODO: %s", __func__);
}

dma_addr_t dma_map_page_attrs(struct device *dev, struct page *page,
		size_t offset, size_t size, enum dma_data_direction dir,
		unsigned long attrs)
{
	rt_kprintf("TODO: %s", __func__);
	return 0;
}

void dma_unmap_page_attrs(struct device *dev, dma_addr_t addr, size_t size,
		enum dma_data_direction dir, unsigned long attrs)
{
    //rt_kprintf("TODO: %s", __func__);
}

void *dmam_alloc_attrs(struct device *dev, size_t size, dma_addr_t *dma_handle,
		gfp_t gfp, unsigned long attrs)
{
	rt_kprintf("TODO: %s", __func__);
	return NULL;
}

void dmam_free_coherent(struct device *dev, size_t size, void *vaddr,
		dma_addr_t dma_handle)
{
    rt_kprintf("TODO: %s", __func__);
}

int dma_set_coherent_mask(struct device *dev, u64 mask)
{
	return 0;
}
