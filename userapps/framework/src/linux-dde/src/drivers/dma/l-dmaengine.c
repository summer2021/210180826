#include <linux/dmaengine.h>
#include <linux/export.h>

int dmaengine_slave_config(struct dma_chan *chan, struct dma_slave_config *config)
{
	if (chan->device->device_config)
		return chan->device->device_config(chan, config);

	return -ENOSYS;
}

enum dma_status dmaengine_tx_status(struct dma_chan *chan,
	dma_cookie_t cookie, struct dma_tx_state *state)
{
    return chan->device->device_tx_status(chan, cookie, state);
}

struct dma_async_tx_descriptor *dmaengine_prep_slave_single(
	struct dma_chan *chan, dma_addr_t buf, size_t len,
	enum dma_transfer_direction dir, unsigned long flags)
{
	struct scatterlist sg;
	sg_init_table(&sg, 1);
	sg_dma_address(&sg) = buf;
	sg_dma_len(&sg) = len;

	if (!chan || !chan->device || !chan->device->device_prep_slave_sg)
		return NULL;

	return chan->device->device_prep_slave_sg(chan, &sg, 1,
						  dir, flags, NULL);
}

void dma_async_issue_pending(struct dma_chan *chan)
{
    chan->device->device_issue_pending(chan);
}

void dmaengine_synchronize(struct dma_chan *chan)
{
	if (chan->device->device_synchronize)
		chan->device->device_synchronize(chan);
}

int dmaengine_terminate_all(struct dma_chan *chan)
{
	if (chan->device->device_terminate_all)
		return chan->device->device_terminate_all(chan);

	return -ENOSYS;
}

dma_cookie_t dmaengine_submit(struct dma_async_tx_descriptor *desc)
{
    return desc->tx_submit(desc);
}

void dma_release_channel(struct dma_chan *chan)
{
    rt_kprintf("TODO %s", __func__);//TODO
}

/**
 * dma_request_chan - try to allocate an exclusive slave channel
 * @dev:	pointer to client device structure
 * @name:	slave channel name
 *
 * Returns pointer to appropriate DMA channel on success or an error pointer.
 */
struct dma_chan *dma_request_chan(struct device *dev, const char *name)
{
	struct dma_device *d;
	struct dma_chan *chan, *pos, *n;
    int i;

	d = (struct dma_device *)rt_device_find("dma");
    if (!d)
	    return NULL;

	list_for_each_entry_safe(pos, n, &d->channels, device_node)
	{
        if (rt_strcmp(pos->name, name) == 0)
		{
			pos->device = d;
			chan = pos;
			break;
		}
	}

	return chan;
}
EXPORT_SYMBOL_GPL(dma_request_chan);

int dmaenginem_async_device_register(struct dma_device *device)
{
	int ret;

	device->rtd.type = RT_Device_Class_Miscellaneous;
    ret = rt_device_register(&device->rtd, "dma", 0);

	return ret;
}
