#include <linux/device.h>
#include <linux/clk.h>

#include <rtdef.h>

RT_WEAK struct clk_hw * clk_find_hw(const char *dev_id, const char *con_id)
{
    return NULL;
}

unsigned long clk_get_rate(struct clk *clk)
{
    return clk->rate;
}

int clk_prepare_enable(struct clk *clk)
{
    return clk->ctrl(clk, CLK_IOC_ENABLE, NULL);
}

void clk_disable_unprepare(struct clk *clk)
{

}

struct clk *clk_get(struct device *dev, const char *id)
{
    struct clk_hw * ch = clk_find_hw(NULL, NULL);
    struct clk *c = NULL;

    if ((ch != NULL) && (id != NULL))
    {
        int i;

        for (i = 0; i < ch->size/sizeof(struct clk); i ++)
        {
            if (strcmp(id, ch->clk[i].id) == 0)
            {
                c = &ch->clk[i];
                break;
            }
        }
    }

    return c;
}

void clk_put(struct clk *clk)
{

}

struct clk *devm_clk_get(struct device *dev, const char *id)
{
    return clk_get(dev, id);
}

struct clk *devm_clk_get_optional(struct device *dev, const char *id)
{
    return devm_clk_get(dev, id);
}
