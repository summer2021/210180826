#include <linux/of.h>
#include <linux/reset.h>
#include <stdio.h>
#include <string.h>

struct reset_control *devm_reset_control_get_optional(
				struct device *dev, const char *id)
{
    struct device_node *dn;
    char name[12];
    int i;
    struct reset_control *ret = NULL;

    for (i = 0; i < 8; i ++)
    {
        struct reset_control *r;

        sprintf(name, "reset-%d", i);
        dn = device_node_find(dev, name);
        if (dn)
        {
            r = (struct reset_control *)dn->value;
            if (strcmp(r->name, id) == 0)
            {
                ret = r;
                break;
            }
        }
    }

    return ret;    
}
