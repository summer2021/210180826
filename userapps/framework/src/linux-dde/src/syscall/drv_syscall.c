/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2019-04-20     Jesven       the first version
 * 2019-10-01     Bernard      code cleanup
 * 2021-07-24     lizhirui     port to new version rt-smart
 */

#include <stdio.h>
#include <rtthread.h>
#include <lwp_shm.h>
#include <drv_syscall.h>

#define _NRSYS_rt_device_init 59
#define _NRSYS_rt_device_register 60
#define _NRSYS_rt_device_control 61
#define _NRSYS_rt_device_find 62
#define _NRSYS_rt_device_open 63
#define _NRSYS_rt_device_close 64
#define _NRSYS_rt_device_read 65
#define _NRSYS_rt_device_write 66
#define _NRSYS_rt_device_unregister 143
#define _NRSYS_dev_debug 144

enum
{
    LWP_DEVICE_CMD_INIT = 0,
    LWP_DEVICE_CMD_OPEN,
    LWP_DEVICE_CMD_CLOSE,
    LWP_DEVICE_CMD_READ,
    LWP_DEVICE_CMD_WRITE,
    LWP_DEVICE_CMD_CONTROL,
    LWP_DEVICE_CMD_INTERRUPT,
    LWP_DEVICE_CMD_QUIT,
};

#define LWP_DEVICE_OP_ARGS_MAX_NR 5

struct lwp_device_cmd
{
    void *client_lwp;
    rt_sem_t reply_sem;
    rt_uint32_t cmd;
    void *ret;
    void* arg[LWP_DEVICE_OP_ARGS_MAX_NR];
};

static struct rt_device_reply reply;

void *rt_device_client_get(void)
{
    return reply.client_lwp;
}

void rt_device_reply_get(rt_device_reply_t r)
{
    r->client_lwp = reply.client_lwp;
    r->reply = reply.reply;
    r->cmd = reply.cmd;
    reply.client_lwp = NULL;
    reply.reply = NULL;
    reply.cmd = NULL;
}

void rt_device_reply_ack(rt_device_reply_t r, size_t ret)
{
    struct lwp_device_cmd *cmd;
    struct rt_user_ipc sem;

    cmd = (struct lwp_device_cmd *)r->cmd;
    cmd->ret = (void*)ret;
    lwp_shmdt(cmd);
    sem.type = RT_Object_Class_Semaphore;
    sem.data = r->reply;
    rt_sem_release(&sem);
}

static void udev_thread_entry(void* parameter)
{
    void *call_ret;
    rt_err_t ret;
    int fd = (int)parameter;
    struct rt_channel_msg msg;
    int shmid;
    int need_quit = 0;
    rt_device_t dev;

    while (1)
    {
        struct lwp_device_cmd *cmd;

        do
        {
            ret = rt_channel_recv(fd, &msg);
        } while (ret != RT_EOK);

        shmid = (int)msg.u.d;
        cmd = lwp_shmat(shmid, NULL);
        reply.client_lwp = cmd->client_lwp;
        reply.reply = cmd->reply_sem;
        reply.cmd = cmd;

        switch (cmd->cmd)
        {
        case LWP_DEVICE_CMD_INIT:
            {
                rt_err_t (*p_init)(rt_device_t) = RT_NULL;

                dev = (rt_device_t)cmd->arg[0];
#ifdef RT_USING_DEVICE_OPS
                if (dev && dev->ops && dev->ops->init)
                {
                    p_init = dev->ops->init;
                }
#else
                if (dev && dev->init)
                {
                    init = dev->init;
                }
#endif
                if (p_init)
                {
                    call_ret = (void*)(size_t)p_init(dev);
                    if (reply.cmd)
                    {
                        cmd->ret = call_ret;
                    }
                }
                else
                {
                    cmd->ret = (void*)RT_EOK;
                }
            }
            break;
        case LWP_DEVICE_CMD_OPEN:
            {
                rt_err_t (*p_open)(rt_device_t, rt_uint16_t) = RT_NULL;

                dev = (rt_device_t)cmd->arg[0];
#ifdef RT_USING_DEVICE_OPS
                if (dev && dev->ops && dev->ops->open)
                {
                    p_open = dev->ops->open;
                }
#else
                if (dev && dev->open)
                {
                    p_open = dev->open;
                }
#endif
                if (p_open)
                {
                    void *call_ret;

                    call_ret = (void*)(size_t)p_open(dev, (rt_uint16_t)(size_t)cmd->arg[1]);
                    if (reply.cmd)
                    {
                        cmd->ret = call_ret;
                    }
                }
                else
                {
                    cmd->ret = (void*)RT_EOK;
                }
            }
            break;
        case LWP_DEVICE_CMD_CLOSE:
            {
                rt_err_t (*p_close)(rt_device_t) = RT_NULL;

                dev = (rt_device_t)cmd->arg[0];
#ifdef RT_USING_DEVICE_OPS
                if (dev && dev->ops && dev->ops->close)
                {
                    p_close = dev->ops->close;
                }
#else
                if (dev && dev->close)
                {
                    p_close = dev->close;
                }
#endif
                if (p_close)
                {
                    call_ret = (void*)(size_t)p_close(dev);
                    if (reply.cmd)
                    {
                        cmd->ret = call_ret;
                    }
                }
                else
                {
                    cmd->ret = (void*)RT_EOK;
                }
            }
            break;
        case LWP_DEVICE_CMD_READ:
            {
                rt_size_t (*p_read)(rt_device_t, rt_off_t, void*, rt_size_t);

                    dev = (rt_device_t)cmd->arg[0];
#ifdef RT_USING_DEVICE_OPS
                if (dev && dev->ops && dev->ops->read)
                {
                    p_read = dev->ops->read;
                }
#else
                if (dev && dev->read)
                {
                    p_read = dev->read;
                }
#endif
                if (p_read)
                {
                    call_ret = (void*)(size_t)p_read(dev, (rt_off_t)cmd->arg[1], (void*)cmd->arg[2], (rt_size_t)cmd->arg[3]);
                    if (reply.cmd)
                    {
                        cmd->ret = call_ret;
                    }
                }
                else
                {
                    cmd->ret = (void*)0;
                }
            }
            break;
        case LWP_DEVICE_CMD_WRITE:
            {
                rt_size_t (*p_write)(rt_device_t, rt_off_t, const void*, rt_size_t);

                    dev = (rt_device_t)cmd->arg[0];
#ifdef RT_USING_DEVICE_OPS
                if (dev && dev->ops && dev->ops->write)
                {
                    p_write = dev->ops->write;
                }
#else
                if (dev && dev->write)
                {
                    p_write = dev->write;
                }
#endif
                if (p_write)
                {
                    call_ret = (void*)(size_t)p_write(dev, (rt_off_t)cmd->arg[1], (const void*)cmd->arg[2], (rt_size_t)cmd->arg[3]);
                    if (reply.cmd)
                    {
                        cmd->ret = call_ret;
                    }
                }
                else
                {
                    cmd->ret = (void*)0;
                }
            }
            break;
        case LWP_DEVICE_CMD_CONTROL:
            {
                rt_err_t (*p_control)(rt_device_t, int, void*) = RT_NULL;

                dev = (rt_device_t)cmd->arg[0];
#ifdef RT_USING_DEVICE_OPS
                if (dev && dev->ops && dev->ops->control)
                {
                    p_control = dev->ops->control;
                }
#else
                if (dev && dev->control)
                {
                    p_control = dev->control;
                }
#endif
                if (p_control)
                {
                    call_ret = (void*)(size_t)p_control(dev, (int)cmd->arg[1], (void*)cmd->arg[2]);
                    if (reply.cmd)
                    {
                        cmd->ret = call_ret;
                    }
                }
                else
                {
                    cmd->ret = (void*)RT_EOK;
                }
            }
            break;
        case LWP_DEVICE_CMD_INTERRUPT:
            {
                void (*p_interrupt_handler)(int, void*);
                p_interrupt_handler = (void (*)(int, void*))cmd->arg[0];
                if (p_interrupt_handler)
                {
                    struct lwp_device_interrupt_info int_info;
                    rt_device_t udev = (rt_device_t)cmd->arg[3];

                    p_interrupt_handler((int)cmd->arg[1], (void*)cmd->arg[2]);
                    int_info.vector = (int)cmd->arg[1];
                    rt_device_control(udev, RT_DEVICE_CTRL_INT_UMASK, &int_info);
                }
            }
            break;
        case LWP_DEVICE_CMD_QUIT:
            {
                rt_err_t (*p_control)(rt_device_t, int, void*) = RT_NULL;

                dev = (rt_device_t)cmd->arg[0];
#ifdef RT_USING_DEVICE_OPS
                if (dev && dev->ops && dev->ops->control)
                {
                    p_control = dev->ops->control;
                }
#else
                if (dev && dev->control)
                {
                    p_control = dev->control;
                }
#endif
                if (p_control)
                {
                    call_ret = (void*)(size_t)p_control(dev, (int)cmd->arg[1], NULL);
                    if (reply.cmd)
                    {
                        cmd->ret = call_ret;
                    }
                }
                else
                {
                    cmd->ret = (void*)RT_EOK;
                }
            }
            need_quit = 1;
            break;
        default:
            break;
        }

        if (reply.cmd)
        {
            lwp_shmdt(reply.cmd);
            reply.cmd = NULL;
        }
        if (reply.reply)
        {
            struct rt_user_ipc sem;

            sem.type = RT_Object_Class_Semaphore;
            sem.data = reply.reply;
            rt_sem_release(&sem);
            reply.reply = NULL;
        }
        reply.client_lwp = NULL;

        if (need_quit)
        {
            rt_channel_close(fd);
            return;
        }
    }
}

rt_err_t rt_udevice_register(rt_device_t dev,
        const char *name,
        rt_uint16_t flags)
{
    rt_err_t ret;
    rt_thread_t tid = RT_NULL;
    int ch_fd = -1;

    ch_fd = rt_channel_open(name, O_CREAT | O_EXEC);
    if (ch_fd < 0)
    {
        goto err;
    }
    tid = rt_thread_create("udev", udev_thread_entry, (void*)ch_fd, 4096, 2, 10);
    if (!tid)
    {
        goto err;
    }
    ret = (rt_err_t)syscall(_NRSYS_rt_device_register, dev, name, flags);
    if (ret == RT_EOK)
    {
        rt_thread_startup(tid);
        return RT_EOK;
    }
err:
    if (tid)
    {
        rt_thread_delete(tid);
    }
    if (ch_fd >= 0)
    {
        rt_channel_close(ch_fd);
    }
    return RT_EIO;
}

rt_err_t rt_udevice_init(rt_device_t dev)
{
    return syscall(_NRSYS_rt_device_init, dev);
}

rt_err_t rt_udevice_control(rt_device_t dev, int cmd, void *arg)
{
    return syscall(_NRSYS_rt_device_control, dev, cmd, arg);
}

rt_device_t rt_udevice_find(const char *name)
{
    return (rt_device_t)syscall(_NRSYS_rt_device_find, name);
}

rt_err_t rt_udevice_open(rt_device_t dev, rt_uint16_t oflag)
{
    return syscall(_NRSYS_rt_device_open, dev, oflag);
}

rt_err_t rt_udevice_close(rt_device_t dev)
{
    return syscall(_NRSYS_rt_device_close, dev);
}

rt_size_t rt_udevice_read(rt_device_t dev,
        rt_off_t    pos,
        void       *buffer,
        rt_size_t   size)
{
    return syscall(_NRSYS_rt_device_read, dev, pos, buffer, size);
}

rt_size_t rt_udevice_write(rt_device_t dev,
        rt_off_t    pos,
        const void *buffer,
        rt_size_t   size)
{
    return syscall(_NRSYS_rt_device_write, dev, pos, buffer, size);;
}

rt_err_t rt_udevice_unregister(rt_device_t dev)
{
    return syscall(_NRSYS_rt_device_unregister, dev);
}

void rt_udevice_interrupt_install(rt_device_t udev, int vector, rt_isr_handler_t handler, void *param)
{
    struct lwp_device_interrupt_info int_info;

    int_info.vector = vector;
    int_info.handler = (void*)handler;
    int_info.param = param;
    rt_device_control(udev, RT_DEVICE_CTRL_SET_INT_HANDLER, &int_info);
}

void rt_udevice_interrupt_mask(rt_device_t udev, int vector)
{
    struct lwp_device_interrupt_info int_info;

    int_info.vector = vector;
    rt_device_control(udev, RT_DEVICE_CTRL_INT_MASK, &int_info);
}

void rt_udevice_interrupt_umask(rt_device_t udev, int vector)
{
    struct lwp_device_interrupt_info int_info;

    int_info.vector = vector;
    rt_device_control(udev, RT_DEVICE_CTRL_INT_UMASK, &int_info);
}

void* rt_ioremap(rt_device_t udev, void *paddr, size_t size, int cached)
{
    struct lwp_device_mapinfo info;
    info.paddr = paddr;
    info.size = size;
    info.cached = cached;
    rt_device_control(udev, RT_DEVICE_CTRL_IOREMAP, &info);
    return info.vaddr;
}

rt_err_t rt_iounremap(rt_device_t udev, void *vaddr, size_t size)
{
    struct lwp_device_mapinfo info;
    info.vaddr = vaddr;
    info.size = size;
    return rt_device_control(udev, RT_DEVICE_CTRL_IOUNREMAP, &info);
}

void* rt_mmap(rt_device_t udev, void **paddr, size_t size, int cached)
{
    struct lwp_device_mapinfo info;
    info.paddr = paddr;
    info.size = size;
    info.cached = cached;
    rt_device_control(udev, RT_DEVICE_CTRL_MMAP, &info);
    if (paddr)
    {
        *paddr = info.paddr;
    }
    return info.vaddr;
}

rt_err_t rt_munmap(rt_device_t udev, void *vaddr, size_t size)
{
    struct lwp_device_mapinfo info;
    info.vaddr = vaddr;
    info.size = size;
    return rt_device_control(udev, RT_DEVICE_CTRL_MUNMAP, &info);
}

size_t rt_data_get(rt_device_t udev, void *lwp, void *dst, const void *src, size_t size)
{
    struct lwp_device_data_info info;

    info.lwp = lwp;
    info.dst = dst;
    info.src = (void*)src;
    info.size = size;
    rt_device_control(udev, RT_DEVICE_CTRL_DATA_GET, &info);
    return info.size;
}

size_t rt_data_put(rt_device_t udev, void *lwp, void *dst, const void *src, size_t size)
{
    struct lwp_device_data_info info;

    info.lwp = lwp;
    info.dst = dst;
    info.src = (void*)src;
    info.size = size;
    rt_device_control(udev, RT_DEVICE_CTRL_DATA_PUT, &info);
    return info.size;
}

void rt_dcache_ops(rt_device_t udev, int ops, void *addr, int size)
{
    struct lwp_device_cache_info info;
    info.ops = ops;
    info.addr = addr;
    info.size = (size_t)size;
    rt_device_control(udev, RT_DEVICE_CTRL_CACHE_MAINTAIN, &info);
}

static rt_device_t current_udevice;

void rt_udevice_set_current(rt_device_t dev)
{
    current_udevice = dev;
}

rt_device_t rt_udevice_get_current()
{
    return current_udevice;
}

void dev_debug()
{
    syscall(_NRSYS_dev_debug);
}