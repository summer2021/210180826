/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-25     lizhirui     the first version
 * 2021-07-28     lizhirui     convert this to userspace emulation implement
 */

#include <rtthread.h>
#include <drv_syscall.h>
#include <ipc/ipc_interface.h>
#include <string.h>

#define _NRSYS_wqueue_init 145
#define _NRSYS_wqueue_wait 146
#define _NRSYS_wqueue_wakeup 147

#define _NRSYS_futex 131

#define FUTEX_WAIT  0
#define FUTEX_WAKE  1

static int sys_futex(int *uaddr,int op,int val,const struct timespec *timeout,int *uaddr2,int val3)
{
    syscall(_NRSYS_futex,uaddr,op,val,timeout,uaddr2,val3);
}

static int futex_wait(rt_wqueue_t *queue,int value,const struct timespec *timeout)
{
    sys_futex((void *)queue,FUTEX_WAIT,value,timeout,RT_NULL,0);
}

static void futex_wake(rt_wqueue_t *queue,int number)
{
    sys_futex((void *)queue,FUTEX_WAKE,number,RT_NULL,RT_NULL,0);
}

int rt_wqueue_init(rt_wqueue_t *queue)
{
    memset(queue,0,sizeof(queue));
    //RT_USER_IPC_INIT(queue,RT_Object_Class_Unknown,_NRSYS_wqueue_init);
}

int rt_wqueue_wait(rt_wqueue_t *queue,int condition,int timeout)
{
    if(condition || !timeout)
    {
        return 0;
    }

    return futex_wait(queue,0,RT_NULL);
    /*RT_USER_IPC_CHECK(queue,RT_Object_Class_Unknown);
    return syscall(_NRSYS_wqueue_wait,queue -> data,condition,timeout);*/
}

int rt_wqueue_wakeup(rt_wqueue_t *queue,void *key)
{
    /*RT_USER_IPC_CHECK(queue,RT_Object_Class_Unknown);
    return syscall(_NRSYS_wqueue_wakeup,queue -> data,key);*/
    futex_wake(queue,1);
    return 0;
}