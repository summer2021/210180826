/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-26     lizhirui     the first version
 */

#include <rtthread.h>
#include <ipc/ipc_interface.h>
#include <ipc/workqueue.h>

#define _NRSYS_rt_workqueue_create 148
#define _NRSYS_rt_workqueue_submit_work 149
#define _NRSYS_rt_work_init 150

static void workqueue_entry(void *arg)
{
    rt_mailbox_t mb = arg;
    rt_ubase_t recv;
    int cnt = 0;
    void (*work_func)(struct rt_work *work,void *work_data);
    void *work_data;
    struct rt_work *work;
    
    while(1)
    {
        printf("workqueue_entry,mb = %p\n",mb);

        if(rt_mb_recv(mb,&recv,RT_WAITING_FOREVER) == RT_EOK)
        {
            switch(cnt)
            {
                case 0:
                    work = (void *)recv;
                    cnt++;
                    break;

                case 1:
                    work_func = (void *)recv;
                    cnt++;
                    break;

                case 2:
                    work_data = (void *)recv;
                    work_func(work,work_data);
                    cnt = 0;
                    break;
            }
        }
    }
}

static struct rt_workqueue *__rt_workqueue_create(const char *name,rt_uint16_t stack_size,rt_uint8_t priority,rt_mailbox_t mb)
{
    RT_USER_IPC_CREATE(RT_Object_Class_Unknown,_NRSYS_rt_workqueue_create,name,stack_size,priority,mb);
}

struct rt_workqueue *rt_workqueue_create(const char *name,rt_uint16_t stack_size,rt_uint8_t priority)
{
    rt_mailbox_t mb = rt_mb_create(name, 16, RT_IPC_FLAG_FIFO);

    if(mb == RT_NULL)
    {
        return RT_NULL;
    }

    printf("rt_workqueue_create: %s\n",name);
    rt_thread_t thread = rt_thread_create(name, workqueue_entry, mb, 4096, 20, 10);

    if(thread == RT_NULL)
    {
        rt_mb_delete(mb);
        return RT_NULL;
    }

    struct rt_workqueue *queue = __rt_workqueue_create(name,stack_size,priority,mb);

    if(queue == RT_NULL)
    {
        rt_thread_delete(thread);
        rt_mb_delete(mb);
        return RT_NULL;
    }

    rt_thread_startup(thread);
    return queue;
}

rt_err_t rt_workqueue_submit_work(struct rt_workqueue *queue,struct rt_work *work,rt_tick_t time)
{
    printf("rt_workqueue_submit_work:queue = %p,work = %p\n",queue,work);
    RT_USER_IPC_CHECK(queue,RT_Object_Class_Unknown);
    return syscall(_NRSYS_rt_workqueue_submit_work,queue -> data,work -> data,time,work);
}

rt_err_t rt_work_init(struct rt_work *work,void (*work_func)(struct rt_work *work,void *work_data),void *work_data)
{
    rt_kprintf("rt_work_init\n");
    RT_USER_IPC_INIT(work,RT_Object_Class_Unknown,_NRSYS_rt_work_init,work_func,work_data);
}