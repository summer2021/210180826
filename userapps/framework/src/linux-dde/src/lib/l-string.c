#include <linux/string.h>
#include <linux/limits.h>
#include <linux/bug.h>
#include <linux/errno.h>
#include <stdlib.h>
#include <rtthread.h>

ssize_t strscpy(char *dest, const char *src, size_t count)
{
    ssize_t res = 0;

    if (count == 0 || WARN_ON_ONCE(count > INT_MAX))
        return -1;

    while (count) {
        char c;

        c = src[res];
        dest[res] = c;
        if (!c)
            return res;
        res++;
        count--;
    }

    /* Hit buffer length without finding a NUL; force NUL-termination. */
    if (res)
        dest[res-1] = '\0';

    return -1;
}

/**
 * sysfs_streq - return true if strings are equal, modulo trailing newline
 * @s1: one string
 * @s2: another string
 *
 * This routine returns true iff two strings are equal, treating both
 * NUL and newline-then-NUL as equivalent string terminations.  It's
 * geared for use with sysfs input strings, which generally terminate
 * with newlines but are compared against values without newlines.
 */
bool sysfs_streq(const char *s1, const char *s2)
{
    while (*s1 && *s1 == *s2) {
        s1++;
        s2++;
    }

    if (*s1 == *s2)
        return true;
    if (!*s1 && *s2 == '\n' && !s2[1])
        return true;
    if (*s1 == '\n' && !s1[1] && !*s2)
        return true;
    return false;
}

char *kstrdup(const char *s, gfp_t gfp)
{
    return rt_strdup(s);
}

int match_string(const char * const *array, size_t n, const char *string)
{
    int index;
    const char *item;

    for (index = 0; index < n; index++) {
        item = array[index];
        if (!item)
            break;
        if (!strcmp(item, string))
            return index;
    }

    return -EINVAL;
}
